/**
 *  Purolator Public Web Services Example Code
 *
 *  Requires    : Installation of soap module dependency
 *              : Local copy of the Service Availability WSDL
 *              : Valid Development/Production Key and Password
 *
 *  This example covers the creation of a proper SOAP Client (including envelope
 *  headers) to communicate with the Estimating Web Service.
 */
const soap = require('soap');
const utils = require('../utils');
const path = require('path');
const account = require('../credentials.json');

const API_KEY = account.key;
const API_PASSWORD = account.password;
const ACCOUNT_NUMBER = account.account;
const BILLING_ACCOUNT = account.billingAccount;
const USER_TOKEN = account.userToken;

function createPWSSOAPClient(headers) {

	const wsdlFile = 'EstimatingService.wsdl';
	const namespace = 'http://purolator.com/pws/datatypes/v2';

	const environment = account.production ? 'production' : 'development';
	const pathToWSDL = path.join(__dirname, '../../wsdl', environment, wsdlFile);

	const options = {
		envelopeKey: 'SOAP-ENV',
	};

	return soap
		.createClientAsync(pathToWSDL, options)
		.then(client => {

			client.setSecurity(new soap.BasicAuthSecurity(API_KEY, API_PASSWORD));
			client.addSoapHeader({ RequestContext: headers }, '', 'ns1', '');

			client.wsdl.definitions.xmlns.ns1 = namespace;
			client.wsdl.xmlnsInEnvelope = client.wsdl._xmlnsMap();

			return client;
		});
}

/**
 * GetFullEstimate Example
 *
 * 1 piece shipment, 10lbs, Purolator Express Service (and all other available
 * services) as Fully Regulated Dangerous Goods.
 */

const headers = {
	Version: '2.1',
	Language: 'en',
	GroupID: 'xxx',
	RequestReference: 'GetFullEstimate example',
	// Uncomment this for Commercial applications
	// UserToken: USER_TOKEN
};

createPWSSOAPClient(headers)
	.then(client => {

		const request = {
			Shipment: {
				SenderInformation: {
					Address: {
						Name: 'Aaron Summer',
						StreetNumber: '1234',
						StreetName: 'Main Street',
						City: 'Mississauga',
						Province: 'ON',
						Country: 'CA',
						PostalCode: 'L4W5M8',
						PhoneNumber: {
							CountryCode: '1',
							AreaCode: '905',
							Phone: '5555555',
						}
					}
				},
				ReceiverInformation: {
					Address: {
						Name: 'Aaron Summer',
						StreetNumber: '2245',
						StreetName: 'Douglas Road',
						City: 'Burnaby',
						Province: 'BC',
						Country: 'CA',
						PostalCode: 'V5C5A9',
						PhoneNumber: {
							CountryCode: '1',
							AreaCode: '604',
							Phone: '2982181',
						}
					}
				},
				ShipmentDate: '2018-10-30',
				PackageInformation: {
					ServiceID: 'PurolatorExpress',
					TotalWeight: {
						Value: 40,
						WeightUnit: 'lb',
					},
					TotalPieces: 1,
					PiecesInformation: {
						Piece: [{
							Weight: {
								Value: 40,
								WeightUnit: 'lb',
							},
							Length: {
								Value: 40,
								DimensionUnit: 'in',
							},
							Width: {
								Value: 10,
								DimensionUnit: 'in',
							},
							Height: {
								Value: 2,
								DimensionUnit: 'in',
							},
							Options: {
								OptionIDValuePair: [
									{ ID: 'SpecialHandling', Value: true },
									{ ID: 'SpecialHandlingType', Value: 'LargePackage' }
								]
							}
						}]
					},
					OptionsInformation: {
						Options: {
							OptionIDValuePair: [
								{ ID: 'DangerousGoods', Value: true },
								{ ID: 'DangerousGoodsMode', Value: 'Air' },
								{ ID: 'DangerousGoodsClass', Value: 'FullyRegulated' },
							]
						}
					}
				},
				PaymentInformation: {
					PaymentType: 'Sender',
					RegisteredAccountNumber: ACCOUNT_NUMBER,
					BillingAccountNumber: BILLING_ACCOUNT,
				},
				PickupInformation: {
					PickupType: 'DropOff',
				}
			},
			ShowAlternativeServicesIndicator: true
		};

		return client.GetFullEstimateAsync(request);
	})
	.then(([result, rawResponse, soapheader, rawRequest]) => {
		utils.log(rawRequest);
		utils.log(rawResponse);
		utils.log(result);
	})
	.catch(error => {
		console.log(error);
	});

/**
 * SOAP Request Envelope (Request Made from the SOAP Client)
 * <?xml version="1.0" encoding="UTF-8"?>
 * <?xml version="1.0" encoding="utf-8"?><SOAP-ENV:Envelope xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"  xmlns:tns="http://purolator.com/pws/service/v2" xmlns:msc="http://schemas.microsoft.com/ws/2005/12/wsdl/contract" xmlns:wsu="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd" xmlns:q1="http://schemas.datacontract.org/2004/07/Microsoft.Practices.EnterpriseLibrary.Validation.Integration.WCF" xmlns:q2="http://purolator.com/pws/datatypes/v2" xmlns:q3="http://purolator.com/pws/datatypes/v2" xmlns:q4="http://purolator.com/pws/datatypes/v2" xmlns:q5="http://www.microsoft.com/practices/EnterpriseLibrary/2007/01/wcf/validation" xmlns:q6="http://purolator.com/pws/datatypes/v2" xmlns:q7="http://purolator.com/pws/datatypes/v2" xmlns:q8="http://purolator.com/pws/datatypes/v2" xmlns:q9="http://purolator.com/pws/datatypes/v2" xmlns:q10="http://www.microsoft.com/practices/EnterpriseLibrary/2007/01/wcf/validation" xmlns:ns1="http://purolator.com/pws/datatypes/v2"><SOAP-ENV:Header><ns1:RequestContext><ns1:Version>2.1</ns1:Version><ns1:Language>en</ns1:Language><ns1:GroupID>xxx</ns1:GroupID><ns1:RequestReference>GetFullEstimate example</ns1:RequestReference></ns1:RequestContext></SOAP-ENV:Header><SOAP-ENV:Body><q6:GetFullEstimateRequest xmlns:q6="http://purolator.com/pws/datatypes/v2" xmlns="http://purolator.com/pws/datatypes/v2"><q6:Shipment><q6:SenderInformation><q6:Address><q6:Name>Aaron Summer</q6:Name><q6:StreetNumber>1234</q6:StreetNumber><q6:StreetName>Main Street</q6:StreetName><q6:City>Mississauga</q6:City><q6:Province>ON</q6:Province><q6:Country>CA</q6:Country><q6:PostalCode>L4W5M8</q6:PostalCode><q6:PhoneNumber><q6:CountryCode>1</q6:CountryCode><q6:AreaCode>905</q6:AreaCode><q6:Phone>5555555</q6:Phone></q6:PhoneNumber></q6:Address></q6:SenderInformation><q6:ReceiverInformation><q6:Address><q6:Name>Aaron Summer</q6:Name><q6:StreetNumber>2245</q6:StreetNumber><q6:StreetName>Douglas Road</q6:StreetName><q6:City>Burnaby</q6:City><q6:Province>BC</q6:Province><q6:Country>CA</q6:Country><q6:PostalCode>V5C5A9</q6:PostalCode><q6:PhoneNumber><q6:CountryCode>1</q6:CountryCode><q6:AreaCode>604</q6:AreaCode><q6:Phone>2982181</q6:Phone></q6:PhoneNumber></q6:Address></q6:ReceiverInformation><q6:ShipmentDate>2018-10-30</q6:ShipmentDate><q6:PackageInformation><q6:ServiceID>PurolatorExpress</q6:ServiceID><q6:TotalWeight><q6:Value>40</q6:Value><q6:WeightUnit>lb</q6:WeightUnit></q6:TotalWeight><q6:TotalPieces>1</q6:TotalPieces><q6:PiecesInformation><q6:Piece><q6:Weight><q6:Value>40</q6:Value><q6:WeightUnit>lb</q6:WeightUnit></q6:Weight><q6:Length><q6:Value>40</q6:Value><q6:DimensionUnit>in</q6:DimensionUnit></q6:Length><q6:Width><q6:Value>10</q6:Value><q6:DimensionUnit>in</q6:DimensionUnit></q6:Width><q6:Height><q6:Value>2</q6:Value><q6:DimensionUnit>in</q6:DimensionUnit></q6:Height><q6:Options><q6:OptionIDValuePair><q6:ID>SpecialHandling</q6:ID><q6:Value>true</q6:Value></q6:OptionIDValuePair><q6:OptionIDValuePair><q6:ID>SpecialHandlingType</q6:ID><q6:Value>LargePackage</q6:Value></q6:OptionIDValuePair></q6:Options></q6:Piece></q6:PiecesInformation><q6:OptionsInformation><q6:Options><q6:OptionIDValuePair><q6:ID>DangerousGoods</q6:ID><q6:Value>true</q6:Value></q6:OptionIDValuePair><q6:OptionIDValuePair><q6:ID>DangerousGoodsMode</q6:ID><q6:Value>Air</q6:Value></q6:OptionIDValuePair><q6:OptionIDValuePair><q6:ID>DangerousGoodsClass</q6:ID><q6:Value>FullyRegulated</q6:Value></q6:OptionIDValuePair></q6:Options></q6:OptionsInformation></q6:PackageInformation><q6:PaymentInformation><q6:PaymentType>Sender</q6:PaymentType><q6:RegisteredAccountNumber>9999999999</q6:RegisteredAccountNumber><q6:BillingAccountNumber>9999999999</q6:BillingAccountNumber></q6:PaymentInformation><q6:PickupInformation><q6:PickupType>DropOff</q6:PickupType></q6:PickupInformation></q6:Shipment><q6:ShowAlternativeServicesIndicator>true</q6:ShowAlternativeServicesIndicator></q6:GetFullEstimateRequest></SOAP-ENV:Body></SOAP-ENV:Envelope>
 **/

/**
 * SOAP Response Envelope (Request Returned from the Web Service)
 * <s:Envelope xmlns:s="http://schemas.xmlsoap.org/soap/envelope/"><s:Header><h:ResponseContext xmlns:h="http://purolator.com/pws/datatypes/v2" xmlns:i="http://www.w3.org/2001/XMLSchema-instance"><h:ResponseReference>GetFullEstimate example</h:ResponseReference></h:ResponseContext></s:Header><s:Body><GetFullEstimateResponse xmlns="http://purolator.com/pws/datatypes/v2" xmlns:i="http://www.w3.org/2001/XMLSchema-instance"><ResponseInformation><Errors/><InformationalMessages i:nil="true"/></ResponseInformation><ShipmentEstimates><ShipmentEstimate><ServiceID>PurolatorExpress9AM</ServiceID><ShipmentDate>2018-10-30</ShipmentDate><ExpectedDeliveryDate>2018-10-31</ExpectedDeliveryDate><EstimatedTransitDays>1</EstimatedTransitDays><BasePrice>232.9</BasePrice><Surcharges><Surcharge><Amount>27.95</Amount><Type>Fuel</Type><Description>Fuel</Description></Surcharge></Surcharges><Taxes><Tax><Amount>0</Amount><Type>PSTQST</Type><Description>PST/QST</Description></Tax><Tax><Amount>39.58</Amount><Type>HST</Type><Description>HST</Description></Tax><Tax><Amount>0</Amount><Type>GST</Type><Description>GST</Description></Tax></Taxes><OptionPrices><OptionPrice><Amount>14</Amount><ID>DangerousGoods</ID><Description>Dangerous Goods</Description></OptionPrice><OptionPrice><Amount>55</Amount><ID>LargePackage</ID><Description>Special Handling - Large Package(1pc)</Description></OptionPrice></OptionPrices><TotalPrice>369.43</TotalPrice></ShipmentEstimate><ShipmentEstimate><ServiceID>PurolatorExpress10:30AM</ServiceID><ShipmentDate>2018-10-30</ShipmentDate><ExpectedDeliveryDate>2018-10-31</ExpectedDeliveryDate><EstimatedTransitDays>1</EstimatedTransitDays><BasePrice>196.3</BasePrice><Surcharges><Surcharge><Amount>23.56</Amount><Type>Fuel</Type><Description>Fuel</Description></Surcharge></Surcharges><Taxes><Tax><Amount>0</Amount><Type>PSTQST</Type><Description>PST/QST</Description></Tax><Tax><Amount>34.66</Amount><Type>HST</Type><Description>HST</Description></Tax><Tax><Amount>0</Amount><Type>GST</Type><Description>GST</Description></Tax></Taxes><OptionPrices><OptionPrice><Amount>14</Amount><ID>DangerousGoods</ID><Description>Dangerous Goods</Description></OptionPrice><OptionPrice><Amount>55</Amount><ID>LargePackage</ID><Description>Special Handling - Large Package(1pc)</Description></OptionPrice></OptionPrices><TotalPrice>323.52</TotalPrice></ShipmentEstimate><ShipmentEstimate><ServiceID>PurolatorExpress12PM</ServiceID><ShipmentDate>2018-10-30</ShipmentDate><ExpectedDeliveryDate>2018-10-31</ExpectedDeliveryDate><EstimatedTransitDays>1</EstimatedTransitDays><BasePrice>192.5</BasePrice><Surcharges><Surcharge><Amount>23.1</Amount><Type>Fuel</Type><Description>Fuel</Description></Surcharge></Surcharges><Taxes><Tax><Amount>0</Amount><Type>PSTQST</Type><Description>PST/QST</Description></Tax><Tax><Amount>34.15</Amount><Type>HST</Type><Description>HST</Description></Tax><Tax><Amount>0</Amount><Type>GST</Type><Description>GST</Description></Tax></Taxes><OptionPrices><OptionPrice><Amount>14</Amount><ID>DangerousGoods</ID><Description>Dangerous Goods</Description></OptionPrice><OptionPrice><Amount>55</Amount><ID>LargePackage</ID><Description>Special Handling - Large Package(1pc)</Description></OptionPrice></OptionPrices><TotalPrice>318.75</TotalPrice></ShipmentEstimate><ShipmentEstimate><ServiceID>PurolatorExpress</ServiceID><ShipmentDate>2018-10-30</ShipmentDate><ExpectedDeliveryDate>2018-10-31</ExpectedDeliveryDate><EstimatedTransitDays>1</EstimatedTransitDays><BasePrice>170</BasePrice><Surcharges><Surcharge><Amount>20.4</Amount><Type>Fuel</Type><Description>Fuel</Description></Surcharge></Surcharges><Taxes><Tax><Amount>0</Amount><Type>PSTQST</Type><Description>PST/QST</Description></Tax><Tax><Amount>31.13</Amount><Type>HST</Type><Description>HST</Description></Tax><Tax><Amount>0</Amount><Type>GST</Type><Description>GST</Description></Tax></Taxes><OptionPrices><OptionPrice><Amount>14</Amount><ID>DangerousGoods</ID><Description>Dangerous Goods</Description></OptionPrice><OptionPrice><Amount>55</Amount><ID>LargePackage</ID><Description>Special Handling - Large Package(1pc)</Description></OptionPrice></OptionPrices><TotalPrice>290.53</TotalPrice></ShipmentEstimate><ShipmentEstimate><ServiceID>PurolatorGround9AM</ServiceID><ShipmentDate>2018-10-30</ShipmentDate><ExpectedDeliveryDate>2018-11-05</ExpectedDeliveryDate><EstimatedTransitDays>4</EstimatedTransitDays><BasePrice>107.95</BasePrice><Surcharges><Surcharge><Amount>12.95</Amount><Type>Fuel</Type><Description>Fuel</Description></Surcharge></Surcharges><Taxes><Tax><Amount>0</Amount><Type>PSTQST</Type><Description>PST/QST</Description></Tax><Tax><Amount>22.79</Amount><Type>HST</Type><Description>HST</Description></Tax><Tax><Amount>0</Amount><Type>GST</Type><Description>GST</Description></Tax></Taxes><OptionPrices><OptionPrice><Amount>14</Amount><ID>DangerousGoods</ID><Description>Dangerous Goods</Description></OptionPrice><OptionPrice><Amount>55</Amount><ID>LargePackage</ID><Description>Special Handling - Large Package(1pc)</Description></OptionPrice></OptionPrices><TotalPrice>212.69</TotalPrice></ShipmentEstimate><ShipmentEstimate><ServiceID>PurolatorGround10:30AM</ServiceID><ShipmentDate>2018-10-30</ShipmentDate><ExpectedDeliveryDate>2018-11-05</ExpectedDeliveryDate><EstimatedTransitDays>4</EstimatedTransitDays><BasePrice>93.1</BasePrice><Surcharges><Surcharge><Amount>11.17</Amount><Type>Fuel</Type><Description>Fuel</Description></Surcharge></Surcharges><Taxes><Tax><Amount>0</Amount><Type>PSTQST</Type><Description>PST/QST</Description></Tax><Tax><Amount>20.79</Amount><Type>HST</Type><Description>HST</Description></Tax><Tax><Amount>0</Amount><Type>GST</Type><Description>GST</Description></Tax></Taxes><OptionPrices><OptionPrice><Amount>14</Amount><ID>DangerousGoods</ID><Description>Dangerous Goods</Description></OptionPrice><OptionPrice><Amount>55</Amount><ID>LargePackage</ID><Description>Special Handling - Large Package(1pc)</Description></OptionPrice></OptionPrices><TotalPrice>194.06</TotalPrice></ShipmentEstimate><ShipmentEstimate><ServiceID>PurolatorGround</ServiceID><ShipmentDate>2018-10-30</ShipmentDate><ExpectedDeliveryDate>2018-11-05</ExpectedDeliveryDate><EstimatedTransitDays>4</EstimatedTransitDays><BasePrice>81.05</BasePrice><Surcharges><Surcharge><Amount>9.73</Amount><Type>Fuel</Type><Description>Fuel</Description></Surcharge></Surcharges><Taxes><Tax><Amount>0</Amount><Type>PSTQST</Type><Description>PST/QST</Description></Tax><Tax><Amount>19.17</Amount><Type>HST</Type><Description>HST</Description></Tax><Tax><Amount>0</Amount><Type>GST</Type><Description>GST</Description></Tax></Taxes><OptionPrices><OptionPrice><Amount>14</Amount><ID>DangerousGoods</ID><Description>Dangerous Goods</Description></OptionPrice><OptionPrice><Amount>55</Amount><ID>LargePackage</ID><Description>Special Handling - Large Package(1pc)</Description></OptionPrice></OptionPrices><TotalPrice>178.95</TotalPrice></ShipmentEstimate></ShipmentEstimates><ReturnShipmentEstimates i:nil="true"/></GetFullEstimateResponse></s:Body></s:Envelope>
 **/

/*
 * EXPECTED RESULTS from PWS
{ ResponseInformation: { Errors: null },
  ShipmentEstimates:
   { ShipmentEstimate:
      [ { ServiceID: 'PurolatorExpress9AM',
          ShipmentDate: '2018-10-30',
          ExpectedDeliveryDate: '2018-10-31',
          EstimatedTransitDays: 1,
          BasePrice: '232.9',
          Surcharges:
           { Surcharge: [ { Amount: '27.95', Type: 'Fuel', Description: 'Fuel' } ] },
          Taxes:
           { Tax:
              [ { Amount: '0', Type: 'PSTQST', Description: 'PST/QST' },
                { Amount: '39.58', Type: 'HST', Description: 'HST' },
                { Amount: '0', Type: 'GST', Description: 'GST' } ] },
          OptionPrices:
           { OptionPrice:
              [ { Amount: '14',
                  ID: 'DangerousGoods',
                  Description: 'Dangerous Goods' },
                { Amount: '55',
                  ID: 'LargePackage',
                  Description: 'Special Handling - Large Package(1pc)' } ] },
          TotalPrice: '369.43' },
        { ServiceID: 'PurolatorExpress10:30AM',
          ShipmentDate: '2018-10-30',
          ExpectedDeliveryDate: '2018-10-31',
          EstimatedTransitDays: 1,
          BasePrice: '196.3',
          Surcharges:
           { Surcharge: [ { Amount: '23.56', Type: 'Fuel', Description: 'Fuel' } ] },
          Taxes:
           { Tax:
              [ { Amount: '0', Type: 'PSTQST', Description: 'PST/QST' },
                { Amount: '34.66', Type: 'HST', Description: 'HST' },
                { Amount: '0', Type: 'GST', Description: 'GST' } ] },
          OptionPrices:
           { OptionPrice:
              [ { Amount: '14',
                  ID: 'DangerousGoods',
                  Description: 'Dangerous Goods' },
                { Amount: '55',
                  ID: 'LargePackage',
                  Description: 'Special Handling - Large Package(1pc)' } ] },
          TotalPrice: '323.52' },
        { ServiceID: 'PurolatorExpress12PM',
          ShipmentDate: '2018-10-30',
          ExpectedDeliveryDate: '2018-10-31',
          EstimatedTransitDays: 1,
          BasePrice: '192.5',
          Surcharges:
           { Surcharge: [ { Amount: '23.1', Type: 'Fuel', Description: 'Fuel' } ] },
          Taxes:
           { Tax:
              [ { Amount: '0', Type: 'PSTQST', Description: 'PST/QST' },
                { Amount: '34.15', Type: 'HST', Description: 'HST' },
                { Amount: '0', Type: 'GST', Description: 'GST' } ] },
          OptionPrices:
           { OptionPrice:
              [ { Amount: '14',
                  ID: 'DangerousGoods',
                  Description: 'Dangerous Goods' },
                { Amount: '55',
                  ID: 'LargePackage',
                  Description: 'Special Handling - Large Package(1pc)' } ] },
          TotalPrice: '318.75' },
        { ServiceID: 'PurolatorExpress',
          ShipmentDate: '2018-10-30',
          ExpectedDeliveryDate: '2018-10-31',
          EstimatedTransitDays: 1,
          BasePrice: '170',
          Surcharges:
           { Surcharge: [ { Amount: '20.4', Type: 'Fuel', Description: 'Fuel' } ] },
          Taxes:
           { Tax:
              [ { Amount: '0', Type: 'PSTQST', Description: 'PST/QST' },
                { Amount: '31.13', Type: 'HST', Description: 'HST' },
                { Amount: '0', Type: 'GST', Description: 'GST' } ] },
          OptionPrices:
           { OptionPrice:
              [ { Amount: '14',
                  ID: 'DangerousGoods',
                  Description: 'Dangerous Goods' },
                { Amount: '55',
                  ID: 'LargePackage',
                  Description: 'Special Handling - Large Package(1pc)' } ] },
          TotalPrice: '290.53' },
        { ServiceID: 'PurolatorGround9AM',
          ShipmentDate: '2018-10-30',
          ExpectedDeliveryDate: '2018-11-05',
          EstimatedTransitDays: 4,
          BasePrice: '107.95',
          Surcharges:
           { Surcharge: [ { Amount: '12.95', Type: 'Fuel', Description: 'Fuel' } ] },
          Taxes:
           { Tax:
              [ { Amount: '0', Type: 'PSTQST', Description: 'PST/QST' },
                { Amount: '22.79', Type: 'HST', Description: 'HST' },
                { Amount: '0', Type: 'GST', Description: 'GST' } ] },
          OptionPrices:
           { OptionPrice:
              [ { Amount: '14',
                  ID: 'DangerousGoods',
                  Description: 'Dangerous Goods' },
                { Amount: '55',
                  ID: 'LargePackage',
                  Description: 'Special Handling - Large Package(1pc)' } ] },
          TotalPrice: '212.69' },
        { ServiceID: 'PurolatorGround10:30AM',
          ShipmentDate: '2018-10-30',
          ExpectedDeliveryDate: '2018-11-05',
          EstimatedTransitDays: 4,
          BasePrice: '93.1',
          Surcharges:
           { Surcharge: [ { Amount: '11.17', Type: 'Fuel', Description: 'Fuel' } ] },
          Taxes:
           { Tax:
              [ { Amount: '0', Type: 'PSTQST', Description: 'PST/QST' },
                { Amount: '20.79', Type: 'HST', Description: 'HST' },
                { Amount: '0', Type: 'GST', Description: 'GST' } ] },
          OptionPrices:
           { OptionPrice:
              [ { Amount: '14',
                  ID: 'DangerousGoods',
                  Description: 'Dangerous Goods' },
                { Amount: '55',
                  ID: 'LargePackage',
                  Description: 'Special Handling - Large Package(1pc)' } ] },
          TotalPrice: '194.06' },
        { ServiceID: 'PurolatorGround',
          ShipmentDate: '2018-10-30',
          ExpectedDeliveryDate: '2018-11-05',
          EstimatedTransitDays: 4,
          BasePrice: '81.05',
          Surcharges:
           { Surcharge: [ { Amount: '9.73', Type: 'Fuel', Description: 'Fuel' } ] },
          Taxes:
           { Tax:
              [ { Amount: '0', Type: 'PSTQST', Description: 'PST/QST' },
                { Amount: '19.17', Type: 'HST', Description: 'HST' },
                { Amount: '0', Type: 'GST', Description: 'GST' } ] },
          OptionPrices:
           { OptionPrice:
              [ { Amount: '14',
                  ID: 'DangerousGoods',
                  Description: 'Dangerous Goods' },
                { Amount: '55',
                  ID: 'LargePackage',
                  Description: 'Special Handling - Large Package(1pc)' } ] },
          TotalPrice: '178.95' } ] } }

 */
