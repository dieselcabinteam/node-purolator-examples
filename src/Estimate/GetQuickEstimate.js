/**
 *  Purolator Public Web Services Example Code
 *
 *  Requires    : Installation of soap module dependency
 *              : Local copy of the Service Availability WSDL
 *              : Valid Development/Production Key and Password
 *
 *  This example covers the creation of a proper SOAP Client (including envelope
 *  headers) to communicate with the Estimating Web Service.
 */
const soap = require('soap');
const utils = require('../utils');
const path = require('path');
const account = require('../credentials.json');

const API_KEY = account.key;
const API_PASSWORD = account.password;
const ACCOUNT_NUMBER = account.account;
const BILLING_ACCOUNT = account.billingAccount;
const USER_TOKEN = account.userToken;

function createPWSSOAPClient(headers) {

	const wsdlFile = 'EstimatingService.wsdl';
	const namespace = 'http://purolator.com/pws/datatypes/v2';

	const environment = account.production ? 'production' : 'development';
	const pathToWSDL = path.join(__dirname, '../../wsdl', environment, wsdlFile);

	const options = {
		envelopeKey: 'SOAP-ENV',
		overrideRootElement: {
			// NOTE: Sometimes the WSDL is not properly configured and the namespace is never applied to the request
			namespace: 'ns1',
		}
	};

	return soap
		.createClientAsync(pathToWSDL, options)
		.then(client => {

			client.setSecurity(new soap.BasicAuthSecurity(API_KEY, API_PASSWORD));
			client.addSoapHeader({ RequestContext: headers }, '', 'ns1', '');

			client.wsdl.definitions.xmlns.ns1 = namespace;
			client.wsdl.xmlnsInEnvelope = client.wsdl._xmlnsMap();

			return client;
		});
}

/**
 * GetQuickEstimate Example
 *
 * 1 piece shipment, 10lbs, Customer Packaging
 */

const headers = {
	Version: '2.1',
	Language: 'en',
	GroupID: 'xxx',
	RequestReference: 'GetQuickEstimate example',
	// Uncomment this for Commercial applications
	// UserToken: USER_TOKEN
};

createPWSSOAPClient(headers)
	.then(client => {

		const request = {
			BillingAccountNumber: BILLING_ACCOUNT,
			SenderPostalCode: 'L4W5M8',
			ReceiverAddress: {
				City: 'Burnaby',
				Province: 'BC',
				Country: 'CA',
				PostalCode: 'V5C5A9',
			},
			PackageType: 'CustomerPackaging',
			TotalWeight: {
				Value: '10',
				WeightUnit: 'lb',
			}
		};

		// NOTE: Sometimes the WSDL is not properly configured and the namespace is never applied to the request
		const requestWithNapespace = utils.addNamespace(request);

		return client.GetQuickEstimateAsync(requestWithNapespace);
	})
	.then(([result, rawResponse, soapheader, rawRequest]) => {
		utils.log(rawRequest);
		utils.log(rawResponse);
		utils.log(result);
	})
	.catch(error => {
		console.log(error);
	});

/**
 * SOAP Request Envelope (Request Made from the SOAP Client)
 * <?xml version="1.0" encoding="UTF-8"?>
 * <?xml version="1.0" encoding="utf-8"?><SOAP-ENV:Envelope xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"  xmlns:tns="http://purolator.com/pws/service/v2" xmlns:msc="http://schemas.microsoft.com/ws/2005/12/wsdl/contract" xmlns:wsu="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd" xmlns:q1="http://schemas.datacontract.org/2004/07/Microsoft.Practices.EnterpriseLibrary.Validation.Integration.WCF" xmlns:q2="http://purolator.com/pws/datatypes/v2" xmlns:q3="http://purolator.com/pws/datatypes/v2" xmlns:q4="http://purolator.com/pws/datatypes/v2" xmlns:q5="http://www.microsoft.com/practices/EnterpriseLibrary/2007/01/wcf/validation" xmlns:q6="http://purolator.com/pws/datatypes/v2" xmlns:q7="http://purolator.com/pws/datatypes/v2" xmlns:q8="http://purolator.com/pws/datatypes/v2" xmlns:q9="http://purolator.com/pws/datatypes/v2" xmlns:q10="http://www.microsoft.com/practices/EnterpriseLibrary/2007/01/wcf/validation" xmlns:ns1="http://purolator.com/pws/datatypes/v2"><SOAP-ENV:Header><ns1:RequestContext><ns1:Version>2.1</ns1:Version><ns1:Language>en</ns1:Language><ns1:GroupID>xxx</ns1:GroupID><ns1:RequestReference>GetQuickEstimate example</ns1:RequestReference></ns1:RequestContext></SOAP-ENV:Header><SOAP-ENV:Body><ns1:GetQuickEstimateRequest><ns1:BillingAccountNumber>9999999999</ns1:BillingAccountNumber><ns1:SenderPostalCode>L4W5M8</ns1:SenderPostalCode><ns1:ReceiverAddress><ns1:City>Burnaby</ns1:City><ns1:Province>BC</ns1:Province><ns1:Country>CA</ns1:Country><ns1:PostalCode>V5C5A9</ns1:PostalCode></ns1:ReceiverAddress><ns1:PackageType>CustomerPackaging</ns1:PackageType><ns1:TotalWeight><ns1:Value>10</ns1:Value><ns1:WeightUnit>lb</ns1:WeightUnit></ns1:TotalWeight></ns1:GetQuickEstimateRequest></SOAP-ENV:Body></SOAP-ENV:Envelope>
 **/

/**
 * SOAP Response Envelope (Request Returned from the Web Service)
 * <s:Envelope xmlns:s="http://schemas.xmlsoap.org/soap/envelope/"><s:Header><h:ResponseContext xmlns:h="http://purolator.com/pws/datatypes/v2" xmlns:i="http://www.w3.org/2001/XMLSchema-instance"><h:ResponseReference>GetQuickEstimate example</h:ResponseReference></h:ResponseContext></s:Header><s:Body><GetQuickEstimateResponse xmlns="http://purolator.com/pws/datatypes/v2" xmlns:i="http://www.w3.org/2001/XMLSchema-instance"><ResponseInformation><Errors/><InformationalMessages i:nil="true"/></ResponseInformation><ShipmentEstimates><ShipmentEstimate><ServiceID>PurolatorExpress9AM</ServiceID><ShipmentDate>2018-10-26</ShipmentDate><ExpectedDeliveryDate>2018-10-29</ExpectedDeliveryDate><EstimatedTransitDays>1</EstimatedTransitDays><BasePrice>104.6</BasePrice><Surcharges><Surcharge><Amount>12.55</Amount><Type>Fuel</Type><Description>Fuel</Description></Surcharge></Surcharges><Taxes><Tax><Amount>0</Amount><Type>PSTQST</Type><Description>PST/QST</Description></Tax><Tax><Amount>14.06</Amount><Type>HST</Type><Description>HST</Description></Tax><Tax><Amount>0</Amount><Type>GST</Type><Description>GST</Description></Tax></Taxes><OptionPrices i:nil="true"/><TotalPrice>131.21</TotalPrice></ShipmentEstimate><ShipmentEstimate><ServiceID>PurolatorExpress10:30AM</ServiceID><ShipmentDate>2018-10-26</ShipmentDate><ExpectedDeliveryDate>2018-10-29</ExpectedDeliveryDate><EstimatedTransitDays>1</EstimatedTransitDays><BasePrice>82.85</BasePrice><Surcharges><Surcharge><Amount>9.94</Amount><Type>Fuel</Type><Description>Fuel</Description></Surcharge></Surcharges><Taxes><Tax><Amount>0</Amount><Type>PSTQST</Type><Description>PST/QST</Description></Tax><Tax><Amount>11.13</Amount><Type>HST</Type><Description>HST</Description></Tax><Tax><Amount>0</Amount><Type>GST</Type><Description>GST</Description></Tax></Taxes><OptionPrices i:nil="true"/><TotalPrice>103.92</TotalPrice></ShipmentEstimate><ShipmentEstimate><ServiceID>PurolatorExpress12PM</ServiceID><ShipmentDate>2018-10-26</ShipmentDate><ExpectedDeliveryDate>2018-10-29</ExpectedDeliveryDate><EstimatedTransitDays>1</EstimatedTransitDays><BasePrice>75.5</BasePrice><Surcharges><Surcharge><Amount>9.06</Amount><Type>Fuel</Type><Description>Fuel</Description></Surcharge></Surcharges><Taxes><Tax><Amount>0</Amount><Type>PSTQST</Type><Description>PST/QST</Description></Tax><Tax><Amount>10.15</Amount><Type>HST</Type><Description>HST</Description></Tax><Tax><Amount>0</Amount><Type>GST</Type><Description>GST</Description></Tax></Taxes><OptionPrices i:nil="true"/><TotalPrice>94.71</TotalPrice></ShipmentEstimate><ShipmentEstimate><ServiceID>PurolatorExpress</ServiceID><ShipmentDate>2018-10-26</ShipmentDate><ExpectedDeliveryDate>2018-10-29</ExpectedDeliveryDate><EstimatedTransitDays>1</EstimatedTransitDays><BasePrice>66.65</BasePrice><Surcharges><Surcharge><Amount>8</Amount><Type>Fuel</Type><Description>Fuel</Description></Surcharge></Surcharges><Taxes><Tax><Amount>0</Amount><Type>PSTQST</Type><Description>PST/QST</Description></Tax><Tax><Amount>8.96</Amount><Type>HST</Type><Description>HST</Description></Tax><Tax><Amount>0</Amount><Type>GST</Type><Description>GST</Description></Tax></Taxes><OptionPrices i:nil="true"/><TotalPrice>83.61</TotalPrice></ShipmentEstimate><ShipmentEstimate><ServiceID>PurolatorGround9AM</ServiceID><ShipmentDate>2018-10-26</ShipmentDate><ExpectedDeliveryDate>2018-11-01</ExpectedDeliveryDate><EstimatedTransitDays>4</EstimatedTransitDays><BasePrice>67.95</BasePrice><Surcharges><Surcharge><Amount>8.15</Amount><Type>Fuel</Type><Description>Fuel</Description></Surcharge></Surcharges><Taxes><Tax><Amount>0</Amount><Type>PSTQST</Type><Description>PST/QST</Description></Tax><Tax><Amount>9.13</Amount><Type>HST</Type><Description>HST</Description></Tax><Tax><Amount>0</Amount><Type>GST</Type><Description>GST</Description></Tax></Taxes><OptionPrices i:nil="true"/><TotalPrice>85.23</TotalPrice></ShipmentEstimate><ShipmentEstimate><ServiceID>PurolatorGround10:30AM</ServiceID><ShipmentDate>2018-10-26</ShipmentDate><ExpectedDeliveryDate>2018-11-01</ExpectedDeliveryDate><EstimatedTransitDays>4</EstimatedTransitDays><BasePrice>55.4</BasePrice><Surcharges><Surcharge><Amount>6.65</Amount><Type>Fuel</Type><Description>Fuel</Description></Surcharge></Surcharges><Taxes><Tax><Amount>0</Amount><Type>PSTQST</Type><Description>PST/QST</Description></Tax><Tax><Amount>7.45</Amount><Type>HST</Type><Description>HST</Description></Tax><Tax><Amount>0</Amount><Type>GST</Type><Description>GST</Description></Tax></Taxes><OptionPrices i:nil="true"/><TotalPrice>69.5</TotalPrice></ShipmentEstimate><ShipmentEstimate><ServiceID>PurolatorGround</ServiceID><ShipmentDate>2018-10-26</ShipmentDate><ExpectedDeliveryDate>2018-11-01</ExpectedDeliveryDate><EstimatedTransitDays>4</EstimatedTransitDays><BasePrice>44.8</BasePrice><Surcharges><Surcharge><Amount>5.38</Amount><Type>Fuel</Type><Description>Fuel</Description></Surcharge></Surcharges><Taxes><Tax><Amount>0</Amount><Type>PSTQST</Type><Description>PST/QST</Description></Tax><Tax><Amount>6.02</Amount><Type>HST</Type><Description>HST</Description></Tax><Tax><Amount>0</Amount><Type>GST</Type><Description>GST</Description></Tax></Taxes><OptionPrices i:nil="true"/><TotalPrice>56.2</TotalPrice></ShipmentEstimate></ShipmentEstimates></GetQuickEstimateResponse></s:Body></s:Envelope>
 **/

/*
 * EXPECTED RESULTS from PWS
{ ResponseInformation: { Errors: null },
  ShipmentEstimates:
   { ShipmentEstimate:
      [ { ServiceID: 'PurolatorExpress9AM',
          ShipmentDate: '2018-10-26',
          ExpectedDeliveryDate: '2018-10-29',
          EstimatedTransitDays: 1,
          BasePrice: '104.6',
          Surcharges:
           { Surcharge: [ { Amount: '12.55', Type: 'Fuel', Description: 'Fuel' } ] },
          Taxes:
           { Tax:
              [ { Amount: '0', Type: 'PSTQST', Description: 'PST/QST' },
                { Amount: '14.06', Type: 'HST', Description: 'HST' },
                { Amount: '0', Type: 'GST', Description: 'GST' } ] },
          TotalPrice: '131.21' },
        { ServiceID: 'PurolatorExpress10:30AM',
          ShipmentDate: '2018-10-26',
          ExpectedDeliveryDate: '2018-10-29',
          EstimatedTransitDays: 1,
          BasePrice: '82.85',
          Surcharges:
           { Surcharge: [ { Amount: '9.94', Type: 'Fuel', Description: 'Fuel' } ] },
          Taxes:
           { Tax:
              [ { Amount: '0', Type: 'PSTQST', Description: 'PST/QST' },
                { Amount: '11.13', Type: 'HST', Description: 'HST' },
                { Amount: '0', Type: 'GST', Description: 'GST' } ] },
          TotalPrice: '103.92' },
        { ServiceID: 'PurolatorExpress12PM',
          ShipmentDate: '2018-10-26',
          ExpectedDeliveryDate: '2018-10-29',
          EstimatedTransitDays: 1,
          BasePrice: '75.5',
          Surcharges:
           { Surcharge: [ { Amount: '9.06', Type: 'Fuel', Description: 'Fuel' } ] },
          Taxes:
           { Tax:
              [ { Amount: '0', Type: 'PSTQST', Description: 'PST/QST' },
                { Amount: '10.15', Type: 'HST', Description: 'HST' },
                { Amount: '0', Type: 'GST', Description: 'GST' } ] },
          TotalPrice: '94.71' },
        { ServiceID: 'PurolatorExpress',
          ShipmentDate: '2018-10-26',
          ExpectedDeliveryDate: '2018-10-29',
          EstimatedTransitDays: 1,
          BasePrice: '66.65',
          Surcharges:
           { Surcharge: [ { Amount: '8', Type: 'Fuel', Description: 'Fuel' } ] },
          Taxes:
           { Tax:
              [ { Amount: '0', Type: 'PSTQST', Description: 'PST/QST' },
                { Amount: '8.96', Type: 'HST', Description: 'HST' },
                { Amount: '0', Type: 'GST', Description: 'GST' } ] },
          TotalPrice: '83.61' },
        { ServiceID: 'PurolatorGround9AM',
          ShipmentDate: '2018-10-26',
          ExpectedDeliveryDate: '2018-11-01',
          EstimatedTransitDays: 4,
          BasePrice: '67.95',
          Surcharges:
           { Surcharge: [ { Amount: '8.15', Type: 'Fuel', Description: 'Fuel' } ] },
          Taxes:
           { Tax:
              [ { Amount: '0', Type: 'PSTQST', Description: 'PST/QST' },
                { Amount: '9.13', Type: 'HST', Description: 'HST' },
                { Amount: '0', Type: 'GST', Description: 'GST' } ] },
          TotalPrice: '85.23' },
        { ServiceID: 'PurolatorGround10:30AM',
          ShipmentDate: '2018-10-26',
          ExpectedDeliveryDate: '2018-11-01',
          EstimatedTransitDays: 4,
          BasePrice: '55.4',
          Surcharges:
           { Surcharge: [ { Amount: '6.65', Type: 'Fuel', Description: 'Fuel' } ] },
          Taxes:
           { Tax:
              [ { Amount: '0', Type: 'PSTQST', Description: 'PST/QST' },
                { Amount: '7.45', Type: 'HST', Description: 'HST' },
                { Amount: '0', Type: 'GST', Description: 'GST' } ] },
          TotalPrice: '69.5' },
        { ServiceID: 'PurolatorGround',
          ShipmentDate: '2018-10-26',
          ExpectedDeliveryDate: '2018-11-01',
          EstimatedTransitDays: 4,
          BasePrice: '44.8',
          Surcharges:
           { Surcharge: [ { Amount: '5.38', Type: 'Fuel', Description: 'Fuel' } ] },
          Taxes:
           { Tax:
              [ { Amount: '0', Type: 'PSTQST', Description: 'PST/QST' },
                { Amount: '6.02', Type: 'HST', Description: 'HST' },
                { Amount: '0', Type: 'GST', Description: 'GST' } ] },
          TotalPrice: '56.2' } ] } }
 */
