/**
 *  Purolator Public Web Services Example Code
 *
 *  Requires    : Installation of soap module dependency
 *              : Local copy of the Service Availability WSDL
 *              : Valid Development/Production Key and Password
 *
 *  This example covers the creation of a proper SOAP Client (including envelope
 *  headers) to communicate with the Pickup Web Service.
 */
const soap = require('soap');
const utils = require('../utils');
const path = require('path');
const account = require('../credentials.json');

const API_KEY = account.key;
const API_PASSWORD = account.password;
const ACCOUNT_NUMBER = account.account;
const BILLING_ACCOUNT = account.billingAccount;
const USER_TOKEN = account.userToken;

function createPWSSOAPClient(headers) {

	const wsdlFile = 'PickUpService.wsdl';
	// NOTE: The API version for this service is 1.2, therefore we need to use v1 datatypes
	const namespace = 'http://purolator.com/pws/datatypes/v1';

	const environment = account.production ? 'production' : 'development';
	const pathToWSDL = path.join(__dirname, '../../wsdl', environment, wsdlFile);

	const options = {
		envelopeKey: 'SOAP-ENV',
	};

	return soap
		.createClientAsync(pathToWSDL, options)
		.then(client => {

			client.setSecurity(new soap.BasicAuthSecurity(API_KEY, API_PASSWORD));
			client.addSoapHeader({ RequestContext: headers }, '', 'ns1', '');

			client.wsdl.definitions.xmlns.ns1 = namespace;
			client.wsdl.xmlnsInEnvelope = client.wsdl._xmlnsMap();

			return client;
		});
}

/**
 * VoidPickUp Example
 */

const headers = {
	Version: '1.2', // NOTE: The version number on the namespace must match this #
	Language: 'en',
	GroupID: 'xxx',
	RequestReference: 'VoidPickUp example',
	// Uncomment this for Commercial applications
	// UserToken: USER_TOKEN
};

createPWSSOAPClient(headers)
	.then(client => {

		const request = {
			PickUpConfirmationNumber: '00017464'
		};

		return client.VoidPickUpAsync(request);
	})
	.then(([result, rawResponse, soapheader, rawRequest]) => {
		utils.log(rawRequest);
		utils.log(rawResponse);
		utils.log(result);
	})
	.catch(error => {
		console.log(error);
	});

/**
 * SOAP Request Envelope (Request Made from the SOAP Client)
 * <?xml version="1.0" encoding="UTF-8"?>
 * <?xml version="1.0" encoding="utf-8"?><SOAP-ENV:Envelope xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"  xmlns:wsu="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd" xmlns:tns="http://purolator.com/pws/service/v1" xmlns:msc="http://schemas.microsoft.com/ws/2005/12/wsdl/contract" xmlns:q1="http://schemas.microsoft.com/2003/10/Serialization/Arrays" xmlns:q2="http://schemas.datacontract.org/2004/07/Microsoft.Practices.EnterpriseLibrary.Validation.Integration.WCF" xmlns:q3="http://purolator.com/pws/datatypes/v1" xmlns:q4="http://purolator.com/pws/datatypes/v1" xmlns:q5="http://purolator.com/pws/datatypes/v1" xmlns:q6="http://purolator.com/pws/datatypes/v1" xmlns:q7="http://www.microsoft.com/practices/EnterpriseLibrary/2007/01/wcf/validation" xmlns:q8="http://purolator.com/pws/datatypes/v1" xmlns:q9="http://purolator.com/pws/datatypes/v1" xmlns:q10="http://purolator.com/pws/datatypes/v1" xmlns:q11="http://purolator.com/pws/datatypes/v1" xmlns:q12="http://www.microsoft.com/practices/EnterpriseLibrary/2007/01/wcf/validation" xmlns:q13="http://purolator.com/pws/datatypes/v1" xmlns:q14="http://purolator.com/pws/datatypes/v1" xmlns:q15="http://purolator.com/pws/datatypes/v1" xmlns:q16="http://purolator.com/pws/datatypes/v1" xmlns:q17="http://www.microsoft.com/practices/EnterpriseLibrary/2007/01/wcf/validation" xmlns:q18="http://purolator.com/pws/datatypes/v1" xmlns:q19="http://purolator.com/pws/datatypes/v1" xmlns:q20="http://purolator.com/pws/datatypes/v1" xmlns:q21="http://purolator.com/pws/datatypes/v1" xmlns:q22="http://www.microsoft.com/practices/EnterpriseLibrary/2007/01/wcf/validation" xmlns:q23="http://purolator.com/pws/datatypes/v1" xmlns:q24="http://purolator.com/pws/datatypes/v1" xmlns:q25="http://purolator.com/pws/datatypes/v1" xmlns:q26="http://purolator.com/pws/datatypes/v1" xmlns:q27="http://www.microsoft.com/practices/EnterpriseLibrary/2007/01/wcf/validation" xmlns:ns1="http://purolator.com/pws/datatypes/v1"><SOAP-ENV:Header><ns1:RequestContext><ns1:Version>1.2</ns1:Version><ns1:Language>en</ns1:Language><ns1:GroupID>xxx</ns1:GroupID><ns1:RequestReference>VoidPickUp example</ns1:RequestReference></ns1:RequestContext></SOAP-ENV:Header><SOAP-ENV:Body><q8:VoidPickUpRequest xmlns:q8="http://purolator.com/pws/datatypes/v1" xmlns="http://purolator.com/pws/datatypes/v1"><q8:PickUpConfirmationNumber>00017464</q8:PickUpConfirmationNumber></q8:VoidPickUpRequest></SOAP-ENV:Body></SOAP-ENV:Envelope>
 **/

/**
 * SOAP Response Envelope (Request Returned from the Web Service)
 * <s:Envelope xmlns:s="http://schemas.xmlsoap.org/soap/envelope/"><s:Header><h:ResponseContext xmlns:h="http://purolator.com/pws/datatypes/v1" xmlns:i="http://www.w3.org/2001/XMLSchema-instance"><h:ResponseReference>VoidPickUp example</h:ResponseReference></h:ResponseContext></s:Header><s:Body><VoidPickUpResponse xmlns="http://purolator.com/pws/datatypes/v1" xmlns:i="http://www.w3.org/2001/XMLSchema-instance"><ResponseInformation i:nil="true"/><PickUpVoided>true</PickUpVoided></VoidPickUpResponse></s:Body></s:Envelope>
 **/

/*
 * EXPECTED RESULTS from PWS
{ PickUpVoided: true }
 */
