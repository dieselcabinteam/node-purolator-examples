/**
 *  Purolator Public Web Services Example Code
 *
 *  Requires    : Installation of soap module dependency
 *              : Local copy of the Service Availability WSDL
 *              : Valid Development/Production Key and Password
 *
 *  This example covers the creation of a proper SOAP Client (including envelope
 *  headers) to communicate with the Pickup Web Service.
 */
const soap = require('soap');
const utils = require('../utils');
const path = require('path');
const account = require('../credentials.json');

const API_KEY = account.key;
const API_PASSWORD = account.password;
const ACCOUNT_NUMBER = account.account;
const BILLING_ACCOUNT = account.billingAccount;
const USER_TOKEN = account.userToken;

function createPWSSOAPClient(headers) {

	const wsdlFile = 'PickUpService.wsdl';
	// NOTE: The API version for this service is 1.2, therefore we need to use v1 datatypes
	const namespace = 'http://purolator.com/pws/datatypes/v1';

	const environment = account.production ? 'production' : 'development';
	const pathToWSDL = path.join(__dirname, '../../wsdl', environment, wsdlFile);

	const options = {
		envelopeKey: 'SOAP-ENV',
	};

	return soap
		.createClientAsync(pathToWSDL, options)
		.then(client => {

			client.setSecurity(new soap.BasicAuthSecurity(API_KEY, API_PASSWORD));
			client.addSoapHeader({ RequestContext: headers }, '', 'ns1', '');

			client.wsdl.definitions.xmlns.ns1 = namespace;
			client.wsdl.xmlnsInEnvelope = client.wsdl._xmlnsMap();

			return client;
		});
}

/**
 * GetPickUpHistory Example
 */

const headers = {
	Version: '1.2', // NOTE: The version number on the namespace must match this #
	Language: 'en',
	GroupID: 'xxx',
	RequestReference: 'GetPickUpHistory example',
	// Uncomment this for Commercial applications
	// UserToken: USER_TOKEN
};

createPWSSOAPClient(headers)
	.then(client => {

		const request = {
			PickUpHistorySearchCriteria: {
				FromDate: "2018-10-26",
				ToDate: "2018-10-26",
				ConfirmationNumber: "",
				AccountNumber: "",
				Status: "",
				MaxNumOfRecords: 10,
				SortColumn: "",
				SortDirection: "",
			}
		};

		return client.GetPickUpHistoryAsync(request);
	})
	.then(([result, rawResponse, soapheader, rawRequest]) => {
		utils.log(rawRequest);
		utils.log(rawResponse);
		utils.log(result);
	})
	.catch(error => {
		console.log(error);
	});

/**
 * SOAP Request Envelope (Request Made from the SOAP Client)
 * <?xml version="1.0" encoding="UTF-8"?>
 * <?xml version="1.0" encoding="utf-8"?><SOAP-ENV:Envelope xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"  xmlns:wsu="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd" xmlns:tns="http://purolator.com/pws/service/v1" xmlns:msc="http://schemas.microsoft.com/ws/2005/12/wsdl/contract" xmlns:q1="http://schemas.microsoft.com/2003/10/Serialization/Arrays" xmlns:q2="http://schemas.datacontract.org/2004/07/Microsoft.Practices.EnterpriseLibrary.Validation.Integration.WCF" xmlns:q3="http://purolator.com/pws/datatypes/v1" xmlns:q4="http://purolator.com/pws/datatypes/v1" xmlns:q5="http://purolator.com/pws/datatypes/v1" xmlns:q6="http://purolator.com/pws/datatypes/v1" xmlns:q7="http://www.microsoft.com/practices/EnterpriseLibrary/2007/01/wcf/validation" xmlns:q8="http://purolator.com/pws/datatypes/v1" xmlns:q9="http://purolator.com/pws/datatypes/v1" xmlns:q10="http://purolator.com/pws/datatypes/v1" xmlns:q11="http://purolator.com/pws/datatypes/v1" xmlns:q12="http://www.microsoft.com/practices/EnterpriseLibrary/2007/01/wcf/validation" xmlns:q13="http://purolator.com/pws/datatypes/v1" xmlns:q14="http://purolator.com/pws/datatypes/v1" xmlns:q15="http://purolator.com/pws/datatypes/v1" xmlns:q16="http://purolator.com/pws/datatypes/v1" xmlns:q17="http://www.microsoft.com/practices/EnterpriseLibrary/2007/01/wcf/validation" xmlns:q18="http://purolator.com/pws/datatypes/v1" xmlns:q19="http://purolator.com/pws/datatypes/v1" xmlns:q20="http://purolator.com/pws/datatypes/v1" xmlns:q21="http://purolator.com/pws/datatypes/v1" xmlns:q22="http://www.microsoft.com/practices/EnterpriseLibrary/2007/01/wcf/validation" xmlns:q23="http://purolator.com/pws/datatypes/v1" xmlns:q24="http://purolator.com/pws/datatypes/v1" xmlns:q25="http://purolator.com/pws/datatypes/v1" xmlns:q26="http://purolator.com/pws/datatypes/v1" xmlns:q27="http://www.microsoft.com/practices/EnterpriseLibrary/2007/01/wcf/validation" xmlns:ns1="http://purolator.com/pws/datatypes/v1"><SOAP-ENV:Header><ns1:RequestContext><ns1:Version>1.2</ns1:Version><ns1:Language>en</ns1:Language><ns1:GroupID>xxx</ns1:GroupID><ns1:RequestReference>GetPickUpHistory example</ns1:RequestReference></ns1:RequestContext></SOAP-ENV:Header><SOAP-ENV:Body><q18:GetPickUpHistoryRequest xmlns:q18="http://purolator.com/pws/datatypes/v1" xmlns="http://purolator.com/pws/datatypes/v1"><q18:PickUpHistorySearchCriteria><q18:FromDate>2018-10-26</q18:FromDate><q18:ToDate>2018-10-26</q18:ToDate><q18:ConfirmationNumber></q18:ConfirmationNumber><q18:AccountNumber></q18:AccountNumber><q18:Status></q18:Status><q18:MaxNumOfRecords>10</q18:MaxNumOfRecords><q18:SortColumn></q18:SortColumn><q18:SortDirection></q18:SortDirection></q18:PickUpHistorySearchCriteria></q18:GetPickUpHistoryRequest></SOAP-ENV:Body></SOAP-ENV:Envelope>
 **/

/**
 * SOAP Response Envelope (Request Returned from the Web Service)
 * <s:Envelope xmlns:s="http://schemas.xmlsoap.org/soap/envelope/"><s:Header><h:ResponseContext xmlns:h="http://purolator.com/pws/datatypes/v1" xmlns:i="http://www.w3.org/2001/XMLSchema-instance"><h:ResponseReference>GetPickUpHistory example</h:ResponseReference></h:ResponseContext></s:Header><s:Body><GetPickUpHistoryResponse xmlns="http://purolator.com/pws/datatypes/v1" xmlns:i="http://www.w3.org/2001/XMLSchema-instance"><ResponseInformation i:nil="true"/><PickUpDetailList><PickUpDetail><BillingAccountNumber i:nil="true"/><PartnerId i:nil="true"/><ConfirmationNumber>00017464</ConfirmationNumber><PickupStatus>Scheduled</PickupStatus><PickupType>Regular</PickupType><PickupInstruction><Date>2010-02-25</Date><AnyTimeAfter>1200</AnyTimeAfter><UntilTime>1800</UntilTime><TotalWeight i:nil="true"/><TotalPieces>0</TotalPieces><BoxesIndicator i:nil="true"/><PickUpLocation>BackDoor</PickUpLocation><AdditionalInstructions i:nil="true"/><SupplyRequestCodes i:nil="true"/><TrailerAccessible>false</TrailerAccessible><LoadingDockAvailable>false</LoadingDockAvailable><ShipmentOnSkids>false</ShipmentOnSkids><NumberOfSkids>0</NumberOfSkids></PickupInstruction><Address><Name i:nil="true"/><Company>A COMPANY</Company><Department/><StreetNumber>6</StreetNumber><StreetSuffix/><StreetName>GOODWOOD</StreetName><StreetType>DR</StreetType><StreetDirection/><Suite i:nil="true"/><Floor i:nil="true"/><StreetAddress2 i:nil="true"/><StreetAddress3 i:nil="true"/><City>MISSISSAUGA</City><Province>ON</Province><Country>CA</Country><PostalCode>L9Z1A1</PostalCode><PhoneNumber><CountryCode>1</CountryCode><AreaCode>905</AreaCode><Phone>5558888</Phone><Extension>8888</Extension></PhoneNumber><FaxNumber i:nil="true"/></Address><ShipmentSummary><ShipmentSummaryDetails><ShipmentSummaryDetail><DestinationCode>DOM</DestinationCode><ModeOfTransport/><TotalPieces>2</TotalPieces><TotalWeight><Value>7.0</Value><WeightUnit>kg</WeightUnit></TotalWeight></ShipmentSummaryDetail><ShipmentSummaryDetail><DestinationCode>INT</DestinationCode><ModeOfTransport/><TotalPieces>0</TotalPieces><TotalWeight><Value>0</Value><WeightUnit>kg</WeightUnit></TotalWeight></ShipmentSummaryDetail><ShipmentSummaryDetail><DestinationCode>USA</DestinationCode><ModeOfTransport/><TotalPieces>0</TotalPieces><TotalWeight><Value>0</Value><WeightUnit>kg</WeightUnit></TotalWeight></ShipmentSummaryDetail></ShipmentSummaryDetails></ShipmentSummary><NotificationEmails xmlns:a="http://schemas.microsoft.com/2003/10/Serialization/Arrays"><a:string>john.smith@example.com</a:string></NotificationEmails><ValidatePickUpAddressDetails>false</ValidatePickUpAddressDetails></PickUpDetail></PickUpDetailList></GetPickUpHistoryResponse></s:Body></s:Envelope>
 **/

/*
 * EXPECTED RESULTS from PWS
{ PickUpDetailList:
   { PickUpDetail:
      [ { ConfirmationNumber: '00017464',
          PickupStatus: 'Scheduled',
          PickupType: 'Regular',
          PickupInstruction:
           { Date: '2010-02-25',
             AnyTimeAfter: '1200',
             UntilTime: '1800',
             TotalPieces: 0,
             PickUpLocation: 'BackDoor',
             TrailerAccessible: false,
             LoadingDockAvailable: false,
             ShipmentOnSkids: false,
             NumberOfSkids: 0 },
          Address:
           { Company: 'A COMPANY',
             Department: '',
             StreetNumber: '6',
             StreetSuffix: '',
             StreetName: 'GOODWOOD',
             StreetType: 'DR',
             StreetDirection: '',
             City: 'MISSISSAUGA',
             Province: 'ON',
             Country: 'CA',
             PostalCode: 'L9Z1A1',
             PhoneNumber:
              { CountryCode: '1',
                AreaCode: '905',
                Phone: '5558888',
                Extension: '8888' } },
          ShipmentSummary:
           { ShipmentSummaryDetails:
              { ShipmentSummaryDetail:
                 [ { DestinationCode: 'DOM',
                     ModeOfTransport: '',
                     TotalPieces: 2,
                     TotalWeight: { Value: '7.0', WeightUnit: 'kg' } },
                   { DestinationCode: 'INT',
                     ModeOfTransport: '',
                     TotalPieces: 0,
                     TotalWeight: { Value: '0', WeightUnit: 'kg' } },
                   { DestinationCode: 'USA',
                     ModeOfTransport: '',
                     TotalPieces: 0,
                     TotalWeight: { Value: '0', WeightUnit: 'kg' } } ] } },
          NotificationEmails: { string: [ 'john.smith@example.com' ] },
          ValidatePickUpAddressDetails: 'false' } ] } }
 */
