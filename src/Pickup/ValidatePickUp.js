/**
 *  Purolator Public Web Services Example Code
 *
 *  Requires    : Installation of soap module dependency
 *              : Local copy of the Service Availability WSDL
 *              : Valid Development/Production Key and Password
 *
 *  This example covers the creation of a proper SOAP Client (including envelope
 *  headers) to communicate with the Pickup Web Service.
 */
const soap = require('soap');
const utils = require('../utils');
const path = require('path');
const account = require('../credentials.json');

const API_KEY = account.key;
const API_PASSWORD = account.password;
const ACCOUNT_NUMBER = account.account;
const BILLING_ACCOUNT = account.billingAccount;
const USER_TOKEN = account.userToken;

function createPWSSOAPClient(headers) {

	const wsdlFile = 'PickUpService.wsdl';
	// NOTE: The API version for this service is 1.2, therefore we need to use v1 datatypes
	const namespace = 'http://purolator.com/pws/datatypes/v1';

	const environment = account.production ? 'production' : 'development';
	const pathToWSDL = path.join(__dirname, '../../wsdl', environment, wsdlFile);

	const options = {
		envelopeKey: 'SOAP-ENV',
	};

	return soap
		.createClientAsync(pathToWSDL, options)
		.then(client => {

			client.setSecurity(new soap.BasicAuthSecurity(API_KEY, API_PASSWORD));
			client.addSoapHeader({ RequestContext: headers }, '', 'ns1', '');

			client.wsdl.definitions.xmlns.ns1 = namespace;
			client.wsdl.xmlnsInEnvelope = client.wsdl._xmlnsMap();

			return client;
		});
}

/**
 * ValidatePickUp Example
 */

const headers = {
	Version: '1.2', // NOTE: The version number on the namespace must match this #
	Language: 'en',
	GroupID: 'xxx',
	RequestReference: 'ValidatePickUp example',
	// Uncomment this for Commercial applications
	// UserToken: USER_TOKEN
};

createPWSSOAPClient(headers)
	.then(client => {

		const request = {
			BillingAccountNumber: BILLING_ACCOUNT,
			PartnerID: '',
			PickupInstruction: {
				Date: '2018-10-27',
				AnyTimeAfter: '1200',
				UntilTime: '1500',
				TotalWeight: {
					Value: 1,
					WeightUnit: 'kg',
				},
				TotalPieces: 1,
				BoxIndicator: '',
				PickUpLocation: 'BackDoor',
				AdditionalInstructions: '',
				SupplyRequestCodes: {
					SupplyRequestCode: 'PuroletterExpressEnvelope'
				},
				LoadingDockAvailable: false,
				TrailerAccessible: false,
				ShipmentOnSkids: false,
				NumberOfSkids: 0
			},
			Address: {
				Name: 'PWS User',
				Company: 'Company',
				Department: 'Department',
				StreetNumber: '5280',
				StreetSuffix: '',
				StreetName: 'Solar',
				StreetType: 'Drive',
				StreetDirection: '',
				Suite: '',
				Floor: '',
				StreetAddress2: '',
				StreetAddress3: '',
				City: 'Mississauga',
				Province: 'ON',
				Country: 'CA',
				PostalCode: 'L4W5M8',
				PhoneNumber: {
					CountryCode: '1',
					AreaCode: '905',
					Phone: '7128101',
				}
			},
			NotificationEmails: {
				NotificationEmail: 'your_email@email.com'
			},
			ShipmentSummary: {
				ShipmentSummaryDetails: {
					ShipmentSummaryDetail: [{
						DestinationCode: 'DOM',
						ModeOfTransport: 'Ground',
						TotalPieces: '70',
						TotalWeight: {
							Value: '100',
							WeightUnit: 'kg',
						}
					}]
				}
			}
		};

		return client.ValidatePickUpAsync(request);
	})
	.then(([result, rawResponse, soapheader, rawRequest]) => {
		utils.log(rawRequest);
		utils.log(rawResponse);
		utils.log(result);
	})
	.catch(error => {
		console.log(error);
	});

/**
 * SOAP Request Envelope (Request Made from the SOAP Client)
 * <?xml version="1.0" encoding="UTF-8"?>
 * <?xml version="1.0" encoding="utf-8"?><SOAP-ENV:Envelope xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"  xmlns:wsu="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd" xmlns:tns="http://purolator.com/pws/service/v1" xmlns:msc="http://schemas.microsoft.com/ws/2005/12/wsdl/contract" xmlns:q1="http://schemas.microsoft.com/2003/10/Serialization/Arrays" xmlns:q2="http://schemas.datacontract.org/2004/07/Microsoft.Practices.EnterpriseLibrary.Validation.Integration.WCF" xmlns:q3="http://purolator.com/pws/datatypes/v1" xmlns:q4="http://purolator.com/pws/datatypes/v1" xmlns:q5="http://purolator.com/pws/datatypes/v1" xmlns:q6="http://purolator.com/pws/datatypes/v1" xmlns:q7="http://www.microsoft.com/practices/EnterpriseLibrary/2007/01/wcf/validation" xmlns:q8="http://purolator.com/pws/datatypes/v1" xmlns:q9="http://purolator.com/pws/datatypes/v1" xmlns:q10="http://purolator.com/pws/datatypes/v1" xmlns:q11="http://purolator.com/pws/datatypes/v1" xmlns:q12="http://www.microsoft.com/practices/EnterpriseLibrary/2007/01/wcf/validation" xmlns:q13="http://purolator.com/pws/datatypes/v1" xmlns:q14="http://purolator.com/pws/datatypes/v1" xmlns:q15="http://purolator.com/pws/datatypes/v1" xmlns:q16="http://purolator.com/pws/datatypes/v1" xmlns:q17="http://www.microsoft.com/practices/EnterpriseLibrary/2007/01/wcf/validation" xmlns:q18="http://purolator.com/pws/datatypes/v1" xmlns:q19="http://purolator.com/pws/datatypes/v1" xmlns:q20="http://purolator.com/pws/datatypes/v1" xmlns:q21="http://purolator.com/pws/datatypes/v1" xmlns:q22="http://www.microsoft.com/practices/EnterpriseLibrary/2007/01/wcf/validation" xmlns:q23="http://purolator.com/pws/datatypes/v1" xmlns:q24="http://purolator.com/pws/datatypes/v1" xmlns:q25="http://purolator.com/pws/datatypes/v1" xmlns:q26="http://purolator.com/pws/datatypes/v1" xmlns:q27="http://www.microsoft.com/practices/EnterpriseLibrary/2007/01/wcf/validation" xmlns:ns1="http://purolator.com/pws/datatypes/v1"><SOAP-ENV:Header><ns1:RequestContext><ns1:Version>1.2</ns1:Version><ns1:Language>en</ns1:Language><ns1:GroupID>xxx</ns1:GroupID><ns1:RequestReference>ValidatePickUp example</ns1:RequestReference></ns1:RequestContext></SOAP-ENV:Header><SOAP-ENV:Body><q13:ValidatePickUpRequest xmlns:q13="http://purolator.com/pws/datatypes/v1" xmlns="http://purolator.com/pws/datatypes/v1"><q13:BillingAccountNumber>9999999999</q13:BillingAccountNumber><q13:PartnerID></q13:PartnerID><q13:PickupInstruction><q13:Date>2018-10-27</q13:Date><q13:AnyTimeAfter>1200</q13:AnyTimeAfter><q13:UntilTime>1500</q13:UntilTime><q13:TotalWeight><q13:Value>1</q13:Value><q13:WeightUnit>kg</q13:WeightUnit></q13:TotalWeight><q13:TotalPieces>1</q13:TotalPieces><q13:BoxIndicator></q13:BoxIndicator><q13:PickUpLocation>BackDoor</q13:PickUpLocation><q13:AdditionalInstructions></q13:AdditionalInstructions><q13:SupplyRequestCodes><q13:SupplyRequestCode>PuroletterExpressEnvelope</q13:SupplyRequestCode></q13:SupplyRequestCodes><q13:LoadingDockAvailable>false</q13:LoadingDockAvailable><q13:TrailerAccessible>false</q13:TrailerAccessible><q13:ShipmentOnSkids>false</q13:ShipmentOnSkids><q13:NumberOfSkids>0</q13:NumberOfSkids></q13:PickupInstruction><q13:Address><q13:Name>PWS User</q13:Name><q13:Company>Company</q13:Company><q13:Department>Department</q13:Department><q13:StreetNumber>5280</q13:StreetNumber><q13:StreetSuffix></q13:StreetSuffix><q13:StreetName>Solar</q13:StreetName><q13:StreetType>Drive</q13:StreetType><q13:StreetDirection></q13:StreetDirection><q13:Suite></q13:Suite><q13:Floor></q13:Floor><q13:StreetAddress2></q13:StreetAddress2><q13:StreetAddress3></q13:StreetAddress3><q13:City>Mississauga</q13:City><q13:Province>ON</q13:Province><q13:Country>CA</q13:Country><q13:PostalCode>L4W5M8</q13:PostalCode><q13:PhoneNumber><q13:CountryCode>1</q13:CountryCode><q13:AreaCode>905</q13:AreaCode><q13:Phone>7128101</q13:Phone></q13:PhoneNumber></q13:Address><q13:NotificationEmails><q13:NotificationEmail>your_email@email.com</q13:NotificationEmail></q13:NotificationEmails><q13:ShipmentSummary><q13:ShipmentSummaryDetails><q13:ShipmentSummaryDetail><q13:DestinationCode>DOM</q13:DestinationCode><q13:ModeOfTransport>Ground</q13:ModeOfTransport><q13:TotalPieces>70</q13:TotalPieces><q13:TotalWeight><q13:Value>100</q13:Value><q13:WeightUnit>kg</q13:WeightUnit></q13:TotalWeight></q13:ShipmentSummaryDetail></q13:ShipmentSummaryDetails></q13:ShipmentSummary></q13:ValidatePickUpRequest></SOAP-ENV:Body></SOAP-ENV:Envelope>
 **/

/**
 * SOAP Response Envelope (Request Returned from the Web Service)
 * <s:Envelope xmlns:s="http://schemas.xmlsoap.org/soap/envelope/"><s:Header><h:ResponseContext xmlns:h="http://purolator.com/pws/datatypes/v1" xmlns:i="http://www.w3.org/2001/XMLSchema-instance"><h:ResponseReference>ValidatePickUp example</h:ResponseReference></h:ResponseContext></s:Header><s:Body><ValidatePickUpResponse xmlns="http://purolator.com/pws/datatypes/v1" xmlns:i="http://www.w3.org/2001/XMLSchema-instance"><ResponseInformation i:nil="true"/><IsBulkdRequired>false</IsBulkdRequired><CutOffTime>17:30</CutOffTime><CutOffWindow>60</CutOffWindow><BulkMaxWeight>0</BulkMaxWeight><BulkMaxPackages>0</BulkMaxPackages></ValidatePickUpResponse></s:Body></s:Envelope>
 **/

/*
 * EXPECTED RESULTS from PWS
{ IsBulkdRequired: false,
  CutOffTime: '17:30',
  CutOffWindow: 60,
  BulkMaxWeight: '0',
  BulkMaxPackages: 0 }
 */
