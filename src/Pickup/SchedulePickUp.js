/**
 *  Purolator Public Web Services Example Code
 *
 *  Requires    : Installation of soap module dependency
 *              : Local copy of the Service Availability WSDL
 *              : Valid Development/Production Key and Password
 *
 *  This example covers the creation of a proper SOAP Client (including envelope
 *  headers) to communicate with the Pickup Web Service.
 */
const soap = require('soap');
const utils = require('../utils');
const path = require('path');
const account = require('../credentials.json');

const API_KEY = account.key;
const API_PASSWORD = account.password;
const ACCOUNT_NUMBER = account.account;
const BILLING_ACCOUNT = account.billingAccount;
const USER_TOKEN = account.userToken;

function createPWSSOAPClient(headers) {

	const wsdlFile = 'PickUpService.wsdl';
	// NOTE: The API version for this service is 1.2, therefore we need to use v1 datatypes
	const namespace = 'http://purolator.com/pws/datatypes/v1';

	const environment = account.production ? 'production' : 'development';
	const pathToWSDL = path.join(__dirname, '../../wsdl', environment, wsdlFile);

	const options = {
		envelopeKey: 'SOAP-ENV',
	};

	return soap
		.createClientAsync(pathToWSDL, options)
		.then(client => {

			client.setSecurity(new soap.BasicAuthSecurity(API_KEY, API_PASSWORD));
			client.addSoapHeader({ RequestContext: headers }, '', 'ns1', '');

			client.wsdl.definitions.xmlns.ns1 = namespace;
			client.wsdl.xmlnsInEnvelope = client.wsdl._xmlnsMap();

			return client;
		});
}

/**
 * SchedulePickUp Example
 */

const headers = {
	Version: '1.2', // NOTE: The version number on the namespace must match this #
	Language: 'en',
	GroupID: 'xxx',
	RequestReference: 'SchedulePickUp example',
	// Uncomment this for Commercial applications
	// UserToken: USER_TOKEN
};

createPWSSOAPClient(headers)
	.then(client => {

		const request = {
			BillingAccountNumber: BILLING_ACCOUNT,
			PartnerID: '',
			PickupInstruction: {
				Date: '2018-10-27',
				AnyTimeAfter: '1200',
				UntilTime: '1500',
				TotalWeight: {
					Value: 1,
					WeightUnit: 'kg',
				},
				TotalPieces: 1,
				BoxIndicator: '',
				PickUpLocation: 'BackDoor',
				AdditionalInstructions: '',
				SupplyRequestCodes: {
					SupplyRequestCode: 'PuroletterExpressEnvelope'
				},
				LoadingDockAvailable: false,
				TrailerAccessible: false,
				ShipmentOnSkids: false,
				NumberOfSkids: 0
			},
			Address: {
				Name: 'PWS User',
				Company: 'Company',
				Department: 'Department',
				StreetNumber: '5280',
				StreetSuffix: '',
				StreetName: 'Solar',
				StreetType: 'Drive',
				StreetDirection: '',
				Suite: '',
				Floor: '',
				StreetAddress2: '',
				StreetAddress3: '',
				City: 'Mississauga',
				Province: 'ON',
				Country: 'CA',
				PostalCode: 'L4W5M8',
				PhoneNumber: {
					CountryCode: '1',
					AreaCode: '905',
					Phone: '7128101',
				}
			},
			NotificationEmails: {
				NotificationEmail: 'your_email@email.com'
			},
			ShipmentSummary: {
				ShipmentSummaryDetails: {
					ShipmentSummaryDetail: [{
						DestinationCode: 'DOM',
						ModeOfTransport: 'Ground',
						TotalPieces: '70',
						TotalWeight: {
							Value: '100',
							WeightUnit: 'kg',
						}
					}]
				}
			}
		};

		return client.SchedulePickUpAsync(request);
	})
	.then(([result, rawResponse, soapheader, rawRequest]) => {
		utils.log(rawRequest);
		utils.log(rawResponse);
		utils.log(result);
	})
	.catch(error => {
		console.log(error);
	});

/**
 * SOAP Request Envelope (Request Made from the SOAP Client)
 * <?xml version="1.0" encoding="UTF-8"?>
 * <?xml version="1.0" encoding="utf-8"?><SOAP-ENV:Envelope xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"  xmlns:wsu="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd" xmlns:tns="http://purolator.com/pws/service/v1" xmlns:msc="http://schemas.microsoft.com/ws/2005/12/wsdl/contract" xmlns:q1="http://schemas.microsoft.com/2003/10/Serialization/Arrays" xmlns:q2="http://schemas.datacontract.org/2004/07/Microsoft.Practices.EnterpriseLibrary.Validation.Integration.WCF" xmlns:q3="http://purolator.com/pws/datatypes/v1" xmlns:q4="http://purolator.com/pws/datatypes/v1" xmlns:q5="http://purolator.com/pws/datatypes/v1" xmlns:q6="http://purolator.com/pws/datatypes/v1" xmlns:q7="http://www.microsoft.com/practices/EnterpriseLibrary/2007/01/wcf/validation" xmlns:q8="http://purolator.com/pws/datatypes/v1" xmlns:q9="http://purolator.com/pws/datatypes/v1" xmlns:q10="http://purolator.com/pws/datatypes/v1" xmlns:q11="http://purolator.com/pws/datatypes/v1" xmlns:q12="http://www.microsoft.com/practices/EnterpriseLibrary/2007/01/wcf/validation" xmlns:q13="http://purolator.com/pws/datatypes/v1" xmlns:q14="http://purolator.com/pws/datatypes/v1" xmlns:q15="http://purolator.com/pws/datatypes/v1" xmlns:q16="http://purolator.com/pws/datatypes/v1" xmlns:q17="http://www.microsoft.com/practices/EnterpriseLibrary/2007/01/wcf/validation" xmlns:q18="http://purolator.com/pws/datatypes/v1" xmlns:q19="http://purolator.com/pws/datatypes/v1" xmlns:q20="http://purolator.com/pws/datatypes/v1" xmlns:q21="http://purolator.com/pws/datatypes/v1" xmlns:q22="http://www.microsoft.com/practices/EnterpriseLibrary/2007/01/wcf/validation" xmlns:q23="http://purolator.com/pws/datatypes/v1" xmlns:q24="http://purolator.com/pws/datatypes/v1" xmlns:q25="http://purolator.com/pws/datatypes/v1" xmlns:q26="http://purolator.com/pws/datatypes/v1" xmlns:q27="http://www.microsoft.com/practices/EnterpriseLibrary/2007/01/wcf/validation" xmlns:ns1="http://purolator.com/pws/datatypes/v1"><SOAP-ENV:Header><ns1:RequestContext><ns1:Version>1.2</ns1:Version><ns1:Language>en</ns1:Language><ns1:GroupID>xxx</ns1:GroupID><ns1:RequestReference>SchedulePickUp example</ns1:RequestReference></ns1:RequestContext></SOAP-ENV:Header><SOAP-ENV:Body><q3:SchedulePickUpRequest xmlns:q3="http://purolator.com/pws/datatypes/v1" xmlns="http://purolator.com/pws/datatypes/v1"><q3:BillingAccountNumber>9999999999</q3:BillingAccountNumber><q3:PartnerID></q3:PartnerID><q3:PickupInstruction><q3:Date>2018-10-27</q3:Date><q3:AnyTimeAfter>1200</q3:AnyTimeAfter><q3:UntilTime>1500</q3:UntilTime><q3:TotalWeight><q3:Value>1</q3:Value><q3:WeightUnit>kg</q3:WeightUnit></q3:TotalWeight><q3:TotalPieces>1</q3:TotalPieces><q3:BoxIndicator></q3:BoxIndicator><q3:PickUpLocation>BackDoor</q3:PickUpLocation><q3:AdditionalInstructions></q3:AdditionalInstructions><q3:SupplyRequestCodes><q3:SupplyRequestCode>PuroletterExpressEnvelope</q3:SupplyRequestCode></q3:SupplyRequestCodes><q3:LoadingDockAvailable>false</q3:LoadingDockAvailable><q3:TrailerAccessible>false</q3:TrailerAccessible><q3:ShipmentOnSkids>false</q3:ShipmentOnSkids><q3:NumberOfSkids>0</q3:NumberOfSkids></q3:PickupInstruction><q3:Address><q3:Name>PWS User</q3:Name><q3:Company>Company</q3:Company><q3:Department>Department</q3:Department><q3:StreetNumber>5280</q3:StreetNumber><q3:StreetSuffix></q3:StreetSuffix><q3:StreetName>Solar</q3:StreetName><q3:StreetType>Drive</q3:StreetType><q3:StreetDirection></q3:StreetDirection><q3:Suite></q3:Suite><q3:Floor></q3:Floor><q3:StreetAddress2></q3:StreetAddress2><q3:StreetAddress3></q3:StreetAddress3><q3:City>Mississauga</q3:City><q3:Province>ON</q3:Province><q3:Country>CA</q3:Country><q3:PostalCode>L4W5M8</q3:PostalCode><q3:PhoneNumber><q3:CountryCode>1</q3:CountryCode><q3:AreaCode>905</q3:AreaCode><q3:Phone>7128101</q3:Phone></q3:PhoneNumber></q3:Address><q3:NotificationEmails><q3:NotificationEmail>your_email@email.com</q3:NotificationEmail></q3:NotificationEmails><q3:ShipmentSummary><q3:ShipmentSummaryDetails><q3:ShipmentSummaryDetail><q3:DestinationCode>DOM</q3:DestinationCode><q3:ModeOfTransport>Ground</q3:ModeOfTransport><q3:TotalPieces>70</q3:TotalPieces><q3:TotalWeight><q3:Value>100</q3:Value><q3:WeightUnit>kg</q3:WeightUnit></q3:TotalWeight></q3:ShipmentSummaryDetail></q3:ShipmentSummaryDetails></q3:ShipmentSummary></q3:SchedulePickUpRequest></SOAP-ENV:Body></SOAP-ENV:Envelope>
 **/

/**
 * SOAP Response Envelope (Request Returned from the Web Service)
 * <s:Envelope xmlns:s="http://schemas.xmlsoap.org/soap/envelope/"><s:Header><h:ResponseContext xmlns:h="http://purolator.com/pws/datatypes/v1" xmlns:i="http://www.w3.org/2001/XMLSchema-instance"><h:ResponseReference>SchedulePickUp example</h:ResponseReference></h:ResponseContext></s:Header><s:Body><SchedulePickUpResponse xmlns="http://purolator.com/pws/datatypes/v1" xmlns:i="http://www.w3.org/2001/XMLSchema-instance"><ResponseInformation i:nil="true"/><PickUpConfirmationNumber>00006293</PickUpConfirmationNumber></SchedulePickUpResponse></s:Body></s:Envelope>
 **/

/*
 * EXPECTED RESULTS from PWS
{ PickUpConfirmationNumber: '00006293' }
 */
