/**
 *  Purolator Public Web Services Example Code
 *
 *  Requires    : Installation of soap module dependency
 *              : Local copy of the Service Availability WSDL
 *              : Valid Development/Production Key and Password
 *
 *  This example covers the creation of a proper SOAP Client (including envelope
 *  headers) to communicate with the Shipping Web Service.
 */
const soap = require('soap');
const utils = require('../utils');
const path = require('path');
const account = require('../credentials.json');

const API_KEY = account.key;
const API_PASSWORD = account.password;
const ACCOUNT_NUMBER = account.account;
const BILLING_ACCOUNT = account.billingAccount;
const USER_TOKEN = account.userToken;

function createPWSSOAPClient(headers) {

	const wsdlFile = 'ShippingService.wsdl';
	const namespace = 'http://purolator.com/pws/datatypes/v2';

	const environment = account.production ? 'production' : 'development';
	const pathToWSDL = path.join(__dirname, '../../wsdl', environment, wsdlFile);

	const options = {
		envelopeKey: 'SOAP-ENV',
	};

	return soap
		.createClientAsync(pathToWSDL, options)
		.then(client => {

			client.setSecurity(new soap.BasicAuthSecurity(API_KEY, API_PASSWORD));
			client.addSoapHeader({ RequestContext: headers }, '', 'ns1', '');

			client.wsdl.definitions.xmlns.ns1 = namespace;
			client.wsdl.xmlnsInEnvelope = client.wsdl._xmlnsMap();

			return client;
		});
}

/**
 * ValidateShipment Example
 *
 * Create a 1 piece validate shipment
 */

const headers = {
	Version: '2.1',
	Language: 'en',
	GroupID: 'xxx',
	RequestReference: 'ValidateShipment example',
	// Uncomment this for Commercial applications
	// UserToken: USER_TOKEN
};

createPWSSOAPClient(headers)
	.then(client => {

		const request = {
			Shipment: {
				SenderInformation: {
					Address: {
						Name: 'Aaron Summer',
						StreetNumber: '1234',
						StreetName: 'Main Street',
						City: 'Mississauga',
						Province: 'ON',
						Country: 'CA',
						PostalCode: 'L4W5M8',
						PhoneNumber: {
							CountryCode: '1',
							AreaCode: '905',
							Phone: '5555555',
						}
					}
				},
				ReceiverInformation: {
					Address: {
						Name: 'Aaron Summer',
						StreetNumber: '2245',
						StreetName: 'Douglas Road',
						City: 'Burnaby',
						Province: 'BC',
						Country: 'CA',
						PostalCode: 'V5C5A9',
						PhoneNumber: {
							CountryCode: '1',
							AreaCode: '604',
							Phone: '2982181',
						}
					}
				},
				PackageInformation: {
					ServiceID: 'PurolatorExpress',
					TotalWeight: {
						Value: 40,
						WeightUnit: 'lb',
					},
					TotalPieces: 1,
					PiecesInformation: {
						Piece: [{
							Weight: {
								Value: 40,
								WeightUnit: 'lb',
							},
							Length: {
								Value: 40,
								DimensionUnit: 'in',
							},
							Width: {
								Value: 10,
								DimensionUnit: 'in',
							},
							Height: {
								Value: 2,
								DimensionUnit: 'in',
							},
							Options: {
								OptionIDValuePair: [
									{ ID: 'SpecialHandling', Value: true },
									{ ID: 'SpecialHandlingType', Value: 'LargePackage' }
								]
							}
						}]
					}
				},
				PaymentInformation: {
					PaymentType: 'Sender',
					RegisteredAccountNumber: ACCOUNT_NUMBER,
					BillingAccountNumber: BILLING_ACCOUNT,
				},
				PickupInformation: {
					PickupType: 'DropOff',
				},
				TrackingReferenceInformation: {
					Reference1: 'RMA123'
				},
				ProactiveNotification: {
					RequestorName: 'MyName',
					RequestorEmail: 'test@test.com',
					Subscriptions: {
						Subscription: [{
							Name: 'MyName',
							Email: 'test@test.com',
							NotifyWhenExceptionOccurs: true,
							NotifyWhenDeliveryOccurs: true,
						}]
					}
				}
			}
		};

		return client.ValidateShipmentAsync(request);
	})
	.then(([result, rawResponse, soapheader, rawRequest]) => {
		utils.log(rawRequest);
		utils.log(rawResponse);
		utils.log(result);
	})
	.catch(error => {
		console.log(error);
	});

/**
 * SOAP Request Envelope (Request Made from the SOAP Client)
 * <?xml version="1.0" encoding="UTF-8"?>
 * <?xml version="1.0" encoding="utf-8"?><SOAP-ENV:Envelope xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"  xmlns:tns="http://purolator.com/pws/service/v2" xmlns:msc="http://schemas.microsoft.com/ws/2005/12/wsdl/contract" xmlns:wsu="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd" xmlns:q1="http://schemas.datacontract.org/2004/07/Microsoft.Practices.EnterpriseLibrary.Validation.Integration.WCF" xmlns:q2="http://purolator.com/pws/datatypes/v2" xmlns:q3="http://purolator.com/pws/datatypes/v2" xmlns:q4="http://purolator.com/pws/datatypes/v2" xmlns:q5="http://www.microsoft.com/practices/EnterpriseLibrary/2007/01/wcf/validation" xmlns:q6="http://purolator.com/pws/datatypes/v2" xmlns:q7="http://purolator.com/pws/datatypes/v2" xmlns:q8="http://purolator.com/pws/datatypes/v2" xmlns:q9="http://purolator.com/pws/datatypes/v2" xmlns:q10="http://www.microsoft.com/practices/EnterpriseLibrary/2007/01/wcf/validation" xmlns:q11="http://purolator.com/pws/datatypes/v2" xmlns:q12="http://purolator.com/pws/datatypes/v2" xmlns:q13="http://purolator.com/pws/datatypes/v2" xmlns:q14="http://purolator.com/pws/datatypes/v2" xmlns:q15="http://www.microsoft.com/practices/EnterpriseLibrary/2007/01/wcf/validation" xmlns:q16="http://purolator.com/pws/datatypes/v2" xmlns:q17="http://purolator.com/pws/datatypes/v2" xmlns:q18="http://purolator.com/pws/datatypes/v2" xmlns:q19="http://purolator.com/pws/datatypes/v2" xmlns:q20="http://www.microsoft.com/practices/EnterpriseLibrary/2007/01/wcf/validation" xmlns:ns1="http://purolator.com/pws/datatypes/v2"><SOAP-ENV:Header><ns1:RequestContext><ns1:Version>2.1</ns1:Version><ns1:Language>en</ns1:Language><ns1:GroupID>xxx</ns1:GroupID><ns1:RequestReference>ValidateShipment example</ns1:RequestReference></ns1:RequestContext></SOAP-ENV:Header><SOAP-ENV:Body><q11:ValidateShipmentRequest xmlns:q11="http://purolator.com/pws/datatypes/v2" xmlns="http://purolator.com/pws/datatypes/v2"><q11:Shipment><q11:SenderInformation><q11:Address><q11:Name>Aaron Summer</q11:Name><q11:StreetNumber>1234</q11:StreetNumber><q11:StreetName>Main Street</q11:StreetName><q11:City>Mississauga</q11:City><q11:Province>ON</q11:Province><q11:Country>CA</q11:Country><q11:PostalCode>L4W5M8</q11:PostalCode><q11:PhoneNumber><q11:CountryCode>1</q11:CountryCode><q11:AreaCode>905</q11:AreaCode><q11:Phone>5555555</q11:Phone></q11:PhoneNumber></q11:Address></q11:SenderInformation><q11:ReceiverInformation><q11:Address><q11:Name>Aaron Summer</q11:Name><q11:StreetNumber>2245</q11:StreetNumber><q11:StreetName>Douglas Road</q11:StreetName><q11:City>Burnaby</q11:City><q11:Province>BC</q11:Province><q11:Country>CA</q11:Country><q11:PostalCode>V5C5A9</q11:PostalCode><q11:PhoneNumber><q11:CountryCode>1</q11:CountryCode><q11:AreaCode>604</q11:AreaCode><q11:Phone>2982181</q11:Phone></q11:PhoneNumber></q11:Address></q11:ReceiverInformation><q11:PackageInformation><q11:ServiceID>PurolatorExpress</q11:ServiceID><q11:TotalWeight><q11:Value>40</q11:Value><q11:WeightUnit>lb</q11:WeightUnit></q11:TotalWeight><q11:TotalPieces>1</q11:TotalPieces><q11:PiecesInformation><q11:Piece><q11:Weight><q11:Value>40</q11:Value><q11:WeightUnit>lb</q11:WeightUnit></q11:Weight><q11:Length><q11:Value>40</q11:Value><q11:DimensionUnit>in</q11:DimensionUnit></q11:Length><q11:Width><q11:Value>10</q11:Value><q11:DimensionUnit>in</q11:DimensionUnit></q11:Width><q11:Height><q11:Value>2</q11:Value><q11:DimensionUnit>in</q11:DimensionUnit></q11:Height><q11:Options><q11:OptionIDValuePair><q11:ID>SpecialHandling</q11:ID><q11:Value>true</q11:Value></q11:OptionIDValuePair><q11:OptionIDValuePair><q11:ID>SpecialHandlingType</q11:ID><q11:Value>LargePackage</q11:Value></q11:OptionIDValuePair></q11:Options></q11:Piece></q11:PiecesInformation></q11:PackageInformation><q11:PaymentInformation><q11:PaymentType>Sender</q11:PaymentType><q11:RegisteredAccountNumber>9999999999</q11:RegisteredAccountNumber><q11:BillingAccountNumber>9999999999</q11:BillingAccountNumber></q11:PaymentInformation><q11:PickupInformation><q11:PickupType>DropOff</q11:PickupType></q11:PickupInformation><q11:TrackingReferenceInformation><q11:Reference1>RMA123</q11:Reference1></q11:TrackingReferenceInformation><q11:ProactiveNotification><q11:RequestorName>MyName</q11:RequestorName><q11:RequestorEmail>test@test.com</q11:RequestorEmail><q11:Subscriptions><q11:Subscription><q11:Name>MyName</q11:Name><q11:Email>test@test.com</q11:Email><q11:NotifyWhenExceptionOccurs>true</q11:NotifyWhenExceptionOccurs><q11:NotifyWhenDeliveryOccurs>true</q11:NotifyWhenDeliveryOccurs></q11:Subscription></q11:Subscriptions></q11:ProactiveNotification></q11:Shipment></q11:ValidateShipmentRequest></SOAP-ENV:Body></SOAP-ENV:Envelope>
 **/

/**
 * SOAP Response Envelope (Request Returned from the Web Service)
 * <s:Envelope xmlns:s="http://schemas.xmlsoap.org/soap/envelope/"><s:Header><h:ResponseContext xmlns:h="http://purolator.com/pws/datatypes/v2" xmlns:i="http://www.w3.org/2001/XMLSchema-instance"><h:ResponseReference>ValidateShipment example</h:ResponseReference></h:ResponseContext></s:Header><s:Body><ValidateShipmentResponse xmlns="http://purolator.com/pws/datatypes/v2" xmlns:i="http://www.w3.org/2001/XMLSchema-instance"><ResponseInformation><Errors/><InformationalMessages i:nil="true"/></ResponseInformation><ValidShipment>true</ValidShipment></ValidateShipmentResponse></s:Body></s:Envelope>
 **/

/*
 * EXPECTED RESULTS from PWS
{ ResponseInformation: { Errors: null }, ValidShipment: true }
 */
