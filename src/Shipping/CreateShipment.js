/**
 *  Purolator Public Web Services Example Code
 *
 *  Requires    : Installation of soap module dependency
 *              : Local copy of the Service Availability WSDL
 *              : Valid Development/Production Key and Password
 *
 *  This example covers the creation of a proper SOAP Client (including envelope
 *  headers) to communicate with the Shipping Web Service.
 */
const soap = require('soap');
const utils = require('../utils');
const path = require('path');
const account = require('../credentials.json');

const API_KEY = account.key;
const API_PASSWORD = account.password;
const ACCOUNT_NUMBER = account.account;
const BILLING_ACCOUNT = account.billingAccount;
const USER_TOKEN = account.userToken;

function createPWSSOAPClient(headers) {

	const wsdlFile = 'ShippingService.wsdl';
	const namespace = 'http://purolator.com/pws/datatypes/v2';

	const environment = account.production ? 'production' : 'development';
	const pathToWSDL = path.join(__dirname, '../../wsdl', environment, wsdlFile);

	const options = {
		envelopeKey: 'SOAP-ENV',
		overrideRootElement: {
			// NOTE: Sometimes the WSDL is not properly configured and the namespace is never applied to the request
			namespace: 'ns1',
		}
	};

	return soap
		.createClientAsync(pathToWSDL, options)
		.then(client => {

			client.setSecurity(new soap.BasicAuthSecurity(API_KEY, API_PASSWORD));
			client.addSoapHeader({ RequestContext: headers }, '', 'ns1', '');

			client.wsdl.definitions.xmlns.ns1 = namespace;
			client.wsdl.xmlnsInEnvelope = client.wsdl._xmlnsMap();

			return client;
		});
}

/**
 * CreateShipment Example
 *
 * 1 piece shipment, 10lbs, Purolator Express Service on a Thermal 4x6 Label
 */

const headers = {
	Version: '2.1',
	Language: 'en',
	GroupID: 'xxx',
	RequestReference: 'CreateShipment example',
	// Uncomment this for Commercial applications
	// UserToken: USER_TOKEN
};

createPWSSOAPClient(headers)
	.then(client => {

		const request = {
			Shipment: {
				SenderInformation: {
					Address: {
						Name: 'Aaron Summer',
						StreetNumber: '1234',
						StreetName: 'Main Street',
						City: 'Mississauga',
						Province: 'ON',
						Country: 'CA',
						PostalCode: 'L4W5M8',
						PhoneNumber: {
							CountryCode: '1',
							AreaCode: '905',
							Phone: '5555555',
						}
					}
				},
				ReceiverInformation: {
					Address: {
						Name: 'Aaron Summer',
						StreetNumber: '2245',
						StreetName: 'Douglas Road',
						City: 'Burnaby',
						Province: 'BC',
						Country: 'CA',
						PostalCode: 'V5C5A9',
						PhoneNumber: {
							CountryCode: '1',
							AreaCode: '604',
							Phone: '2982181',
						}
					}
				},
				PackageInformation: {
					ServiceID: 'PurolatorExpress',
					TotalWeight: {
						Value: 40,
						WeightUnit: 'lb',
					},
					TotalPieces: 1,
					PiecesInformation: {
						Piece: [{
							Weight: {
								Value: 40,
								WeightUnit: 'lb',
							},
							Length: {
								Value: 40,
								DimensionUnit: 'in',
							},
							Width: {
								Value: 10,
								DimensionUnit: 'in',
							},
							Height: {
								Value: 2,
								DimensionUnit: 'in',
							},
							Options: {
								OptionIDValuePair: [
									{ ID: 'SpecialHandling', Value: true },
									{ ID: 'SpecialHandlingType', Value: 'LargePackage' }
								]
							}
						}]
					},
					OptionsInformation: {
						Options: {
							OptionIDValuePair: [
								{ ID: 'ResidentialSignatureDomestic', Value: true },
							]
						}
					}
				},
				PaymentInformation: {
					PaymentType: 'Sender',
					RegisteredAccountNumber: ACCOUNT_NUMBER,
					BillingAccountNumber: BILLING_ACCOUNT,
				},
				PickupInformation: {
					PickupType: 'DropOff',
				},
				TrackingReferenceInformation: {
					Reference1: 'Reference For Shipment'
				},
				OtherInformation: {
					SpecialInstructions: 'Notes go here'
				},
				ProactiveNotification: {
					RequestorName: 'MyName',
					RequestorEmail: 'test@test.com',
					Subscriptions: {
						Subscription: [{
							Name: 'MyName',
							Email: 'test@test.com',
							NotifyWhenExceptionOccurs: true,
							NotifyWhenDeliveryOccurs: true,
						}]
					}
				}
			},
			PrinterType: 'Thermal'
		};

		// NOTE: Sometimes the WSDL is not properly configured and the namespace is never applied to the request
		const requestWithNapespace = utils.addNamespace(request);

		return client.CreateShipmentAsync(requestWithNapespace);
	})
	.then(([result, rawResponse, soapheader, rawRequest]) => {
		utils.log(rawRequest);
		utils.log(rawResponse);
		utils.log(result);
	})
	.catch(error => {
		console.log(error);
	});

/**
 * SOAP Request Envelope (Request Made from the SOAP Client)
 * <?xml version="1.0" encoding="UTF-8"?>
 * <?xml version="1.0" encoding="utf-8"?><SOAP-ENV:Envelope xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"  xmlns:tns="http://purolator.com/pws/service/v2" xmlns:msc="http://schemas.microsoft.com/ws/2005/12/wsdl/contract" xmlns:wsu="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd" xmlns:q1="http://schemas.datacontract.org/2004/07/Microsoft.Practices.EnterpriseLibrary.Validation.Integration.WCF" xmlns:q2="http://purolator.com/pws/datatypes/v2" xmlns:q3="http://purolator.com/pws/datatypes/v2" xmlns:q4="http://purolator.com/pws/datatypes/v2" xmlns:q5="http://www.microsoft.com/practices/EnterpriseLibrary/2007/01/wcf/validation" xmlns:q6="http://purolator.com/pws/datatypes/v2" xmlns:q7="http://purolator.com/pws/datatypes/v2" xmlns:q8="http://purolator.com/pws/datatypes/v2" xmlns:q9="http://purolator.com/pws/datatypes/v2" xmlns:q10="http://www.microsoft.com/practices/EnterpriseLibrary/2007/01/wcf/validation" xmlns:q11="http://purolator.com/pws/datatypes/v2" xmlns:q12="http://purolator.com/pws/datatypes/v2" xmlns:q13="http://purolator.com/pws/datatypes/v2" xmlns:q14="http://purolator.com/pws/datatypes/v2" xmlns:q15="http://www.microsoft.com/practices/EnterpriseLibrary/2007/01/wcf/validation" xmlns:q16="http://purolator.com/pws/datatypes/v2" xmlns:q17="http://purolator.com/pws/datatypes/v2" xmlns:q18="http://purolator.com/pws/datatypes/v2" xmlns:q19="http://purolator.com/pws/datatypes/v2" xmlns:q20="http://www.microsoft.com/practices/EnterpriseLibrary/2007/01/wcf/validation" xmlns:ns1="http://purolator.com/pws/datatypes/v2"><SOAP-ENV:Header><ns1:RequestContext><ns1:Version>2.1</ns1:Version><ns1:Language>en</ns1:Language><ns1:GroupID>xxx</ns1:GroupID><ns1:RequestReference>CreateShipment example</ns1:RequestReference></ns1:RequestContext></SOAP-ENV:Header><SOAP-ENV:Body><ns1:CreateShipmentRequest><ns1:Shipment><ns1:SenderInformation><ns1:Address><ns1:Name>Aaron Summer</ns1:Name><ns1:StreetNumber>1234</ns1:StreetNumber><ns1:StreetName>Main Street</ns1:StreetName><ns1:City>Mississauga</ns1:City><ns1:Province>ON</ns1:Province><ns1:Country>CA</ns1:Country><ns1:PostalCode>L4W5M8</ns1:PostalCode><ns1:PhoneNumber><ns1:CountryCode>1</ns1:CountryCode><ns1:AreaCode>905</ns1:AreaCode><ns1:Phone>5555555</ns1:Phone></ns1:PhoneNumber></ns1:Address></ns1:SenderInformation><ns1:ReceiverInformation><ns1:Address><ns1:Name>Aaron Summer</ns1:Name><ns1:StreetNumber>2245</ns1:StreetNumber><ns1:StreetName>Douglas Road</ns1:StreetName><ns1:City>Burnaby</ns1:City><ns1:Province>BC</ns1:Province><ns1:Country>CA</ns1:Country><ns1:PostalCode>V5C5A9</ns1:PostalCode><ns1:PhoneNumber><ns1:CountryCode>1</ns1:CountryCode><ns1:AreaCode>604</ns1:AreaCode><ns1:Phone>2982181</ns1:Phone></ns1:PhoneNumber></ns1:Address></ns1:ReceiverInformation><ns1:PackageInformation><ns1:ServiceID>PurolatorExpress</ns1:ServiceID><ns1:TotalWeight><ns1:Value>40</ns1:Value><ns1:WeightUnit>lb</ns1:WeightUnit></ns1:TotalWeight><ns1:TotalPieces>1</ns1:TotalPieces><ns1:PiecesInformation><Piece><Weight><Value>40</Value><WeightUnit>lb</WeightUnit></Weight><Length><Value>40</Value><DimensionUnit>in</DimensionUnit></Length><Width><Value>10</Value><DimensionUnit>in</DimensionUnit></Width><Height><Value>2</Value><DimensionUnit>in</DimensionUnit></Height><Options><OptionIDValuePair><ID>SpecialHandling</ID><Value>true</Value></OptionIDValuePair><OptionIDValuePair><ID>SpecialHandlingType</ID><Value>LargePackage</Value></OptionIDValuePair></Options></Piece></ns1:PiecesInformation><ns1:OptionsInformation><ns1:Options><OptionIDValuePair><ID>ResidentialSignatureDomestic</ID><Value>true</Value></OptionIDValuePair></ns1:Options></ns1:OptionsInformation></ns1:PackageInformation><ns1:PaymentInformation><ns1:PaymentType>Sender</ns1:PaymentType><ns1:RegisteredAccountNumber>9999999999</ns1:RegisteredAccountNumber><ns1:BillingAccountNumber>9999999999</ns1:BillingAccountNumber></ns1:PaymentInformation><ns1:PickupInformation><ns1:PickupType>DropOff</ns1:PickupType></ns1:PickupInformation><ns1:TrackingReferenceInformation><ns1:Reference1>Reference For Shipment</ns1:Reference1></ns1:TrackingReferenceInformation><ns1:OtherInformation><ns1:SpecialInstructions>Notes go here</ns1:SpecialInstructions></ns1:OtherInformation><ns1:ProactiveNotification><ns1:RequestorName>MyName</ns1:RequestorName><ns1:RequestorEmail>test@test.com</ns1:RequestorEmail><ns1:Subscriptions><Subscription><Name>MyName</Name><Email>test@test.com</Email><NotifyWhenExceptionOccurs>true</NotifyWhenExceptionOccurs><NotifyWhenDeliveryOccurs>true</NotifyWhenDeliveryOccurs></Subscription></ns1:Subscriptions></ns1:ProactiveNotification></ns1:Shipment><ns1:PrinterType>Thermal</ns1:PrinterType></ns1:CreateShipmentRequest></SOAP-ENV:Body></SOAP-ENV:Envelope>
 **/

/**
 * SOAP Response Envelope (Request Returned from the Web Service)
 * <s:Envelope xmlns:s="http://schemas.xmlsoap.org/soap/envelope/"><s:Header><h:ResponseContext xmlns:h="http://purolator.com/pws/datatypes/v2" xmlns:i="http://www.w3.org/2001/XMLSchema-instance"><h:ResponseReference>CreateShipment example</h:ResponseReference></h:ResponseContext></s:Header><s:Body><CreateShipmentResponse xmlns="http://purolator.com/pws/datatypes/v2" xmlns:i="http://www.w3.org/2001/XMLSchema-instance"><ResponseInformation><Errors/><InformationalMessages i:nil="true"/></ResponseInformation><ShipmentPIN><Value>329022093200</Value></ShipmentPIN><PiecePINs><PIN><Value>329022093200</Value></PIN></PiecePINs><ReturnShipmentPINs/><ExpressChequePIN><Value/></ExpressChequePIN></CreateShipmentResponse></s:Body></s:Envelope>
 **/

/*
 * EXPECTED RESULTS from PWS
{ ResponseInformation: { Errors: null },
  ShipmentPIN: { Value: '329022093200' },
  PiecePINs: { PIN: [ { Value: '329022093200' } ] },
  ReturnShipmentPINs: null,
  ExpressChequePIN: { Value: '' } }

 */
