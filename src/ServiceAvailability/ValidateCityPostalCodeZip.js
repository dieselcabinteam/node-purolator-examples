/**
 *  Purolator Public Web Services Example Code
 *
 *  Requires    : Installation of soap module dependency
 *              : Local copy of the Service Availability WSDL
 *              : Valid Development/Production Key and Password
 *
 *  This example covers the creation of a proper SOAP Client (including envelope
 *  headers) to communicate with the Validate City Postal Code/Zip Web Service.
 */
const soap = require('soap');
const utils = require('../utils');
const path = require('path');
const account = require('../credentials.json');

const API_KEY = account.key;
const API_PASSWORD = account.password;
const ACCOUNT_NUMBER = account.account;
const BILLING_ACCOUNT = account.billingAccount;
const USER_TOKEN = account.userToken;

function log(o) {
	const util = require('util');
	console.log(util.inspect(o, false, null))
}

function createPWSSOAPClient(headers) {

	const wsdlFile = 'ServiceAvailabilityService.wsdl';
	const namespace = 'http://purolator.com/pws/datatypes/v2';

	const environment = account.production ? 'production' : 'development';
	const pathToWSDL = path.join(__dirname, '../../wsdl', environment, wsdlFile);

	const options = {
		envelopeKey: 'SOAP-ENV',
	};

	return soap
		.createClientAsync(pathToWSDL, options)
		.then(client => {

			client.setSecurity(new soap.BasicAuthSecurity(API_KEY, API_PASSWORD));
			client.addSoapHeader({ RequestContext: headers }, '', 'ns1', '');

			client.wsdl.definitions.xmlns.ns1 = namespace;
			client.wsdl.xmlnsInEnvelope = client.wsdl._xmlnsMap();

			return client;
		});
}

/**
 * ValidateCityPostalCodeZip Example
 *
 * Determine if Postal Code V5E4H9 is valid
 */

const headers = {
	Version: '2.0',
	Language: 'en',
	GroupID: 'xxx',
	RequestReference: 'ValidateCityPostalCodeZip example',
	// Uncomment this for Commercial applications
	// UserToken: USER_TOKEN
};

createPWSSOAPClient(headers)
	.then(client => {

		const request = {
			Addresses: {
				ShortAddress: {
					City: 'Burnaby',
					Province: 'BC',
					Country: 'CA',
					PostalCode: 'V5E4H9',
				}
			}
		};

		return client.ValidateCityPostalCodeZipAsync(request);
	})
	.then(([result, rawResponse, soapheader, rawRequest]) => {
		utils.log(rawRequest);
		utils.log(rawResponse);
		utils.log(result);
	})
	.catch(error => {
		console.log(error);
	});

/**
 * SOAP Request Envelope (Request Made from the SOAP Client)
 * <?xml version="1.0" encoding="UTF-8"?>
 * <?xml version="1.0" encoding="utf-8"?><SOAP-ENV:Envelope xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"  xmlns:wsu="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd" xmlns:tns="http://purolator.com/pws/service/v2" xmlns:msc="http://schemas.microsoft.com/ws/2005/12/wsdl/contract" xmlns:q1="http://schemas.datacontract.org/2004/07/Microsoft.Practices.EnterpriseLibrary.Validation.Integration.WCF" xmlns:q2="http://purolator.com/pws/datatypes/v2" xmlns:q3="http://purolator.com/pws/datatypes/v2" xmlns:q4="http://purolator.com/pws/datatypes/v2" xmlns:q5="http://purolator.com/pws/datatypes/v2" xmlns:q6="http://www.microsoft.com/practices/EnterpriseLibrary/2007/01/wcf/validation" xmlns:q7="http://purolator.com/pws/datatypes/v2" xmlns:q8="http://purolator.com/pws/datatypes/v2" xmlns:q9="http://purolator.com/pws/datatypes/v2" xmlns:q10="http://purolator.com/pws/datatypes/v2" xmlns:q11="http://www.microsoft.com/practices/EnterpriseLibrary/2007/01/wcf/validation" xmlns:q12="http://purolator.com/pws/datatypes/v2" xmlns:q13="http://purolator.com/pws/datatypes/v2" xmlns:q14="http://purolator.com/pws/datatypes/v2" xmlns:q15="http://purolator.com/pws/datatypes/v2" xmlns:q16="http://www.microsoft.com/practices/EnterpriseLibrary/2007/01/wcf/validation" xmlns:ns1="http://purolator.com/pws/datatypes/v2"><SOAP-ENV:Header><ns1:RequestContext><ns1:Version>2.0</ns1:Version><ns1:Language>en</ns1:Language><ns1:GroupID>xxx</ns1:GroupID><ns1:RequestReference>ValidateCityPostalCodeZip example</ns1:RequestReference></ns1:RequestContext></SOAP-ENV:Header><SOAP-ENV:Body><q12:ValidateCityPostalCodeZipRequest xmlns:q12="http://purolator.com/pws/datatypes/v2" xmlns="http://purolator.com/pws/datatypes/v2"><q12:Addresses><q12:ShortAddress><q12:City>Burnaby</q12:City><q12:Province>BC</q12:Province><q12:Country>CA</q12:Country><q12:PostalCode>V5E4H9</q12:PostalCode></q12:ShortAddress></q12:Addresses></q12:ValidateCityPostalCodeZipRequest></SOAP-ENV:Body></SOAP-ENV:Envelope>
 **/

/**
 * SOAP Response Envelope (Request Returned from the Web Service)
 * <s:Envelope xmlns:s="http://schemas.xmlsoap.org/soap/envelope/"><s:Header><h:ResponseContext xmlns:h="http://purolator.com/pws/datatypes/v2" xmlns:i="http://www.w3.org/2001/XMLSchema-instance"><h:ResponseReference>ValidateCityPostalCodeZip example</h:ResponseReference></h:ResponseContext></s:Header><s:Body><ValidateCityPostalCodeZipResponse xmlns="http://purolator.com/pws/datatypes/v2" xmlns:i="http://www.w3.org/2001/XMLSchema-instance"><ResponseInformation><Errors/><InformationalMessages i:nil="true"/></ResponseInformation><SuggestedAddresses><SuggestedAddress><Address><City>Burnaby</City><Province>BC</Province><Country>CA</Country><PostalCode>V5E4H9</PostalCode></Address><ResponseInformation><Errors/><InformationalMessages i:nil="true"/></ResponseInformation></SuggestedAddress></SuggestedAddresses></ValidateCityPostalCodeZipResponse></s:Body></s:Envelope>
 **/

/*
 * EXPECTED RESULTS from PWS
{ ResponseInformation: { Errors: null },
  SuggestedAddresses:
   { SuggestedAddress:
      [ { Address:
           { City: 'Burnaby',
             Province: 'BC',
             Country: 'CA',
             PostalCode: 'V5E4H9' },
          ResponseInformation: { Errors: null } } ] } }
 */
