/**
 *  Purolator Public Web Services Example Code
 *
 *  Requires    : Installation of soap module dependency
 *              : Local copy of the Service Availability WSDL
 *              : Valid Development/Production Key and Password
 *
 *  This example covers the creation of a proper SOAP Client (including envelope
 *  headers) to communicate with the Services Options Web Service.
 */
const soap = require('soap');
const utils = require('../utils');
const path = require('path');
const account = require('../credentials.json');

const API_KEY = account.key;
const API_PASSWORD = account.password;
const ACCOUNT_NUMBER = account.account;
const BILLING_ACCOUNT = account.billingAccount;
const USER_TOKEN = account.userToken;

function createPWSSOAPClient(headers) {

	const wsdlFile = 'ServiceAvailabilityService.wsdl';
	const namespace = 'http://purolator.com/pws/datatypes/v2';

	const environment = account.production ? 'production' : 'development';
	const pathToWSDL = path.join(__dirname, '../../wsdl', environment, wsdlFile);

	const options = {
		envelopeKey: 'SOAP-ENV',
	};

	return soap
		.createClientAsync(pathToWSDL, options)
		.then(client => {

			client.setSecurity(new soap.BasicAuthSecurity(API_KEY, API_PASSWORD));
			client.addSoapHeader({ RequestContext: headers }, '', 'ns1', '');

			client.wsdl.definitions.xmlns.ns1 = namespace;
			client.wsdl.xmlnsInEnvelope = client.wsdl._xmlnsMap();

			return client;
		});
}

/**
 * GetServicesOptions Example
 *
 * Determine all service options origin and destination
 */

const headers = {
	Version: '2.0',
	Language: 'en',
	GroupID: 'xxx',
	RequestReference: 'GetServicesOptions example',
	// Uncomment this for Commercial applications
	// UserToken: USER_TOKEN
};

createPWSSOAPClient(headers)
	.then(client => {

		const request = {
			BillingAccountNumber: BILLING_ACCOUNT,
			SenderAddress: {
				City: 'Mississauga',
				Province: 'ON',
				Country: 'CA',
				PostalCode: 'L4W5M8',
			},
			ReceiverAddress: {
				City: 'Burnaby',
				Province: 'BC',
				Country: 'CA',
				PostalCode: 'V5C5A9',
			},
			ShipmentDate: '2018-10-30'
		};

		return client.GetServicesOptionsAsync(request);
	})
	.then(([result, rawResponse, soapheader, rawRequest]) => {
		utils.log(rawRequest);
		utils.log(rawResponse);
		utils.log(result);
	})
	.catch(error => {
		console.log(error);
	});

/**
 * SOAP Request Envelope (Request Made from the SOAP Client)
 * <?xml version="1.0" encoding="UTF-8"?>
 * <?xml version="1.0" encoding="utf-8"?><SOAP-ENV:Envelope xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"  xmlns:wsu="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd" xmlns:tns="http://purolator.com/pws/service/v2" xmlns:msc="http://schemas.microsoft.com/ws/2005/12/wsdl/contract" xmlns:q1="http://schemas.datacontract.org/2004/07/Microsoft.Practices.EnterpriseLibrary.Validation.Integration.WCF" xmlns:q2="http://purolator.com/pws/datatypes/v2" xmlns:q3="http://purolator.com/pws/datatypes/v2" xmlns:q4="http://purolator.com/pws/datatypes/v2" xmlns:q5="http://purolator.com/pws/datatypes/v2" xmlns:q6="http://www.microsoft.com/practices/EnterpriseLibrary/2007/01/wcf/validation" xmlns:q7="http://purolator.com/pws/datatypes/v2" xmlns:q8="http://purolator.com/pws/datatypes/v2" xmlns:q9="http://purolator.com/pws/datatypes/v2" xmlns:q10="http://purolator.com/pws/datatypes/v2" xmlns:q11="http://www.microsoft.com/practices/EnterpriseLibrary/2007/01/wcf/validation" xmlns:q12="http://purolator.com/pws/datatypes/v2" xmlns:q13="http://purolator.com/pws/datatypes/v2" xmlns:q14="http://purolator.com/pws/datatypes/v2" xmlns:q15="http://purolator.com/pws/datatypes/v2" xmlns:q16="http://www.microsoft.com/practices/EnterpriseLibrary/2007/01/wcf/validation" xmlns:ns1="http://purolator.com/pws/datatypes/v2"><SOAP-ENV:Header><ns1:RequestContext><ns1:Version>2.0</ns1:Version><ns1:Language>en</ns1:Language><ns1:GroupID>xxx</ns1:GroupID><ns1:RequestReference>GetServiceRules example</ns1:RequestReference></ns1:RequestContext></SOAP-ENV:Header><SOAP-ENV:Body><q2:GetServicesOptionsRequest xmlns:q2="http://purolator.com/pws/datatypes/v2" xmlns="http://purolator.com/pws/datatypes/v2"><q2:BillingAccountNumber>9999999999</q2:BillingAccountNumber><q2:SenderAddress><q2:City>Mississauga</q2:City><q2:Province>ON</q2:Province><q2:Country>CA</q2:Country><q2:PostalCode>L4W5M8</q2:PostalCode></q2:SenderAddress><q2:ReceiverAddress><q2:City>Burnaby</q2:City><q2:Province>BC</q2:Province><q2:Country>CA</q2:Country><q2:PostalCode>V5C5A9</q2:PostalCode></q2:ReceiverAddress><q2:ShipmentDate>2018-10-30</q2:ShipmentDate></q2:GetServicesOptionsRequest></SOAP-ENV:Body></SOAP-ENV:Envelope>
 **/

/**
 * SOAP Response Envelope (Request Returned from the Web Service)
 * <s:Envelope xmlns:s="http://schemas.xmlsoap.org/soap/envelope/"><s:Header><h:ResponseContext xmlns:h="http://purolator.com/pws/datatypes/v2" xmlns:i="http://www.w3.org/2001/XMLSchema-instance"><h:ResponseReference>GetServiceRules example</h:ResponseReference></h:ResponseContext></s:Header><s:Body><GetServicesOptionsResponse xmlns="http://purolator.com/pws/datatypes/v2" xmlns:i="http://www.w3.org/2001/XMLSchema-instance"><ResponseInformation><Errors/><InformationalMessages i:nil="true"/></ResponseInformation><Services><Service><ID>PurolatorExpress9AM</ID><Description>Purolator Express 9AM</Description><PackageType>CustomerPackaging</PackageType><PackageTypeDescription>Customer Packaging</PackageTypeDescription><Options><Option><ID>ChainOfSignature</ID><Description>Chain Of Signature</Description><ValueType>Enumeration</ValueType><AvailableForPieces>false</AvailableForPieces><PossibleValues><OptionValue><Value>true</Value><Description>Yes</Description></OptionValue><OptionValue><Value>false</Value><Description>No</Description></OptionValue></PossibleValues><ChildServiceOptions/></Option><Option><ID>DangerousGoods</ID><Description>Dangerous Goods Indicator</Description><ValueType>Enumeration</ValueType><AvailableForPieces>false</AvailableForPieces><PossibleValues><OptionValue><Value>true</Value><Description>Yes</Description></OptionValue><OptionValue><Value>false</Value><Description>No</Description></OptionValue></PossibleValues><ChildServiceOptions><Option><ID>DangerousGoodsMode</ID><Description>Dangerous Goods Mode</Description><ValueType>Enumeration</ValueType><AvailableForPieces>false</AvailableForPieces><PossibleValues><OptionValue><Value>Air</Value><Description>Air</Description></OptionValue></PossibleValues><ChildServiceOptions><Option><ID>DangerousGoodsClass</ID><Description>Dangerous Goods Class</Description><ValueType>Enumeration</ValueType><AvailableForPieces>false</AvailableForPieces><PossibleValues><OptionValue><Value>FullyRegulated</Value><Description>Fully Regulated</Description></OptionValue><OptionValue><Value>UN3373</Value><Description>UN3373</Description></OptionValue><OptionValue><Value>UN1845</Value><Description>UN1845</Description></OptionValue></PossibleValues><ChildServiceOptions/></Option></ChildServiceOptions></Option></ChildServiceOptions></Option><Option><ID>DeclaredValue</ID><Description>Declared Value</Description><ValueType>Decimal</ValueType><AvailableForPieces>false</AvailableForPieces><PossibleValues/><ChildServiceOptions/></Option><Option><ID>HoldForPickup</ID><Description>Hold For Pickup</Description><ValueType>Enumeration</ValueType><AvailableForPieces>false</AvailableForPieces><PossibleValues><OptionValue><Value>true</Value><Description>Yes</Description></OptionValue><OptionValue><Value>false</Value><Description>No</Description></OptionValue></PossibleValues><ChildServiceOptions/></Option><Option><ID>OriginSignatureNotRequired</ID><Description>OSNR-Signature Not Required</Description><ValueType>Enumeration</ValueType><AvailableForPieces>false</AvailableForPieces><PossibleValues><OptionValue><Value>true</Value><Description>Yes</Description></OptionValue><OptionValue><Value>false</Value><Description>No</Description></OptionValue></PossibleValues><ChildServiceOptions/></Option><Option><ID>AdultSignatureRequired</ID><Description>Adult Signature Required</Description><ValueType>Enumeration</ValueType><AvailableForPieces>false</AvailableForPieces><PossibleValues><OptionValue><Value>true</Value><Description>Yes</Description></OptionValue><OptionValue><Value>false</Value><Description>No</Description></OptionValue></PossibleValues><ChildServiceOptions/></Option><Option><ID>SpecialHandling</ID><Description>Special Handling</Description><ValueType>Enumeration</ValueType><AvailableForPieces>true</AvailableForPieces><PossibleValues><OptionValue><Value>true</Value><Description>Yes</Description></OptionValue><OptionValue><Value>false</Value><Description>No</Description></OptionValue></PossibleValues><ChildServiceOptions><Option><ID>SpecialHandlingType</ID><Description>Type of Special Handling</Description><ValueType>Enumeration</ValueType><AvailableForPieces>true</AvailableForPieces><PossibleValues><OptionValue><Value>FlatPackage</Value><Description>Flat Package</Description></OptionValue><OptionValue><Value>AdditionalHandling</Value><Description>Additional Handling</Description></OptionValue><OptionValue><Value>LargePackage</Value><Description>Large package</Description></OptionValue><OptionValue><Value>Oversized</Value><Description>Oversized</Description></OptionValue></PossibleValues><ChildServiceOptions/></Option></ChildServiceOptions></Option></Options></Service><Service><ID>PurolatorExpress10:30AM</ID><Description>Purolator Express 10:30AM</Description><PackageType>CustomerPackaging</PackageType><PackageTypeDescription>Customer Packaging</PackageTypeDescription><Options><Option><ID>ChainOfSignature</ID><Description>Chain Of Signature</Description><ValueType>Enumeration</ValueType><AvailableForPieces>false</AvailableForPieces><PossibleValues><OptionValue><Value>true</Value><Description>Yes</Description></OptionValue><OptionValue><Value>false</Value><Description>No</Description></OptionValue></PossibleValues><ChildServiceOptions/></Option><Option><ID>DangerousGoods</ID><Description>Dangerous Goods Indicator</Description><ValueType>Enumeration</ValueType><AvailableForPieces>false</AvailableForPieces><PossibleValues><OptionValue><Value>true</Value><Description>Yes</Description></OptionValue><OptionValue><Value>false</Value><Description>No</Description></OptionValue></PossibleValues><ChildServiceOptions><Option><ID>DangerousGoodsMode</ID><Description>Dangerous Goods Mode</Description><ValueType>Enumeration</ValueType><AvailableForPieces>false</AvailableForPieces><PossibleValues><OptionValue><Value>Air</Value><Description>Air</Description></OptionValue></PossibleValues><ChildServiceOptions><Option><ID>DangerousGoodsClass</ID><Description>Dangerous Goods Class</Description><ValueType>Enumeration</ValueType><AvailableForPieces>false</AvailableForPieces><PossibleValues><OptionValue><Value>FullyRegulated</Value><Description>Fully Regulated</Description></OptionValue><OptionValue><Value>UN3373</Value><Description>UN3373</Description></OptionValue><OptionValue><Value>UN1845</Value><Description>UN1845</Description></OptionValue></PossibleValues><ChildServiceOptions/></Option></ChildServiceOptions></Option></ChildServiceOptions></Option><Option><ID>DeclaredValue</ID><Description>Declared Value</Description><ValueType>Decimal</ValueType><AvailableForPieces>false</AvailableForPieces><PossibleValues/><ChildServiceOptions/></Option><Option><ID>HoldForPickup</ID><Description>Hold For Pickup</Description><ValueType>Enumeration</ValueType><AvailableForPieces>false</AvailableForPieces><PossibleValues><OptionValue><Value>true</Value><Description>Yes</Description></OptionValue><OptionValue><Value>false</Value><Description>No</Description></OptionValue></PossibleValues><ChildServiceOptions/></Option><Option><ID>OriginSignatureNotRequired</ID><Description>OSNR-Signature Not Required</Description><ValueType>Enumeration</ValueType><AvailableForPieces>false</AvailableForPieces><PossibleValues><OptionValue><Value>true</Value><Description>Yes</Description></OptionValue><OptionValue><Value>false</Value><Description>No</Description></OptionValue></PossibleValues><ChildServiceOptions/></Option><Option><ID>AdultSignatureRequired</ID><Description>Adult Signature Required</Description><ValueType>Enumeration</ValueType><AvailableForPieces>false</AvailableForPieces><PossibleValues><OptionValue><Value>true</Value><Description>Yes</Description></OptionValue><OptionValue><Value>false</Value><Description>No</Description></OptionValue></PossibleValues><ChildServiceOptions/></Option><Option><ID>SpecialHandling</ID><Description>Special Handling</Description><ValueType>Enumeration</ValueType><AvailableForPieces>true</AvailableForPieces><PossibleValues><OptionValue><Value>true</Value><Description>Yes</Description></OptionValue><OptionValue><Value>false</Value><Description>No</Description></OptionValue></PossibleValues><ChildServiceOptions><Option><ID>SpecialHandlingType</ID><Description>Type of Special Handling</Description><ValueType>Enumeration</ValueType><AvailableForPieces>true</AvailableForPieces><PossibleValues><OptionValue><Value>FlatPackage</Value><Description>Flat Package</Description></OptionValue><OptionValue><Value>AdditionalHandling</Value><Description>Additional Handling</Description></OptionValue><OptionValue><Value>LargePackage</Value><Description>Large package</Description></OptionValue><OptionValue><Value>Oversized</Value><Description>Oversized</Description></OptionValue></PossibleValues><ChildServiceOptions/></Option></ChildServiceOptions></Option></Options></Service><Service><ID>PurolatorExpress12PM</ID><Description>Purolator Express 12PM</Description><PackageType>CustomerPackaging</PackageType><PackageTypeDescription>Customer Packaging</PackageTypeDescription><Options><Option><ID>ChainOfSignature</ID><Description>Chain Of Signature</Description><ValueType>Enumeration</ValueType><AvailableForPieces>false</AvailableForPieces><PossibleValues><OptionValue><Value>true</Value><Description>Yes</Description></OptionValue><OptionValue><Value>false</Value><Description>No</Description></OptionValue></PossibleValues><ChildServiceOptions/></Option><Option><ID>DangerousGoods</ID><Description>Dangerous Goods Indicator</Description><ValueType>Enumeration</ValueType><AvailableForPieces>false</AvailableForPieces><PossibleValues><OptionValue><Value>true</Value><Description>Yes</Description></OptionValue><OptionValue><Value>false</Value><Description>No</Description></OptionValue></PossibleValues><ChildServiceOptions><Option><ID>DangerousGoodsMode</ID><Description>Dangerous Goods Mode</Description><ValueType>Enumeration</ValueType><AvailableForPieces>false</AvailableForPieces><PossibleValues><OptionValue><Value>Air</Value><Description>Air</Description></OptionValue></PossibleValues><ChildServiceOptions><Option><ID>DangerousGoodsClass</ID><Description>Dangerous Goods Class</Description><ValueType>Enumeration</ValueType><AvailableForPieces>false</AvailableForPieces><PossibleValues><OptionValue><Value>FullyRegulated</Value><Description>Fully Regulated</Description></OptionValue><OptionValue><Value>UN3373</Value><Description>UN3373</Description></OptionValue><OptionValue><Value>UN1845</Value><Description>UN1845</Description></OptionValue></PossibleValues><ChildServiceOptions/></Option></ChildServiceOptions></Option></ChildServiceOptions></Option><Option><ID>DeclaredValue</ID><Description>Declared Value</Description><ValueType>Decimal</ValueType><AvailableForPieces>false</AvailableForPieces><PossibleValues/><ChildServiceOptions/></Option><Option><ID>HoldForPickup</ID><Description>Hold For Pickup</Description><ValueType>Enumeration</ValueType><AvailableForPieces>false</AvailableForPieces><PossibleValues><OptionValue><Value>true</Value><Description>Yes</Description></OptionValue><OptionValue><Value>false</Value><Description>No</Description></OptionValue></PossibleValues><ChildServiceOptions/></Option><Option><ID>OriginSignatureNotRequired</ID><Description>OSNR-Signature Not Required</Description><ValueType>Enumeration</ValueType><AvailableForPieces>false</AvailableForPieces><PossibleValues><OptionValue><Value>true</Value><Description>Yes</Description></OptionValue><OptionValue><Value>false</Value><Description>No</Description></OptionValue></PossibleValues><ChildServiceOptions/></Option><Option><ID>AdultSignatureRequired</ID><Description>Adult Signature Required</Description><ValueType>Enumeration</ValueType><AvailableForPieces>false</AvailableForPieces><PossibleValues><OptionValue><Value>true</Value><Description>Yes</Description></OptionValue><OptionValue><Value>false</Value><Description>No</Description></OptionValue></PossibleValues><ChildServiceOptions/></Option><Option><ID>SpecialHandling</ID><Description>Special Handling</Description><ValueType>Enumeration</ValueType><AvailableForPieces>true</AvailableForPieces><PossibleValues><OptionValue><Value>true</Value><Description>Yes</Description></OptionValue><OptionValue><Value>false</Value><Description>No</Description></OptionValue></PossibleValues><ChildServiceOptions><Option><ID>SpecialHandlingType</ID><Description>Type of Special Handling</Description><ValueType>Enumeration</ValueType><AvailableForPieces>true</AvailableForPieces><PossibleValues><OptionValue><Value>FlatPackage</Value><Description>Flat Package</Description></OptionValue><OptionValue><Value>AdditionalHandling</Value><Description>Additional Handling</Description></OptionValue><OptionValue><Value>LargePackage</Value><Description>Large package</Description></OptionValue><OptionValue><Value>Oversized</Value><Description>Oversized</Description></OptionValue></PossibleValues><ChildServiceOptions/></Option></ChildServiceOptions></Option></Options></Service><Service><ID>PurolatorExpress</ID><Description>Purolator Express</Description><PackageType>CustomerPackaging</PackageType><PackageTypeDescription>Customer Packaging</PackageTypeDescription><Options><Option><ID>ChainOfSignature</ID><Description>Chain Of Signature</Description><ValueType>Enumeration</ValueType><AvailableForPieces>false</AvailableForPieces><PossibleValues><OptionValue><Value>true</Value><Description>Yes</Description></OptionValue><OptionValue><Value>false</Value><Description>No</Description></OptionValue></PossibleValues><ChildServiceOptions/></Option><Option><ID>DangerousGoods</ID><Description>Dangerous Goods Indicator</Description><ValueType>Enumeration</ValueType><AvailableForPieces>false</AvailableForPieces><PossibleValues><OptionValue><Value>true</Value><Description>Yes</Description></OptionValue><OptionValue><Value>false</Value><Description>No</Description></OptionValue></PossibleValues><ChildServiceOptions><Option><ID>DangerousGoodsMode</ID><Description>Dangerous Goods Mode</Description><ValueType>Enumeration</ValueType><AvailableForPieces>false</AvailableForPieces><PossibleValues><OptionValue><Value>Air</Value><Description>Air</Description></OptionValue></PossibleValues><ChildServiceOptions><Option><ID>DangerousGoodsClass</ID><Description>Dangerous Goods Class</Description><ValueType>Enumeration</ValueType><AvailableForPieces>false</AvailableForPieces><PossibleValues><OptionValue><Value>FullyRegulated</Value><Description>Fully Regulated</Description></OptionValue><OptionValue><Value>UN3373</Value><Description>UN3373</Description></OptionValue><OptionValue><Value>UN1845</Value><Description>UN1845</Description></OptionValue></PossibleValues><ChildServiceOptions/></Option></ChildServiceOptions></Option></ChildServiceOptions></Option><Option><ID>DeclaredValue</ID><Description>Declared Value</Description><ValueType>Decimal</ValueType><AvailableForPieces>false</AvailableForPieces><PossibleValues/><ChildServiceOptions/></Option><Option><ID>ExpressCheque</ID><Description>ExpressCheque</Description><ValueType>Enumeration</ValueType><AvailableForPieces>false</AvailableForPieces><PossibleValues><OptionValue><Value>true</Value><Description>Yes</Description></OptionValue><OptionValue><Value>false</Value><Description>No</Description></OptionValue></PossibleValues><ChildServiceOptions><Option><ID>ExpressChequeMethodOfPayment</ID><Description>Method of Payment</Description><ValueType>Enumeration</ValueType><AvailableForPieces>false</AvailableForPieces><PossibleValues><OptionValue><Value>Cheque</Value><Description>Cheque</Description></OptionValue><OptionValue><Value>PostDatedCheque</Value><Description>Post dated Cheque</Description></OptionValue><OptionValue><Value>CertifiedCheque</Value><Description>Certified Cheque</Description></OptionValue><OptionValue><Value>MoneyOrder</Value><Description>Money Order</Description></OptionValue><OptionValue><Value>BankDraft</Value><Description>Bank Draft</Description></OptionValue></PossibleValues><ChildServiceOptions><Option><ID>ExpressChequeAmount</ID><Description>Amount</Description><ValueType>Decimal</ValueType><AvailableForPieces>false</AvailableForPieces><PossibleValues/><ChildServiceOptions/></Option></ChildServiceOptions></Option></ChildServiceOptions></Option><Option><ID>HoldForPickup</ID><Description>Hold For Pickup</Description><ValueType>Enumeration</ValueType><AvailableForPieces>false</AvailableForPieces><PossibleValues><OptionValue><Value>true</Value><Description>Yes</Description></OptionValue><OptionValue><Value>false</Value><Description>No</Description></OptionValue></PossibleValues><ChildServiceOptions/></Option><Option><ID>OriginSignatureNotRequired</ID><Description>OSNR-Signature Not Required</Description><ValueType>Enumeration</ValueType><AvailableForPieces>false</AvailableForPieces><PossibleValues><OptionValue><Value>true</Value><Description>Yes</Description></OptionValue><OptionValue><Value>false</Value><Description>No</Description></OptionValue></PossibleValues><ChildServiceOptions/></Option><Option><ID>AdultSignatureRequired</ID><Description>Adult Signature Required</Description><ValueType>Enumeration</ValueType><AvailableForPieces>false</AvailableForPieces><PossibleValues><OptionValue><Value>true</Value><Description>Yes</Description></OptionValue><OptionValue><Value>false</Value><Description>No</Description></OptionValue></PossibleValues><ChildServiceOptions/></Option><Option><ID>SpecialHandling</ID><Description>Special Handling</Description><ValueType>Enumeration</ValueType><AvailableForPieces>true</AvailableForPieces><PossibleValues><OptionValue><Value>true</Value><Description>Yes</Description></OptionValue><OptionValue><Value>false</Value><Description>No</Description></OptionValue></PossibleValues><ChildServiceOptions><Option><ID>SpecialHandlingType</ID><Description>Type of Special Handling</Description><ValueType>Enumeration</ValueType><AvailableForPieces>true</AvailableForPieces><PossibleValues><OptionValue><Value>FlatPackage</Value><Description>Flat Package</Description></OptionValue><OptionValue><Value>AdditionalHandling</Value><Description>Additional Handling</Description></OptionValue><OptionValue><Value>LargePackage</Value><Description>Large package</Description></OptionValue><OptionValue><Value>Oversized</Value><Description>Oversized</Description></OptionValue></PossibleValues><ChildServiceOptions/></Option></ChildServiceOptions></Option></Options></Service><Service><ID>PurolatorGround9AM</ID><Description>Purolator Ground 9AM</Description><PackageType>CustomerPackaging</PackageType><PackageTypeDescription>Customer Packaging</PackageTypeDescription><Options><Option><ID>ChainOfSignature</ID><Description>Chain Of Signature</Description><ValueType>Enumeration</ValueType><AvailableForPieces>false</AvailableForPieces><PossibleValues><OptionValue><Value>true</Value><Description>Yes</Description></OptionValue><OptionValue><Value>false</Value><Description>No</Description></OptionValue></PossibleValues><ChildServiceOptions/></Option><Option><ID>DangerousGoods</ID><Description>Dangerous Goods Indicator</Description><ValueType>Enumeration</ValueType><AvailableForPieces>false</AvailableForPieces><PossibleValues><OptionValue><Value>true</Value><Description>Yes</Description></OptionValue><OptionValue><Value>false</Value><Description>No</Description></OptionValue></PossibleValues><ChildServiceOptions><Option><ID>DangerousGoodsMode</ID><Description>Dangerous Goods Mode</Description><ValueType>Enumeration</ValueType><AvailableForPieces>false</AvailableForPieces><PossibleValues><OptionValue><Value>Ground</Value><Description>Ground</Description></OptionValue></PossibleValues><ChildServiceOptions><Option><ID>DangerousGoodsClass</ID><Description>Dangerous Goods Class</Description><ValueType>Enumeration</ValueType><AvailableForPieces>false</AvailableForPieces><PossibleValues><OptionValue><Value>FullyRegulated</Value><Description>Fully Regulated</Description></OptionValue><OptionValue><Value>UN3373</Value><Description>UN3373</Description></OptionValue><OptionValue><Value>UN1845</Value><Description>UN1845</Description></OptionValue><OptionValue><Value>LessThan500kgExempt</Value><Description>&lt;500 kg Exempt</Description></OptionValue><OptionValue><Value>LimitedQuantities</Value><Description>Limited Quantities</Description></OptionValue></PossibleValues><ChildServiceOptions/></Option></ChildServiceOptions></Option></ChildServiceOptions></Option><Option><ID>DeclaredValue</ID><Description>Declared Value</Description><ValueType>Decimal</ValueType><AvailableForPieces>false</AvailableForPieces><PossibleValues/><ChildServiceOptions/></Option><Option><ID>HoldForPickup</ID><Description>Hold For Pickup</Description><ValueType>Enumeration</ValueType><AvailableForPieces>false</AvailableForPieces><PossibleValues><OptionValue><Value>true</Value><Description>Yes</Description></OptionValue><OptionValue><Value>false</Value><Description>No</Description></OptionValue></PossibleValues><ChildServiceOptions/></Option><Option><ID>OriginSignatureNotRequired</ID><Description>OSNR-Signature Not Required</Description><ValueType>Enumeration</ValueType><AvailableForPieces>false</AvailableForPieces><PossibleValues><OptionValue><Value>true</Value><Description>Yes</Description></OptionValue><OptionValue><Value>false</Value><Description>No</Description></OptionValue></PossibleValues><ChildServiceOptions/></Option><Option><ID>AdultSignatureRequired</ID><Description>Adult Signature Required</Description><ValueType>Enumeration</ValueType><AvailableForPieces>false</AvailableForPieces><PossibleValues><OptionValue><Value>true</Value><Description>Yes</Description></OptionValue><OptionValue><Value>false</Value><Description>No</Description></OptionValue></PossibleValues><ChildServiceOptions/></Option><Option><ID>SpecialHandling</ID><Description>Special Handling</Description><ValueType>Enumeration</ValueType><AvailableForPieces>true</AvailableForPieces><PossibleValues><OptionValue><Value>true</Value><Description>Yes</Description></OptionValue><OptionValue><Value>false</Value><Description>No</Description></OptionValue></PossibleValues><ChildServiceOptions><Option><ID>SpecialHandlingType</ID><Description>Type of Special Handling</Description><ValueType>Enumeration</ValueType><AvailableForPieces>true</AvailableForPieces><PossibleValues><OptionValue><Value>FlatPackage</Value><Description>Flat Package</Description></OptionValue><OptionValue><Value>AdditionalHandling</Value><Description>Additional Handling</Description></OptionValue><OptionValue><Value>LargePackage</Value><Description>Large package</Description></OptionValue><OptionValue><Value>Oversized</Value><Description>Oversized</Description></OptionValue></PossibleValues><ChildServiceOptions/></Option></ChildServiceOptions></Option></Options></Service><Service><ID>PurolatorGround10:30AM</ID><Description>Purolator Ground 10:30AM</Description><PackageType>CustomerPackaging</PackageType><PackageTypeDescription>Customer Packaging</PackageTypeDescription><Options><Option><ID>ChainOfSignature</ID><Description>Chain Of Signature</Description><ValueType>Enumeration</ValueType><AvailableForPieces>false</AvailableForPieces><PossibleValues><OptionValue><Value>true</Value><Description>Yes</Description></OptionValue><OptionValue><Value>false</Value><Description>No</Description></OptionValue></PossibleValues><ChildServiceOptions/></Option><Option><ID>DangerousGoods</ID><Description>Dangerous Goods Indicator</Description><ValueType>Enumeration</ValueType><AvailableForPieces>false</AvailableForPieces><PossibleValues><OptionValue><Value>true</Value><Description>Yes</Description></OptionValue><OptionValue><Value>false</Value><Description>No</Description></OptionValue></PossibleValues><ChildServiceOptions><Option><ID>DangerousGoodsMode</ID><Description>Dangerous Goods Mode</Description><ValueType>Enumeration</ValueType><AvailableForPieces>false</AvailableForPieces><PossibleValues><OptionValue><Value>Ground</Value><Description>Ground</Description></OptionValue></PossibleValues><ChildServiceOptions><Option><ID>DangerousGoodsClass</ID><Description>Dangerous Goods Class</Description><ValueType>Enumeration</ValueType><AvailableForPieces>false</AvailableForPieces><PossibleValues><OptionValue><Value>FullyRegulated</Value><Description>Fully Regulated</Description></OptionValue><OptionValue><Value>UN3373</Value><Description>UN3373</Description></OptionValue><OptionValue><Value>UN1845</Value><Description>UN1845</Description></OptionValue><OptionValue><Value>LessThan500kgExempt</Value><Description>&lt;500 kg Exempt</Description></OptionValue><OptionValue><Value>LimitedQuantities</Value><Description>Limited Quantities</Description></OptionValue></PossibleValues><ChildServiceOptions/></Option></ChildServiceOptions></Option></ChildServiceOptions></Option><Option><ID>DeclaredValue</ID><Description>Declared Value</Description><ValueType>Decimal</ValueType><AvailableForPieces>false</AvailableForPieces><PossibleValues/><ChildServiceOptions/></Option><Option><ID>HoldForPickup</ID><Description>Hold For Pickup</Description><ValueType>Enumeration</ValueType><AvailableForPieces>false</AvailableForPieces><PossibleValues><OptionValue><Value>true</Value><Description>Yes</Description></OptionValue><OptionValue><Value>false</Value><Description>No</Description></OptionValue></PossibleValues><ChildServiceOptions/></Option><Option><ID>OriginSignatureNotRequired</ID><Description>OSNR-Signature Not Required</Description><ValueType>Enumeration</ValueType><AvailableForPieces>false</AvailableForPieces><PossibleValues><OptionValue><Value>true</Value><Description>Yes</Description></OptionValue><OptionValue><Value>false</Value><Description>No</Description></OptionValue></PossibleValues><ChildServiceOptions/></Option><Option><ID>AdultSignatureRequired</ID><Description>Adult Signature Required</Description><ValueType>Enumeration</ValueType><AvailableForPieces>false</AvailableForPieces><PossibleValues><OptionValue><Value>true</Value><Description>Yes</Description></OptionValue><OptionValue><Value>false</Value><Description>No</Description></OptionValue></PossibleValues><ChildServiceOptions/></Option><Option><ID>SpecialHandling</ID><Description>Special Handling</Description><ValueType>Enumeration</ValueType><AvailableForPieces>true</AvailableForPieces><PossibleValues><OptionValue><Value>true</Value><Description>Yes</Description></OptionValue><OptionValue><Value>false</Value><Description>No</Description></OptionValue></PossibleValues><ChildServiceOptions><Option><ID>SpecialHandlingType</ID><Description>Type of Special Handling</Description><ValueType>Enumeration</ValueType><AvailableForPieces>true</AvailableForPieces><PossibleValues><OptionValue><Value>FlatPackage</Value><Description>Flat Package</Description></OptionValue><OptionValue><Value>AdditionalHandling</Value><Description>Additional Handling</Description></OptionValue><OptionValue><Value>LargePackage</Value><Description>Large package</Description></OptionValue><OptionValue><Value>Oversized</Value><Description>Oversized</Description></OptionValue></PossibleValues><ChildServiceOptions/></Option></ChildServiceOptions></Option></Options></Service><Service><ID>PurolatorGround</ID><Description>Purolator Ground</Description><PackageType>CustomerPackaging</PackageType><PackageTypeDescription>Customer Packaging</PackageTypeDescription><Options><Option><ID>ChainOfSignature</ID><Description>Chain Of Signature</Description><ValueType>Enumeration</ValueType><AvailableForPieces>false</AvailableForPieces><PossibleValues><OptionValue><Value>true</Value><Description>Yes</Description></OptionValue><OptionValue><Value>false</Value><Description>No</Description></OptionValue></PossibleValues><ChildServiceOptions/></Option><Option><ID>DangerousGoods</ID><Description>Dangerous Goods Indicator</Description><ValueType>Enumeration</ValueType><AvailableForPieces>false</AvailableForPieces><PossibleValues><OptionValue><Value>true</Value><Description>Yes</Description></OptionValue><OptionValue><Value>false</Value><Description>No</Description></OptionValue></PossibleValues><ChildServiceOptions><Option><ID>DangerousGoodsMode</ID><Description>Dangerous Goods Mode</Description><ValueType>Enumeration</ValueType><AvailableForPieces>false</AvailableForPieces><PossibleValues><OptionValue><Value>Ground</Value><Description>Ground</Description></OptionValue></PossibleValues><ChildServiceOptions><Option><ID>DangerousGoodsClass</ID><Description>Dangerous Goods Class</Description><ValueType>Enumeration</ValueType><AvailableForPieces>false</AvailableForPieces><PossibleValues><OptionValue><Value>FullyRegulated</Value><Description>Fully Regulated</Description></OptionValue><OptionValue><Value>UN3373</Value><Description>UN3373</Description></OptionValue><OptionValue><Value>UN1845</Value><Description>UN1845</Description></OptionValue><OptionValue><Value>LessThan500kgExempt</Value><Description>&lt;500 kg Exempt</Description></OptionValue><OptionValue><Value>LimitedQuantities</Value><Description>Limited Quantities</Description></OptionValue></PossibleValues><ChildServiceOptions/></Option></ChildServiceOptions></Option></ChildServiceOptions></Option><Option><ID>DeclaredValue</ID><Description>Declared Value</Description><ValueType>Decimal</ValueType><AvailableForPieces>false</AvailableForPieces><PossibleValues/><ChildServiceOptions/></Option><Option><ID>ExpressCheque</ID><Description>ExpressCheque</Description><ValueType>Enumeration</ValueType><AvailableForPieces>false</AvailableForPieces><PossibleValues><OptionValue><Value>true</Value><Description>Yes</Description></OptionValue><OptionValue><Value>false</Value><Description>No</Description></OptionValue></PossibleValues><ChildServiceOptions><Option><ID>ExpressChequeMethodOfPayment</ID><Description>Method of Payment</Description><ValueType>Enumeration</ValueType><AvailableForPieces>false</AvailableForPieces><PossibleValues><OptionValue><Value>Cheque</Value><Description>Cheque</Description></OptionValue><OptionValue><Value>PostDatedCheque</Value><Description>Post dated Cheque</Description></OptionValue><OptionValue><Value>CertifiedCheque</Value><Description>Certified Cheque</Description></OptionValue><OptionValue><Value>MoneyOrder</Value><Description>Money Order</Description></OptionValue><OptionValue><Value>BankDraft</Value><Description>Bank Draft</Description></OptionValue></PossibleValues><ChildServiceOptions><Option><ID>ExpressChequeAmount</ID><Description>Amount</Description><ValueType>Decimal</ValueType><AvailableForPieces>false</AvailableForPieces><PossibleValues/><ChildServiceOptions/></Option></ChildServiceOptions></Option></ChildServiceOptions></Option><Option><ID>HoldForPickup</ID><Description>Hold For Pickup</Description><ValueType>Enumeration</ValueType><AvailableForPieces>false</AvailableForPieces><PossibleValues><OptionValue><Value>true</Value><Description>Yes</Description></OptionValue><OptionValue><Value>false</Value><Description>No</Description></OptionValue></PossibleValues><ChildServiceOptions/></Option><Option><ID>OriginSignatureNotRequired</ID><Description>OSNR-Signature Not Required</Description><ValueType>Enumeration</ValueType><AvailableForPieces>false</AvailableForPieces><PossibleValues><OptionValue><Value>true</Value><Description>Yes</Description></OptionValue><OptionValue><Value>false</Value><Description>No</Description></OptionValue></PossibleValues><ChildServiceOptions/></Option><Option><ID>AdultSignatureRequired</ID><Description>Adult Signature Required</Description><ValueType>Enumeration</ValueType><AvailableForPieces>false</AvailableForPieces><PossibleValues><OptionValue><Value>true</Value><Description>Yes</Description></OptionValue><OptionValue><Value>false</Value><Description>No</Description></OptionValue></PossibleValues><ChildServiceOptions/></Option><Option><ID>SpecialHandling</ID><Description>Special Handling</Description><ValueType>Enumeration</ValueType><AvailableForPieces>true</AvailableForPieces><PossibleValues><OptionValue><Value>true</Value><Description>Yes</Description></OptionValue><OptionValue><Value>false</Value><Description>No</Description></OptionValue></PossibleValues><ChildServiceOptions><Option><ID>SpecialHandlingType</ID><Description>Type of Special Handling</Description><ValueType>Enumeration</ValueType><AvailableForPieces>true</AvailableForPieces><PossibleValues><OptionValue><Value>FlatPackage</Value><Description>Flat Package</Description></OptionValue><OptionValue><Value>AdditionalHandling</Value><Description>Additional Handling</Description></OptionValue><OptionValue><Value>LargePackage</Value><Description>Large package</Description></OptionValue><OptionValue><Value>Oversized</Value><Description>Oversized</Description></OptionValue></PossibleValues><ChildServiceOptions/></Option></ChildServiceOptions></Option></Options></Service><Service><ID>PurolatorExpressEnvelope9AM</ID><Description>Purolator Express Envelope 9AM</Description><PackageType>ExpressEnvelope</PackageType><PackageTypeDescription>Express Envelope</PackageTypeDescription><Options><Option><ID>ChainOfSignature</ID><Description>Chain Of Signature</Description><ValueType>Enumeration</ValueType><AvailableForPieces>false</AvailableForPieces><PossibleValues><OptionValue><Value>true</Value><Description>Yes</Description></OptionValue><OptionValue><Value>false</Value><Description>No</Description></OptionValue></PossibleValues><ChildServiceOptions/></Option><Option><ID>DeclaredValue</ID><Description>Declared Value</Description><ValueType>Decimal</ValueType><AvailableForPieces>false</AvailableForPieces><PossibleValues/><ChildServiceOptions/></Option><Option><ID>HoldForPickup</ID><Description>Hold For Pickup</Description><ValueType>Enumeration</ValueType><AvailableForPieces>false</AvailableForPieces><PossibleValues><OptionValue><Value>true</Value><Description>Yes</Description></OptionValue><OptionValue><Value>false</Value><Description>No</Description></OptionValue></PossibleValues><ChildServiceOptions/></Option><Option><ID>OriginSignatureNotRequired</ID><Description>OSNR-Signature Not Required</Description><ValueType>Enumeration</ValueType><AvailableForPieces>false</AvailableForPieces><PossibleValues><OptionValue><Value>true</Value><Description>Yes</Description></OptionValue><OptionValue><Value>false</Value><Description>No</Description></OptionValue></PossibleValues><ChildServiceOptions/></Option><Option><ID>AdultSignatureRequired</ID><Description>Adult Signature Required</Description><ValueType>Enumeration</ValueType><AvailableForPieces>false</AvailableForPieces><PossibleValues><OptionValue><Value>true</Value><Description>Yes</Description></OptionValue><OptionValue><Value>false</Value><Description>No</Description></OptionValue></PossibleValues><ChildServiceOptions/></Option></Options></Service><Service><ID>PurolatorExpressEnvelope10:30AM</ID><Description>Purolator Express Envelope 10:30AM</Description><PackageType>ExpressEnvelope</PackageType><PackageTypeDescription>Express Envelope</PackageTypeDescription><Options><Option><ID>ChainOfSignature</ID><Description>Chain Of Signature</Description><ValueType>Enumeration</ValueType><AvailableForPieces>false</AvailableForPieces><PossibleValues><OptionValue><Value>true</Value><Description>Yes</Description></OptionValue><OptionValue><Value>false</Value><Description>No</Description></OptionValue></PossibleValues><ChildServiceOptions/></Option><Option><ID>DeclaredValue</ID><Description>Declared Value</Description><ValueType>Decimal</ValueType><AvailableForPieces>false</AvailableForPieces><PossibleValues/><ChildServiceOptions/></Option><Option><ID>HoldForPickup</ID><Description>Hold For Pickup</Description><ValueType>Enumeration</ValueType><AvailableForPieces>false</AvailableForPieces><PossibleValues><OptionValue><Value>true</Value><Description>Yes</Description></OptionValue><OptionValue><Value>false</Value><Description>No</Description></OptionValue></PossibleValues><ChildServiceOptions/></Option><Option><ID>OriginSignatureNotRequired</ID><Description>OSNR-Signature Not Required</Description><ValueType>Enumeration</ValueType><AvailableForPieces>false</AvailableForPieces><PossibleValues><OptionValue><Value>true</Value><Description>Yes</Description></OptionValue><OptionValue><Value>false</Value><Description>No</Description></OptionValue></PossibleValues><ChildServiceOptions/></Option><Option><ID>AdultSignatureRequired</ID><Description>Adult Signature Required</Description><ValueType>Enumeration</ValueType><AvailableForPieces>false</AvailableForPieces><PossibleValues><OptionValue><Value>true</Value><Description>Yes</Description></OptionValue><OptionValue><Value>false</Value><Description>No</Description></OptionValue></PossibleValues><ChildServiceOptions/></Option></Options></Service><Service><ID>PurolatorExpressEnvelope12PM</ID><Description>Purolator Express Envelope 12PM</Description><PackageType>ExpressEnvelope</PackageType><PackageTypeDescription>Express Envelope</PackageTypeDescription><Options><Option><ID>ChainOfSignature</ID><Description>Chain Of Signature</Description><ValueType>Enumeration</ValueType><AvailableForPieces>false</AvailableForPieces><PossibleValues><OptionValue><Value>true</Value><Description>Yes</Description></OptionValue><OptionValue><Value>false</Value><Description>No</Description></OptionValue></PossibleValues><ChildServiceOptions/></Option><Option><ID>DeclaredValue</ID><Description>Declared Value</Description><ValueType>Decimal</ValueType><AvailableForPieces>false</AvailableForPieces><PossibleValues/><ChildServiceOptions/></Option><Option><ID>HoldForPickup</ID><Description>Hold For Pickup</Description><ValueType>Enumeration</ValueType><AvailableForPieces>false</AvailableForPieces><PossibleValues><OptionValue><Value>true</Value><Description>Yes</Description></OptionValue><OptionValue><Value>false</Value><Description>No</Description></OptionValue></PossibleValues><ChildServiceOptions/></Option><Option><ID>OriginSignatureNotRequired</ID><Description>OSNR-Signature Not Required</Description><ValueType>Enumeration</ValueType><AvailableForPieces>false</AvailableForPieces><PossibleValues><OptionValue><Value>true</Value><Description>Yes</Description></OptionValue><OptionValue><Value>false</Value><Description>No</Description></OptionValue></PossibleValues><ChildServiceOptions/></Option><Option><ID>AdultSignatureRequired</ID><Description>Adult Signature Required</Description><ValueType>Enumeration</ValueType><AvailableForPieces>false</AvailableForPieces><PossibleValues><OptionValue><Value>true</Value><Description>Yes</Description></OptionValue><OptionValue><Value>false</Value><Description>No</Description></OptionValue></PossibleValues><ChildServiceOptions/></Option></Options></Service><Service><ID>PurolatorExpressEnvelope</ID><Description>Purolator Express Envelope</Description><PackageType>ExpressEnvelope</PackageType><PackageTypeDescription>Express Envelope</PackageTypeDescription><Options><Option><ID>ChainOfSignature</ID><Description>Chain Of Signature</Description><ValueType>Enumeration</ValueType><AvailableForPieces>false</AvailableForPieces><PossibleValues><OptionValue><Value>true</Value><Description>Yes</Description></OptionValue><OptionValue><Value>false</Value><Description>No</Description></OptionValue></PossibleValues><ChildServiceOptions/></Option><Option><ID>DeclaredValue</ID><Description>Declared Value</Description><ValueType>Decimal</ValueType><AvailableForPieces>false</AvailableForPieces><PossibleValues/><ChildServiceOptions/></Option><Option><ID>ExpressCheque</ID><Description>ExpressCheque</Description><ValueType>Enumeration</ValueType><AvailableForPieces>false</AvailableForPieces><PossibleValues><OptionValue><Value>true</Value><Description>Yes</Description></OptionValue><OptionValue><Value>false</Value><Description>No</Description></OptionValue></PossibleValues><ChildServiceOptions><Option><ID>ExpressChequeMethodOfPayment</ID><Description>Method of Payment</Description><ValueType>Enumeration</ValueType><AvailableForPieces>false</AvailableForPieces><PossibleValues><OptionValue><Value>Cheque</Value><Description>Cheque</Description></OptionValue><OptionValue><Value>PostDatedCheque</Value><Description>Post dated Cheque</Description></OptionValue><OptionValue><Value>CertifiedCheque</Value><Description>Certified Cheque</Description></OptionValue><OptionValue><Value>MoneyOrder</Value><Description>Money Order</Description></OptionValue><OptionValue><Value>BankDraft</Value><Description>Bank Draft</Description></OptionValue></PossibleValues><ChildServiceOptions><Option><ID>ExpressChequeAmount</ID><Description>Amount</Description><ValueType>Decimal</ValueType><AvailableForPieces>false</AvailableForPieces><PossibleValues/><ChildServiceOptions/></Option></ChildServiceOptions></Option></ChildServiceOptions></Option><Option><ID>HoldForPickup</ID><Description>Hold For Pickup</Description><ValueType>Enumeration</ValueType><AvailableForPieces>false</AvailableForPieces><PossibleValues><OptionValue><Value>true</Value><Description>Yes</Description></OptionValue><OptionValue><Value>false</Value><Description>No</Description></OptionValue></PossibleValues><ChildServiceOptions/></Option><Option><ID>OriginSignatureNotRequired</ID><Description>OSNR-Signature Not Required</Description><ValueType>Enumeration</ValueType><AvailableForPieces>false</AvailableForPieces><PossibleValues><OptionValue><Value>true</Value><Description>Yes</Description></OptionValue><OptionValue><Value>false</Value><Description>No</Description></OptionValue></PossibleValues><ChildServiceOptions/></Option><Option><ID>AdultSignatureRequired</ID><Description>Adult Signature Required</Description><ValueType>Enumeration</ValueType><AvailableForPieces>false</AvailableForPieces><PossibleValues><OptionValue><Value>true</Value><Description>Yes</Description></OptionValue><OptionValue><Value>false</Value><Description>No</Description></OptionValue></PossibleValues><ChildServiceOptions/></Option></Options></Service><Service><ID>PurolatorExpressPack9AM</ID><Description>Purolator Express Pack 9AM</Description><PackageType>ExpressPack</PackageType><PackageTypeDescription>Express Pack</PackageTypeDescription><Options><Option><ID>ChainOfSignature</ID><Description>Chain Of Signature</Description><ValueType>Enumeration</ValueType><AvailableForPieces>false</AvailableForPieces><PossibleValues><OptionValue><Value>true</Value><Description>Yes</Description></OptionValue><OptionValue><Value>false</Value><Description>No</Description></OptionValue></PossibleValues><ChildServiceOptions/></Option><Option><ID>DeclaredValue</ID><Description>Declared Value</Description><ValueType>Decimal</ValueType><AvailableForPieces>false</AvailableForPieces><PossibleValues/><ChildServiceOptions/></Option><Option><ID>HoldForPickup</ID><Description>Hold For Pickup</Description><ValueType>Enumeration</ValueType><AvailableForPieces>false</AvailableForPieces><PossibleValues><OptionValue><Value>true</Value><Description>Yes</Description></OptionValue><OptionValue><Value>false</Value><Description>No</Description></OptionValue></PossibleValues><ChildServiceOptions/></Option><Option><ID>OriginSignatureNotRequired</ID><Description>OSNR-Signature Not Required</Description><ValueType>Enumeration</ValueType><AvailableForPieces>false</AvailableForPieces><PossibleValues><OptionValue><Value>true</Value><Description>Yes</Description></OptionValue><OptionValue><Value>false</Value><Description>No</Description></OptionValue></PossibleValues><ChildServiceOptions/></Option><Option><ID>AdultSignatureRequired</ID><Description>Adult Signature Required</Description><ValueType>Enumeration</ValueType><AvailableForPieces>false</AvailableForPieces><PossibleValues><OptionValue><Value>true</Value><Description>Yes</Description></OptionValue><OptionValue><Value>false</Value><Description>No</Description></OptionValue></PossibleValues><ChildServiceOptions/></Option></Options></Service><Service><ID>PurolatorExpressPack10:30AM</ID><Description>Purolator Express Pack 10:30AM</Description><PackageType>ExpressPack</PackageType><PackageTypeDescription>Express Pack</PackageTypeDescription><Options><Option><ID>ChainOfSignature</ID><Description>Chain Of Signature</Description><ValueType>Enumeration</ValueType><AvailableForPieces>false</AvailableForPieces><PossibleValues><OptionValue><Value>true</Value><Description>Yes</Description></OptionValue><OptionValue><Value>false</Value><Description>No</Description></OptionValue></PossibleValues><ChildServiceOptions/></Option><Option><ID>DeclaredValue</ID><Description>Declared Value</Description><ValueType>Decimal</ValueType><AvailableForPieces>false</AvailableForPieces><PossibleValues/><ChildServiceOptions/></Option><Option><ID>HoldForPickup</ID><Description>Hold For Pickup</Description><ValueType>Enumeration</ValueType><AvailableForPieces>false</AvailableForPieces><PossibleValues><OptionValue><Value>true</Value><Description>Yes</Description></OptionValue><OptionValue><Value>false</Value><Description>No</Description></OptionValue></PossibleValues><ChildServiceOptions/></Option><Option><ID>OriginSignatureNotRequired</ID><Description>OSNR-Signature Not Required</Description><ValueType>Enumeration</ValueType><AvailableForPieces>false</AvailableForPieces><PossibleValues><OptionValue><Value>true</Value><Description>Yes</Description></OptionValue><OptionValue><Value>false</Value><Description>No</Description></OptionValue></PossibleValues><ChildServiceOptions/></Option><Option><ID>AdultSignatureRequired</ID><Description>Adult Signature Required</Description><ValueType>Enumeration</ValueType><AvailableForPieces>false</AvailableForPieces><PossibleValues><OptionValue><Value>true</Value><Description>Yes</Description></OptionValue><OptionValue><Value>false</Value><Description>No</Description></OptionValue></PossibleValues><ChildServiceOptions/></Option></Options></Service><Service><ID>PurolatorExpressPack12PM</ID><Description>Purolator Express Pack 12PM</Description><PackageType>ExpressPack</PackageType><PackageTypeDescription>Express Pack</PackageTypeDescription><Options><Option><ID>ChainOfSignature</ID><Description>Chain Of Signature</Description><ValueType>Enumeration</ValueType><AvailableForPieces>false</AvailableForPieces><PossibleValues><OptionValue><Value>true</Value><Description>Yes</Description></OptionValue><OptionValue><Value>false</Value><Description>No</Description></OptionValue></PossibleValues><ChildServiceOptions/></Option><Option><ID>DeclaredValue</ID><Description>Declared Value</Description><ValueType>Decimal</ValueType><AvailableForPieces>false</AvailableForPieces><PossibleValues/><ChildServiceOptions/></Option><Option><ID>HoldForPickup</ID><Description>Hold For Pickup</Description><ValueType>Enumeration</ValueType><AvailableForPieces>false</AvailableForPieces><PossibleValues><OptionValue><Value>true</Value><Description>Yes</Description></OptionValue><OptionValue><Value>false</Value><Description>No</Description></OptionValue></PossibleValues><ChildServiceOptions/></Option><Option><ID>OriginSignatureNotRequired</ID><Description>OSNR-Signature Not Required</Description><ValueType>Enumeration</ValueType><AvailableForPieces>false</AvailableForPieces><PossibleValues><OptionValue><Value>true</Value><Description>Yes</Description></OptionValue><OptionValue><Value>false</Value><Description>No</Description></OptionValue></PossibleValues><ChildServiceOptions/></Option><Option><ID>AdultSignatureRequired</ID><Description>Adult Signature Required</Description><ValueType>Enumeration</ValueType><AvailableForPieces>false</AvailableForPieces><PossibleValues><OptionValue><Value>true</Value><Description>Yes</Description></OptionValue><OptionValue><Value>false</Value><Description>No</Description></OptionValue></PossibleValues><ChildServiceOptions/></Option></Options></Service><Service><ID>PurolatorExpressPack</ID><Description>Purolator Express Pack</Description><PackageType>ExpressPack</PackageType><PackageTypeDescription>Express Pack</PackageTypeDescription><Options><Option><ID>ChainOfSignature</ID><Description>Chain Of Signature</Description><ValueType>Enumeration</ValueType><AvailableForPieces>false</AvailableForPieces><PossibleValues><OptionValue><Value>true</Value><Description>Yes</Description></OptionValue><OptionValue><Value>false</Value><Description>No</Description></OptionValue></PossibleValues><ChildServiceOptions/></Option><Option><ID>DeclaredValue</ID><Description>Declared Value</Description><ValueType>Decimal</ValueType><AvailableForPieces>false</AvailableForPieces><PossibleValues/><ChildServiceOptions/></Option><Option><ID>ExpressCheque</ID><Description>ExpressCheque</Description><ValueType>Enumeration</ValueType><AvailableForPieces>false</AvailableForPieces><PossibleValues><OptionValue><Value>true</Value><Description>Yes</Description></OptionValue><OptionValue><Value>false</Value><Description>No</Description></OptionValue></PossibleValues><ChildServiceOptions><Option><ID>ExpressChequeMethodOfPayment</ID><Description>Method of Payment</Description><ValueType>Enumeration</ValueType><AvailableForPieces>false</AvailableForPieces><PossibleValues><OptionValue><Value>Cheque</Value><Description>Cheque</Description></OptionValue><OptionValue><Value>PostDatedCheque</Value><Description>Post dated Cheque</Description></OptionValue><OptionValue><Value>CertifiedCheque</Value><Description>Certified Cheque</Description></OptionValue><OptionValue><Value>MoneyOrder</Value><Description>Money Order</Description></OptionValue><OptionValue><Value>BankDraft</Value><Description>Bank Draft</Description></OptionValue></PossibleValues><ChildServiceOptions><Option><ID>ExpressChequeAmount</ID><Description>Amount</Description><ValueType>Decimal</ValueType><AvailableForPieces>false</AvailableForPieces><PossibleValues/><ChildServiceOptions/></Option></ChildServiceOptions></Option></ChildServiceOptions></Option><Option><ID>HoldForPickup</ID><Description>Hold For Pickup</Description><ValueType>Enumeration</ValueType><AvailableForPieces>false</AvailableForPieces><PossibleValues><OptionValue><Value>true</Value><Description>Yes</Description></OptionValue><OptionValue><Value>false</Value><Description>No</Description></OptionValue></PossibleValues><ChildServiceOptions/></Option><Option><ID>OriginSignatureNotRequired</ID><Description>OSNR-Signature Not Required</Description><ValueType>Enumeration</ValueType><AvailableForPieces>false</AvailableForPieces><PossibleValues><OptionValue><Value>true</Value><Description>Yes</Description></OptionValue><OptionValue><Value>false</Value><Description>No</Description></OptionValue></PossibleValues><ChildServiceOptions/></Option><Option><ID>AdultSignatureRequired</ID><Description>Adult Signature Required</Description><ValueType>Enumeration</ValueType><AvailableForPieces>false</AvailableForPieces><PossibleValues><OptionValue><Value>true</Value><Description>Yes</Description></OptionValue><OptionValue><Value>false</Value><Description>No</Description></OptionValue></PossibleValues><ChildServiceOptions/></Option></Options></Service><Service><ID>PurolatorExpressBox9AM</ID><Description>Purolator Express Box 9AM</Description><PackageType>ExpressBox</PackageType><PackageTypeDescription>Express Box</PackageTypeDescription><Options><Option><ID>ChainOfSignature</ID><Description>Chain Of Signature</Description><ValueType>Enumeration</ValueType><AvailableForPieces>false</AvailableForPieces><PossibleValues><OptionValue><Value>true</Value><Description>Yes</Description></OptionValue><OptionValue><Value>false</Value><Description>No</Description></OptionValue></PossibleValues><ChildServiceOptions/></Option><Option><ID>DangerousGoods</ID><Description>Dangerous Goods Indicator</Description><ValueType>Enumeration</ValueType><AvailableForPieces>false</AvailableForPieces><PossibleValues><OptionValue><Value>true</Value><Description>Yes</Description></OptionValue><OptionValue><Value>false</Value><Description>No</Description></OptionValue></PossibleValues><ChildServiceOptions><Option><ID>DangerousGoodsMode</ID><Description>Dangerous Goods Mode</Description><ValueType>Enumeration</ValueType><AvailableForPieces>false</AvailableForPieces><PossibleValues><OptionValue><Value>Air</Value><Description>Air</Description></OptionValue></PossibleValues><ChildServiceOptions><Option><ID>DangerousGoodsClass</ID><Description>Dangerous Goods Class</Description><ValueType>Enumeration</ValueType><AvailableForPieces>false</AvailableForPieces><PossibleValues><OptionValue><Value>FullyRegulated</Value><Description>Fully Regulated</Description></OptionValue><OptionValue><Value>UN3373</Value><Description>UN3373</Description></OptionValue><OptionValue><Value>UN1845</Value><Description>UN1845</Description></OptionValue></PossibleValues><ChildServiceOptions/></Option></ChildServiceOptions></Option></ChildServiceOptions></Option><Option><ID>DeclaredValue</ID><Description>Declared Value</Description><ValueType>Decimal</ValueType><AvailableForPieces>false</AvailableForPieces><PossibleValues/><ChildServiceOptions/></Option><Option><ID>HoldForPickup</ID><Description>Hold For Pickup</Description><ValueType>Enumeration</ValueType><AvailableForPieces>false</AvailableForPieces><PossibleValues><OptionValue><Value>true</Value><Description>Yes</Description></OptionValue><OptionValue><Value>false</Value><Description>No</Description></OptionValue></PossibleValues><ChildServiceOptions/></Option><Option><ID>OriginSignatureNotRequired</ID><Description>OSNR-Signature Not Required</Description><ValueType>Enumeration</ValueType><AvailableForPieces>false</AvailableForPieces><PossibleValues><OptionValue><Value>true</Value><Description>Yes</Description></OptionValue><OptionValue><Value>false</Value><Description>No</Description></OptionValue></PossibleValues><ChildServiceOptions/></Option><Option><ID>AdultSignatureRequired</ID><Description>Adult Signature Required</Description><ValueType>Enumeration</ValueType><AvailableForPieces>false</AvailableForPieces><PossibleValues><OptionValue><Value>true</Value><Description>Yes</Description></OptionValue><OptionValue><Value>false</Value><Description>No</Description></OptionValue></PossibleValues><ChildServiceOptions/></Option><Option><ID>SpecialHandling</ID><Description>Special Handling</Description><ValueType>Enumeration</ValueType><AvailableForPieces>true</AvailableForPieces><PossibleValues><OptionValue><Value>true</Value><Description>Yes</Description></OptionValue><OptionValue><Value>false</Value><Description>No</Description></OptionValue></PossibleValues><ChildServiceOptions><Option><ID>SpecialHandlingType</ID><Description>Type of Special Handling</Description><ValueType>Enumeration</ValueType><AvailableForPieces>true</AvailableForPieces><PossibleValues><OptionValue><Value>FlatPackage</Value><Description>Flat Package</Description></OptionValue><OptionValue><Value>AdditionalHandling</Value><Description>Additional Handling</Description></OptionValue><OptionValue><Value>LargePackage</Value><Description>Large package</Description></OptionValue><OptionValue><Value>Oversized</Value><Description>Oversized</Description></OptionValue></PossibleValues><ChildServiceOptions/></Option></ChildServiceOptions></Option></Options></Service><Service><ID>PurolatorExpressBox10:30AM</ID><Description>Purolator Express Box 10:30AM</Description><PackageType>ExpressBox</PackageType><PackageTypeDescription>Express Box</PackageTypeDescription><Options><Option><ID>ChainOfSignature</ID><Description>Chain Of Signature</Description><ValueType>Enumeration</ValueType><AvailableForPieces>false</AvailableForPieces><PossibleValues><OptionValue><Value>true</Value><Description>Yes</Description></OptionValue><OptionValue><Value>false</Value><Description>No</Description></OptionValue></PossibleValues><ChildServiceOptions/></Option><Option><ID>DangerousGoods</ID><Description>Dangerous Goods Indicator</Description><ValueType>Enumeration</ValueType><AvailableForPieces>false</AvailableForPieces><PossibleValues><OptionValue><Value>true</Value><Description>Yes</Description></OptionValue><OptionValue><Value>false</Value><Description>No</Description></OptionValue></PossibleValues><ChildServiceOptions><Option><ID>DangerousGoodsMode</ID><Description>Dangerous Goods Mode</Description><ValueType>Enumeration</ValueType><AvailableForPieces>false</AvailableForPieces><PossibleValues><OptionValue><Value>Air</Value><Description>Air</Description></OptionValue></PossibleValues><ChildServiceOptions><Option><ID>DangerousGoodsClass</ID><Description>Dangerous Goods Class</Description><ValueType>Enumeration</ValueType><AvailableForPieces>false</AvailableForPieces><PossibleValues><OptionValue><Value>FullyRegulated</Value><Description>Fully Regulated</Description></OptionValue><OptionValue><Value>UN3373</Value><Description>UN3373</Description></OptionValue><OptionValue><Value>UN1845</Value><Description>UN1845</Description></OptionValue></PossibleValues><ChildServiceOptions/></Option></ChildServiceOptions></Option></ChildServiceOptions></Option><Option><ID>DeclaredValue</ID><Description>Declared Value</Description><ValueType>Decimal</ValueType><AvailableForPieces>false</AvailableForPieces><PossibleValues/><ChildServiceOptions/></Option><Option><ID>HoldForPickup</ID><Description>Hold For Pickup</Description><ValueType>Enumeration</ValueType><AvailableForPieces>false</AvailableForPieces><PossibleValues><OptionValue><Value>true</Value><Description>Yes</Description></OptionValue><OptionValue><Value>false</Value><Description>No</Description></OptionValue></PossibleValues><ChildServiceOptions/></Option><Option><ID>OriginSignatureNotRequired</ID><Description>OSNR-Signature Not Required</Description><ValueType>Enumeration</ValueType><AvailableForPieces>false</AvailableForPieces><PossibleValues><OptionValue><Value>true</Value><Description>Yes</Description></OptionValue><OptionValue><Value>false</Value><Description>No</Description></OptionValue></PossibleValues><ChildServiceOptions/></Option><Option><ID>AdultSignatureRequired</ID><Description>Adult Signature Required</Description><ValueType>Enumeration</ValueType><AvailableForPieces>false</AvailableForPieces><PossibleValues><OptionValue><Value>true</Value><Description>Yes</Description></OptionValue><OptionValue><Value>false</Value><Description>No</Description></OptionValue></PossibleValues><ChildServiceOptions/></Option><Option><ID>SpecialHandling</ID><Description>Special Handling</Description><ValueType>Enumeration</ValueType><AvailableForPieces>true</AvailableForPieces><PossibleValues><OptionValue><Value>true</Value><Description>Yes</Description></OptionValue><OptionValue><Value>false</Value><Description>No</Description></OptionValue></PossibleValues><ChildServiceOptions><Option><ID>SpecialHandlingType</ID><Description>Type of Special Handling</Description><ValueType>Enumeration</ValueType><AvailableForPieces>true</AvailableForPieces><PossibleValues><OptionValue><Value>FlatPackage</Value><Description>Flat Package</Description></OptionValue><OptionValue><Value>AdditionalHandling</Value><Description>Additional Handling</Description></OptionValue><OptionValue><Value>LargePackage</Value><Description>Large package</Description></OptionValue><OptionValue><Value>Oversized</Value><Description>Oversized</Description></OptionValue></PossibleValues><ChildServiceOptions/></Option></ChildServiceOptions></Option></Options></Service><Service><ID>PurolatorExpressBox12PM</ID><Description>Purolator Express Box 12PM</Description><PackageType>ExpressBox</PackageType><PackageTypeDescription>Express Box</PackageTypeDescription><Options><Option><ID>ChainOfSignature</ID><Description>Chain Of Signature</Description><ValueType>Enumeration</ValueType><AvailableForPieces>false</AvailableForPieces><PossibleValues><OptionValue><Value>true</Value><Description>Yes</Description></OptionValue><OptionValue><Value>false</Value><Description>No</Description></OptionValue></PossibleValues><ChildServiceOptions/></Option><Option><ID>DangerousGoods</ID><Description>Dangerous Goods Indicator</Description><ValueType>Enumeration</ValueType><AvailableForPieces>false</AvailableForPieces><PossibleValues><OptionValue><Value>true</Value><Description>Yes</Description></OptionValue><OptionValue><Value>false</Value><Description>No</Description></OptionValue></PossibleValues><ChildServiceOptions><Option><ID>DangerousGoodsMode</ID><Description>Dangerous Goods Mode</Description><ValueType>Enumeration</ValueType><AvailableForPieces>false</AvailableForPieces><PossibleValues><OptionValue><Value>Air</Value><Description>Air</Description></OptionValue></PossibleValues><ChildServiceOptions><Option><ID>DangerousGoodsClass</ID><Description>Dangerous Goods Class</Description><ValueType>Enumeration</ValueType><AvailableForPieces>false</AvailableForPieces><PossibleValues><OptionValue><Value>FullyRegulated</Value><Description>Fully Regulated</Description></OptionValue><OptionValue><Value>UN3373</Value><Description>UN3373</Description></OptionValue><OptionValue><Value>UN1845</Value><Description>UN1845</Description></OptionValue></PossibleValues><ChildServiceOptions/></Option></ChildServiceOptions></Option></ChildServiceOptions></Option><Option><ID>DeclaredValue</ID><Description>Declared Value</Description><ValueType>Decimal</ValueType><AvailableForPieces>false</AvailableForPieces><PossibleValues/><ChildServiceOptions/></Option><Option><ID>HoldForPickup</ID><Description>Hold For Pickup</Description><ValueType>Enumeration</ValueType><AvailableForPieces>false</AvailableForPieces><PossibleValues><OptionValue><Value>true</Value><Description>Yes</Description></OptionValue><OptionValue><Value>false</Value><Description>No</Description></OptionValue></PossibleValues><ChildServiceOptions/></Option><Option><ID>OriginSignatureNotRequired</ID><Description>OSNR-Signature Not Required</Description><ValueType>Enumeration</ValueType><AvailableForPieces>false</AvailableForPieces><PossibleValues><OptionValue><Value>true</Value><Description>Yes</Description></OptionValue><OptionValue><Value>false</Value><Description>No</Description></OptionValue></PossibleValues><ChildServiceOptions/></Option><Option><ID>AdultSignatureRequired</ID><Description>Adult Signature Required</Description><ValueType>Enumeration</ValueType><AvailableForPieces>false</AvailableForPieces><PossibleValues><OptionValue><Value>true</Value><Description>Yes</Description></OptionValue><OptionValue><Value>false</Value><Description>No</Description></OptionValue></PossibleValues><ChildServiceOptions/></Option><Option><ID>SpecialHandling</ID><Description>Special Handling</Description><ValueType>Enumeration</ValueType><AvailableForPieces>true</AvailableForPieces><PossibleValues><OptionValue><Value>true</Value><Description>Yes</Description></OptionValue><OptionValue><Value>false</Value><Description>No</Description></OptionValue></PossibleValues><ChildServiceOptions><Option><ID>SpecialHandlingType</ID><Description>Type of Special Handling</Description><ValueType>Enumeration</ValueType><AvailableForPieces>true</AvailableForPieces><PossibleValues><OptionValue><Value>FlatPackage</Value><Description>Flat Package</Description></OptionValue><OptionValue><Value>AdditionalHandling</Value><Description>Additional Handling</Description></OptionValue><OptionValue><Value>LargePackage</Value><Description>Large package</Description></OptionValue><OptionValue><Value>Oversized</Value><Description>Oversized</Description></OptionValue></PossibleValues><ChildServiceOptions/></Option></ChildServiceOptions></Option></Options></Service><Service><ID>PurolatorExpressBox</ID><Description>Purolator Express Box</Description><PackageType>ExpressBox</PackageType><PackageTypeDescription>Express Box</PackageTypeDescription><Options><Option><ID>ChainOfSignature</ID><Description>Chain Of Signature</Description><ValueType>Enumeration</ValueType><AvailableForPieces>false</AvailableForPieces><PossibleValues><OptionValue><Value>true</Value><Description>Yes</Description></OptionValue><OptionValue><Value>false</Value><Description>No</Description></OptionValue></PossibleValues><ChildServiceOptions/></Option><Option><ID>DangerousGoods</ID><Description>Dangerous Goods Indicator</Description><ValueType>Enumeration</ValueType><AvailableForPieces>false</AvailableForPieces><PossibleValues><OptionValue><Value>true</Value><Description>Yes</Description></OptionValue><OptionValue><Value>false</Value><Description>No</Description></OptionValue></PossibleValues><ChildServiceOptions><Option><ID>DangerousGoodsMode</ID><Description>Dangerous Goods Mode</Description><ValueType>Enumeration</ValueType><AvailableForPieces>false</AvailableForPieces><PossibleValues><OptionValue><Value>Air</Value><Description>Air</Description></OptionValue></PossibleValues><ChildServiceOptions><Option><ID>DangerousGoodsClass</ID><Description>Dangerous Goods Class</Description><ValueType>Enumeration</ValueType><AvailableForPieces>false</AvailableForPieces><PossibleValues><OptionValue><Value>FullyRegulated</Value><Description>Fully Regulated</Description></OptionValue><OptionValue><Value>UN3373</Value><Description>UN3373</Description></OptionValue><OptionValue><Value>UN1845</Value><Description>UN1845</Description></OptionValue></PossibleValues><ChildServiceOptions/></Option></ChildServiceOptions></Option></ChildServiceOptions></Option><Option><ID>DeclaredValue</ID><Description>Declared Value</Description><ValueType>Decimal</ValueType><AvailableForPieces>false</AvailableForPieces><PossibleValues/><ChildServiceOptions/></Option><Option><ID>ExpressCheque</ID><Description>ExpressCheque</Description><ValueType>Enumeration</ValueType><AvailableForPieces>false</AvailableForPieces><PossibleValues><OptionValue><Value>true</Value><Description>Yes</Description></OptionValue><OptionValue><Value>false</Value><Description>No</Description></OptionValue></PossibleValues><ChildServiceOptions><Option><ID>ExpressChequeMethodOfPayment</ID><Description>Method of Payment</Description><ValueType>Enumeration</ValueType><AvailableForPieces>false</AvailableForPieces><PossibleValues><OptionValue><Value>Cheque</Value><Description>Cheque</Description></OptionValue><OptionValue><Value>PostDatedCheque</Value><Description>Post dated Cheque</Description></OptionValue><OptionValue><Value>CertifiedCheque</Value><Description>Certified Cheque</Description></OptionValue><OptionValue><Value>MoneyOrder</Value><Description>Money Order</Description></OptionValue><OptionValue><Value>BankDraft</Value><Description>Bank Draft</Description></OptionValue></PossibleValues><ChildServiceOptions><Option><ID>ExpressChequeAmount</ID><Description>Amount</Description><ValueType>Decimal</ValueType><AvailableForPieces>false</AvailableForPieces><PossibleValues/><ChildServiceOptions/></Option></ChildServiceOptions></Option></ChildServiceOptions></Option><Option><ID>HoldForPickup</ID><Description>Hold For Pickup</Description><ValueType>Enumeration</ValueType><AvailableForPieces>false</AvailableForPieces><PossibleValues><OptionValue><Value>true</Value><Description>Yes</Description></OptionValue><OptionValue><Value>false</Value><Description>No</Description></OptionValue></PossibleValues><ChildServiceOptions/></Option><Option><ID>OriginSignatureNotRequired</ID><Description>OSNR-Signature Not Required</Description><ValueType>Enumeration</ValueType><AvailableForPieces>false</AvailableForPieces><PossibleValues><OptionValue><Value>true</Value><Description>Yes</Description></OptionValue><OptionValue><Value>false</Value><Description>No</Description></OptionValue></PossibleValues><ChildServiceOptions/></Option><Option><ID>AdultSignatureRequired</ID><Description>Adult Signature Required</Description><ValueType>Enumeration</ValueType><AvailableForPieces>false</AvailableForPieces><PossibleValues><OptionValue><Value>true</Value><Description>Yes</Description></OptionValue><OptionValue><Value>false</Value><Description>No</Description></OptionValue></PossibleValues><ChildServiceOptions/></Option><Option><ID>SpecialHandling</ID><Description>Special Handling</Description><ValueType>Enumeration</ValueType><AvailableForPieces>true</AvailableForPieces><PossibleValues><OptionValue><Value>true</Value><Description>Yes</Description></OptionValue><OptionValue><Value>false</Value><Description>No</Description></OptionValue></PossibleValues><ChildServiceOptions><Option><ID>SpecialHandlingType</ID><Description>Type of Special Handling</Description><ValueType>Enumeration</ValueType><AvailableForPieces>true</AvailableForPieces><PossibleValues><OptionValue><Value>FlatPackage</Value><Description>Flat Package</Description></OptionValue><OptionValue><Value>AdditionalHandling</Value><Description>Additional Handling</Description></OptionValue><OptionValue><Value>LargePackage</Value><Description>Large package</Description></OptionValue><OptionValue><Value>Oversized</Value><Description>Oversized</Description></OptionValue></PossibleValues><ChildServiceOptions/></Option></ChildServiceOptions></Option></Options></Service></Services></GetServicesOptionsResponse></s:Body></s:Envelope>
 **/

/*
 * EXPECTED RESULTS from PWS
{ ResponseInformation: { Errors: null },
  Services:
   { Service:
      [ { ID: 'PurolatorExpress9AM',
          Description: 'Purolator Express 9AM',
          PackageType: 'CustomerPackaging',
          PackageTypeDescription: 'Customer Packaging',
          Options:
           { Option:
              [ { ID: 'ChainOfSignature',
                  Description: 'Chain Of Signature',
                  ValueType: 'Enumeration',
                  AvailableForPieces: false,
                  PossibleValues:
                   { OptionValue:
                      [ { Value: 'true', Description: 'Yes' },
                        { Value: 'false', Description: 'No' } ] },
                  ChildServiceOptions: null },
                { ID: 'DangerousGoods',
                  Description: 'Dangerous Goods Indicator',
                  ValueType: 'Enumeration',
                  AvailableForPieces: false,
                  PossibleValues:
                   { OptionValue:
                      [ { Value: 'true', Description: 'Yes' },
                        { Value: 'false', Description: 'No' } ] },
                  ChildServiceOptions:
                   { Option:
                      [ { ID: 'DangerousGoodsMode',
                          Description: 'Dangerous Goods Mode',
                          ValueType: 'Enumeration',
                          AvailableForPieces: false,
                          PossibleValues: { OptionValue: [ { Value: 'Air', Description: 'Air' } ] },
                          ChildServiceOptions:
                           { Option:
                              [ { ID: 'DangerousGoodsClass',
                                  Description: 'Dangerous Goods Class',
                                  ValueType: 'Enumeration',
                                  AvailableForPieces: false,
                                  PossibleValues:
                                   { OptionValue:
                                      [ { Value: 'FullyRegulated', Description: 'Fully Regulated' },
                                        { Value: 'UN3373', Description: 'UN3373' },
                                        { Value: 'UN1845', Description: 'UN1845' } ] },
                                  ChildServiceOptions: null } ] } } ] } },
                { ID: 'DeclaredValue',
                  Description: 'Declared Value',
                  ValueType: 'Decimal',
                  AvailableForPieces: false,
                  PossibleValues: null,
                  ChildServiceOptions: null },
                { ID: 'HoldForPickup',
                  Description: 'Hold For Pickup',
                  ValueType: 'Enumeration',
                  AvailableForPieces: false,
                  PossibleValues:
                   { OptionValue:
                      [ { Value: 'true', Description: 'Yes' },
                        { Value: 'false', Description: 'No' } ] },
                  ChildServiceOptions: null },
                { ID: 'OriginSignatureNotRequired',
                  Description: 'OSNR-Signature Not Required',
                  ValueType: 'Enumeration',
                  AvailableForPieces: false,
                  PossibleValues:
                   { OptionValue:
                      [ { Value: 'true', Description: 'Yes' },
                        { Value: 'false', Description: 'No' } ] },
                  ChildServiceOptions: null },
                { ID: 'AdultSignatureRequired',
                  Description: 'Adult Signature Required',
                  ValueType: 'Enumeration',
                  AvailableForPieces: false,
                  PossibleValues:
                   { OptionValue:
                      [ { Value: 'true', Description: 'Yes' },
                        { Value: 'false', Description: 'No' } ] },
                  ChildServiceOptions: null },
                { ID: 'SpecialHandling',
                  Description: 'Special Handling',
                  ValueType: 'Enumeration',
                  AvailableForPieces: true,
                  PossibleValues:
                   { OptionValue:
                      [ { Value: 'true', Description: 'Yes' },
                        { Value: 'false', Description: 'No' } ] },
                  ChildServiceOptions:
                   { Option:
                      [ { ID: 'SpecialHandlingType',
                          Description: 'Type of Special Handling',
                          ValueType: 'Enumeration',
                          AvailableForPieces: true,
                          PossibleValues:
                           { OptionValue:
                              [ { Value: 'FlatPackage', Description: 'Flat Package' },
                                { Value: 'AdditionalHandling',
                                  Description: 'Additional Handling' },
                                { Value: 'LargePackage', Description: 'Large package' },
                                { Value: 'Oversized', Description: 'Oversized' } ] },
                          ChildServiceOptions: null } ] } } ] } },
        { ID: 'PurolatorExpress10:30AM',
          Description: 'Purolator Express 10:30AM',
          PackageType: 'CustomerPackaging',
          PackageTypeDescription: 'Customer Packaging',
          Options:
           { Option:
              [ { ID: 'ChainOfSignature',
                  Description: 'Chain Of Signature',
                  ValueType: 'Enumeration',
                  AvailableForPieces: false,
                  PossibleValues:
                   { OptionValue:
                      [ { Value: 'true', Description: 'Yes' },
                        { Value: 'false', Description: 'No' } ] },
                  ChildServiceOptions: null },
                { ID: 'DangerousGoods',
                  Description: 'Dangerous Goods Indicator',
                  ValueType: 'Enumeration',
                  AvailableForPieces: false,
                  PossibleValues:
                   { OptionValue:
                      [ { Value: 'true', Description: 'Yes' },
                        { Value: 'false', Description: 'No' } ] },
                  ChildServiceOptions:
                   { Option:
                      [ { ID: 'DangerousGoodsMode',
                          Description: 'Dangerous Goods Mode',
                          ValueType: 'Enumeration',
                          AvailableForPieces: false,
                          PossibleValues: { OptionValue: [ { Value: 'Air', Description: 'Air' } ] },
                          ChildServiceOptions:
                           { Option:
                              [ { ID: 'DangerousGoodsClass',
                                  Description: 'Dangerous Goods Class',
                                  ValueType: 'Enumeration',
                                  AvailableForPieces: false,
                                  PossibleValues:
                                   { OptionValue:
                                      [ { Value: 'FullyRegulated', Description: 'Fully Regulated' },
                                        { Value: 'UN3373', Description: 'UN3373' },
                                        { Value: 'UN1845', Description: 'UN1845' } ] },
                                  ChildServiceOptions: null } ] } } ] } },
                { ID: 'DeclaredValue',
                  Description: 'Declared Value',
                  ValueType: 'Decimal',
                  AvailableForPieces: false,
                  PossibleValues: null,
                  ChildServiceOptions: null },
                { ID: 'HoldForPickup',
                  Description: 'Hold For Pickup',
                  ValueType: 'Enumeration',
                  AvailableForPieces: false,
                  PossibleValues:
                   { OptionValue:
                      [ { Value: 'true', Description: 'Yes' },
                        { Value: 'false', Description: 'No' } ] },
                  ChildServiceOptions: null },
                { ID: 'OriginSignatureNotRequired',
                  Description: 'OSNR-Signature Not Required',
                  ValueType: 'Enumeration',
                  AvailableForPieces: false,
                  PossibleValues:
                   { OptionValue:
                      [ { Value: 'true', Description: 'Yes' },
                        { Value: 'false', Description: 'No' } ] },
                  ChildServiceOptions: null },
                { ID: 'AdultSignatureRequired',
                  Description: 'Adult Signature Required',
                  ValueType: 'Enumeration',
                  AvailableForPieces: false,
                  PossibleValues:
                   { OptionValue:
                      [ { Value: 'true', Description: 'Yes' },
                        { Value: 'false', Description: 'No' } ] },
                  ChildServiceOptions: null },
                { ID: 'SpecialHandling',
                  Description: 'Special Handling',
                  ValueType: 'Enumeration',
                  AvailableForPieces: true,
                  PossibleValues:
                   { OptionValue:
                      [ { Value: 'true', Description: 'Yes' },
                        { Value: 'false', Description: 'No' } ] },
                  ChildServiceOptions:
                   { Option:
                      [ { ID: 'SpecialHandlingType',
                          Description: 'Type of Special Handling',
                          ValueType: 'Enumeration',
                          AvailableForPieces: true,
                          PossibleValues:
                           { OptionValue:
                              [ { Value: 'FlatPackage', Description: 'Flat Package' },
                                { Value: 'AdditionalHandling',
                                  Description: 'Additional Handling' },
                                { Value: 'LargePackage', Description: 'Large package' },
                                { Value: 'Oversized', Description: 'Oversized' } ] },
                          ChildServiceOptions: null } ] } } ] } },
        { ID: 'PurolatorExpress12PM',
          Description: 'Purolator Express 12PM',
          PackageType: 'CustomerPackaging',
          PackageTypeDescription: 'Customer Packaging',
          Options:
           { Option:
              [ { ID: 'ChainOfSignature',
                  Description: 'Chain Of Signature',
                  ValueType: 'Enumeration',
                  AvailableForPieces: false,
                  PossibleValues:
                   { OptionValue:
                      [ { Value: 'true', Description: 'Yes' },
                        { Value: 'false', Description: 'No' } ] },
                  ChildServiceOptions: null },
                { ID: 'DangerousGoods',
                  Description: 'Dangerous Goods Indicator',
                  ValueType: 'Enumeration',
                  AvailableForPieces: false,
                  PossibleValues:
                   { OptionValue:
                      [ { Value: 'true', Description: 'Yes' },
                        { Value: 'false', Description: 'No' } ] },
                  ChildServiceOptions:
                   { Option:
                      [ { ID: 'DangerousGoodsMode',
                          Description: 'Dangerous Goods Mode',
                          ValueType: 'Enumeration',
                          AvailableForPieces: false,
                          PossibleValues: { OptionValue: [ { Value: 'Air', Description: 'Air' } ] },
                          ChildServiceOptions:
                           { Option:
                              [ { ID: 'DangerousGoodsClass',
                                  Description: 'Dangerous Goods Class',
                                  ValueType: 'Enumeration',
                                  AvailableForPieces: false,
                                  PossibleValues:
                                   { OptionValue:
                                      [ { Value: 'FullyRegulated', Description: 'Fully Regulated' },
                                        { Value: 'UN3373', Description: 'UN3373' },
                                        { Value: 'UN1845', Description: 'UN1845' } ] },
                                  ChildServiceOptions: null } ] } } ] } },
                { ID: 'DeclaredValue',
                  Description: 'Declared Value',
                  ValueType: 'Decimal',
                  AvailableForPieces: false,
                  PossibleValues: null,
                  ChildServiceOptions: null },
                { ID: 'HoldForPickup',
                  Description: 'Hold For Pickup',
                  ValueType: 'Enumeration',
                  AvailableForPieces: false,
                  PossibleValues:
                   { OptionValue:
                      [ { Value: 'true', Description: 'Yes' },
                        { Value: 'false', Description: 'No' } ] },
                  ChildServiceOptions: null },
                { ID: 'OriginSignatureNotRequired',
                  Description: 'OSNR-Signature Not Required',
                  ValueType: 'Enumeration',
                  AvailableForPieces: false,
                  PossibleValues:
                   { OptionValue:
                      [ { Value: 'true', Description: 'Yes' },
                        { Value: 'false', Description: 'No' } ] },
                  ChildServiceOptions: null },
                { ID: 'AdultSignatureRequired',
                  Description: 'Adult Signature Required',
                  ValueType: 'Enumeration',
                  AvailableForPieces: false,
                  PossibleValues:
                   { OptionValue:
                      [ { Value: 'true', Description: 'Yes' },
                        { Value: 'false', Description: 'No' } ] },
                  ChildServiceOptions: null },
                { ID: 'SpecialHandling',
                  Description: 'Special Handling',
                  ValueType: 'Enumeration',
                  AvailableForPieces: true,
                  PossibleValues:
                   { OptionValue:
                      [ { Value: 'true', Description: 'Yes' },
                        { Value: 'false', Description: 'No' } ] },
                  ChildServiceOptions:
                   { Option:
                      [ { ID: 'SpecialHandlingType',
                          Description: 'Type of Special Handling',
                          ValueType: 'Enumeration',
                          AvailableForPieces: true,
                          PossibleValues:
                           { OptionValue:
                              [ { Value: 'FlatPackage', Description: 'Flat Package' },
                                { Value: 'AdditionalHandling',
                                  Description: 'Additional Handling' },
                                { Value: 'LargePackage', Description: 'Large package' },
                                { Value: 'Oversized', Description: 'Oversized' } ] },
                          ChildServiceOptions: null } ] } } ] } },
        { ID: 'PurolatorExpress',
          Description: 'Purolator Express',
          PackageType: 'CustomerPackaging',
          PackageTypeDescription: 'Customer Packaging',
          Options:
           { Option:
              [ { ID: 'ChainOfSignature',
                  Description: 'Chain Of Signature',
                  ValueType: 'Enumeration',
                  AvailableForPieces: false,
                  PossibleValues:
                   { OptionValue:
                      [ { Value: 'true', Description: 'Yes' },
                        { Value: 'false', Description: 'No' } ] },
                  ChildServiceOptions: null },
                { ID: 'DangerousGoods',
                  Description: 'Dangerous Goods Indicator',
                  ValueType: 'Enumeration',
                  AvailableForPieces: false,
                  PossibleValues:
                   { OptionValue:
                      [ { Value: 'true', Description: 'Yes' },
                        { Value: 'false', Description: 'No' } ] },
                  ChildServiceOptions:
                   { Option:
                      [ { ID: 'DangerousGoodsMode',
                          Description: 'Dangerous Goods Mode',
                          ValueType: 'Enumeration',
                          AvailableForPieces: false,
                          PossibleValues: { OptionValue: [ { Value: 'Air', Description: 'Air' } ] },
                          ChildServiceOptions:
                           { Option:
                              [ { ID: 'DangerousGoodsClass',
                                  Description: 'Dangerous Goods Class',
                                  ValueType: 'Enumeration',
                                  AvailableForPieces: false,
                                  PossibleValues:
                                   { OptionValue:
                                      [ { Value: 'FullyRegulated', Description: 'Fully Regulated' },
                                        { Value: 'UN3373', Description: 'UN3373' },
                                        { Value: 'UN1845', Description: 'UN1845' } ] },
                                  ChildServiceOptions: null } ] } } ] } },
                { ID: 'DeclaredValue',
                  Description: 'Declared Value',
                  ValueType: 'Decimal',
                  AvailableForPieces: false,
                  PossibleValues: null,
                  ChildServiceOptions: null },
                { ID: 'ExpressCheque',
                  Description: 'ExpressCheque',
                  ValueType: 'Enumeration',
                  AvailableForPieces: false,
                  PossibleValues:
                   { OptionValue:
                      [ { Value: 'true', Description: 'Yes' },
                        { Value: 'false', Description: 'No' } ] },
                  ChildServiceOptions:
                   { Option:
                      [ { ID: 'ExpressChequeMethodOfPayment',
                          Description: 'Method of Payment',
                          ValueType: 'Enumeration',
                          AvailableForPieces: false,
                          PossibleValues:
                           { OptionValue:
                              [ { Value: 'Cheque', Description: 'Cheque' },
                                { Value: 'PostDatedCheque', Description: 'Post dated Cheque' },
                                { Value: 'CertifiedCheque', Description: 'Certified Cheque' },
                                { Value: 'MoneyOrder', Description: 'Money Order' },
                                { Value: 'BankDraft', Description: 'Bank Draft' } ] },
                          ChildServiceOptions:
                           { Option:
                              [ { ID: 'ExpressChequeAmount',
                                  Description: 'Amount',
                                  ValueType: 'Decimal',
                                  AvailableForPieces: false,
                                  PossibleValues: null,
                                  ChildServiceOptions: null } ] } } ] } },
                { ID: 'HoldForPickup',
                  Description: 'Hold For Pickup',
                  ValueType: 'Enumeration',
                  AvailableForPieces: false,
                  PossibleValues:
                   { OptionValue:
                      [ { Value: 'true', Description: 'Yes' },
                        { Value: 'false', Description: 'No' } ] },
                  ChildServiceOptions: null },
                { ID: 'OriginSignatureNotRequired',
                  Description: 'OSNR-Signature Not Required',
                  ValueType: 'Enumeration',
                  AvailableForPieces: false,
                  PossibleValues:
                   { OptionValue:
                      [ { Value: 'true', Description: 'Yes' },
                        { Value: 'false', Description: 'No' } ] },
                  ChildServiceOptions: null },
                { ID: 'AdultSignatureRequired',
                  Description: 'Adult Signature Required',
                  ValueType: 'Enumeration',
                  AvailableForPieces: false,
                  PossibleValues:
                   { OptionValue:
                      [ { Value: 'true', Description: 'Yes' },
                        { Value: 'false', Description: 'No' } ] },
                  ChildServiceOptions: null },
                { ID: 'SpecialHandling',
                  Description: 'Special Handling',
                  ValueType: 'Enumeration',
                  AvailableForPieces: true,
                  PossibleValues:
                   { OptionValue:
                      [ { Value: 'true', Description: 'Yes' },
                        { Value: 'false', Description: 'No' } ] },
                  ChildServiceOptions:
                   { Option:
                      [ { ID: 'SpecialHandlingType',
                          Description: 'Type of Special Handling',
                          ValueType: 'Enumeration',
                          AvailableForPieces: true,
                          PossibleValues:
                           { OptionValue:
                              [ { Value: 'FlatPackage', Description: 'Flat Package' },
                                { Value: 'AdditionalHandling',
                                  Description: 'Additional Handling' },
                                { Value: 'LargePackage', Description: 'Large package' },
                                { Value: 'Oversized', Description: 'Oversized' } ] },
                          ChildServiceOptions: null } ] } } ] } },
        { ID: 'PurolatorGround9AM',
          Description: 'Purolator Ground 9AM',
          PackageType: 'CustomerPackaging',
          PackageTypeDescription: 'Customer Packaging',
          Options:
           { Option:
              [ { ID: 'ChainOfSignature',
                  Description: 'Chain Of Signature',
                  ValueType: 'Enumeration',
                  AvailableForPieces: false,
                  PossibleValues:
                   { OptionValue:
                      [ { Value: 'true', Description: 'Yes' },
                        { Value: 'false', Description: 'No' } ] },
                  ChildServiceOptions: null },
                { ID: 'DangerousGoods',
                  Description: 'Dangerous Goods Indicator',
                  ValueType: 'Enumeration',
                  AvailableForPieces: false,
                  PossibleValues:
                   { OptionValue:
                      [ { Value: 'true', Description: 'Yes' },
                        { Value: 'false', Description: 'No' } ] },
                  ChildServiceOptions:
                   { Option:
                      [ { ID: 'DangerousGoodsMode',
                          Description: 'Dangerous Goods Mode',
                          ValueType: 'Enumeration',
                          AvailableForPieces: false,
                          PossibleValues: { OptionValue: [ { Value: 'Ground', Description: 'Ground' } ] },
                          ChildServiceOptions:
                           { Option:
                              [ { ID: 'DangerousGoodsClass',
                                  Description: 'Dangerous Goods Class',
                                  ValueType: 'Enumeration',
                                  AvailableForPieces: false,
                                  PossibleValues:
                                   { OptionValue:
                                      [ { Value: 'FullyRegulated', Description: 'Fully Regulated' },
                                        { Value: 'UN3373', Description: 'UN3373' },
                                        { Value: 'UN1845', Description: 'UN1845' },
                                        { Value: 'LessThan500kgExempt', Description: '<500 kg Exempt' },
                                        { Value: 'LimitedQuantities',
                                          Description: 'Limited Quantities' } ] },
                                  ChildServiceOptions: null } ] } } ] } },
                { ID: 'DeclaredValue',
                  Description: 'Declared Value',
                  ValueType: 'Decimal',
                  AvailableForPieces: false,
                  PossibleValues: null,
                  ChildServiceOptions: null },
                { ID: 'HoldForPickup',
                  Description: 'Hold For Pickup',
                  ValueType: 'Enumeration',
                  AvailableForPieces: false,
                  PossibleValues:
                   { OptionValue:
                      [ { Value: 'true', Description: 'Yes' },
                        { Value: 'false', Description: 'No' } ] },
                  ChildServiceOptions: null },
                { ID: 'OriginSignatureNotRequired',
                  Description: 'OSNR-Signature Not Required',
                  ValueType: 'Enumeration',
                  AvailableForPieces: false,
                  PossibleValues:
                   { OptionValue:
                      [ { Value: 'true', Description: 'Yes' },
                        { Value: 'false', Description: 'No' } ] },
                  ChildServiceOptions: null },
                { ID: 'AdultSignatureRequired',
                  Description: 'Adult Signature Required',
                  ValueType: 'Enumeration',
                  AvailableForPieces: false,
                  PossibleValues:
                   { OptionValue:
                      [ { Value: 'true', Description: 'Yes' },
                        { Value: 'false', Description: 'No' } ] },
                  ChildServiceOptions: null },
                { ID: 'SpecialHandling',
                  Description: 'Special Handling',
                  ValueType: 'Enumeration',
                  AvailableForPieces: true,
                  PossibleValues:
                   { OptionValue:
                      [ { Value: 'true', Description: 'Yes' },
                        { Value: 'false', Description: 'No' } ] },
                  ChildServiceOptions:
                   { Option:
                      [ { ID: 'SpecialHandlingType',
                          Description: 'Type of Special Handling',
                          ValueType: 'Enumeration',
                          AvailableForPieces: true,
                          PossibleValues:
                           { OptionValue:
                              [ { Value: 'FlatPackage', Description: 'Flat Package' },
                                { Value: 'AdditionalHandling',
                                  Description: 'Additional Handling' },
                                { Value: 'LargePackage', Description: 'Large package' },
                                { Value: 'Oversized', Description: 'Oversized' } ] },
                          ChildServiceOptions: null } ] } } ] } },
        { ID: 'PurolatorGround10:30AM',
          Description: 'Purolator Ground 10:30AM',
          PackageType: 'CustomerPackaging',
          PackageTypeDescription: 'Customer Packaging',
          Options:
           { Option:
              [ { ID: 'ChainOfSignature',
                  Description: 'Chain Of Signature',
                  ValueType: 'Enumeration',
                  AvailableForPieces: false,
                  PossibleValues:
                   { OptionValue:
                      [ { Value: 'true', Description: 'Yes' },
                        { Value: 'false', Description: 'No' } ] },
                  ChildServiceOptions: null },
                { ID: 'DangerousGoods',
                  Description: 'Dangerous Goods Indicator',
                  ValueType: 'Enumeration',
                  AvailableForPieces: false,
                  PossibleValues:
                   { OptionValue:
                      [ { Value: 'true', Description: 'Yes' },
                        { Value: 'false', Description: 'No' } ] },
                  ChildServiceOptions:
                   { Option:
                      [ { ID: 'DangerousGoodsMode',
                          Description: 'Dangerous Goods Mode',
                          ValueType: 'Enumeration',
                          AvailableForPieces: false,
                          PossibleValues: { OptionValue: [ { Value: 'Ground', Description: 'Ground' } ] },
                          ChildServiceOptions:
                           { Option:
                              [ { ID: 'DangerousGoodsClass',
                                  Description: 'Dangerous Goods Class',
                                  ValueType: 'Enumeration',
                                  AvailableForPieces: false,
                                  PossibleValues:
                                   { OptionValue:
                                      [ { Value: 'FullyRegulated', Description: 'Fully Regulated' },
                                        { Value: 'UN3373', Description: 'UN3373' },
                                        { Value: 'UN1845', Description: 'UN1845' },
                                        { Value: 'LessThan500kgExempt', Description: '<500 kg Exempt' },
                                        { Value: 'LimitedQuantities',
                                          Description: 'Limited Quantities' } ] },
                                  ChildServiceOptions: null } ] } } ] } },
                { ID: 'DeclaredValue',
                  Description: 'Declared Value',
                  ValueType: 'Decimal',
                  AvailableForPieces: false,
                  PossibleValues: null,
                  ChildServiceOptions: null },
                { ID: 'HoldForPickup',
                  Description: 'Hold For Pickup',
                  ValueType: 'Enumeration',
                  AvailableForPieces: false,
                  PossibleValues:
                   { OptionValue:
                      [ { Value: 'true', Description: 'Yes' },
                        { Value: 'false', Description: 'No' } ] },
                  ChildServiceOptions: null },
                { ID: 'OriginSignatureNotRequired',
                  Description: 'OSNR-Signature Not Required',
                  ValueType: 'Enumeration',
                  AvailableForPieces: false,
                  PossibleValues:
                   { OptionValue:
                      [ { Value: 'true', Description: 'Yes' },
                        { Value: 'false', Description: 'No' } ] },
                  ChildServiceOptions: null },
                { ID: 'AdultSignatureRequired',
                  Description: 'Adult Signature Required',
                  ValueType: 'Enumeration',
                  AvailableForPieces: false,
                  PossibleValues:
                   { OptionValue:
                      [ { Value: 'true', Description: 'Yes' },
                        { Value: 'false', Description: 'No' } ] },
                  ChildServiceOptions: null },
                { ID: 'SpecialHandling',
                  Description: 'Special Handling',
                  ValueType: 'Enumeration',
                  AvailableForPieces: true,
                  PossibleValues:
                   { OptionValue:
                      [ { Value: 'true', Description: 'Yes' },
                        { Value: 'false', Description: 'No' } ] },
                  ChildServiceOptions:
                   { Option:
                      [ { ID: 'SpecialHandlingType',
                          Description: 'Type of Special Handling',
                          ValueType: 'Enumeration',
                          AvailableForPieces: true,
                          PossibleValues:
                           { OptionValue:
                              [ { Value: 'FlatPackage', Description: 'Flat Package' },
                                { Value: 'AdditionalHandling',
                                  Description: 'Additional Handling' },
                                { Value: 'LargePackage', Description: 'Large package' },
                                { Value: 'Oversized', Description: 'Oversized' } ] },
                          ChildServiceOptions: null } ] } } ] } },
        { ID: 'PurolatorGround',
          Description: 'Purolator Ground',
          PackageType: 'CustomerPackaging',
          PackageTypeDescription: 'Customer Packaging',
          Options:
           { Option:
              [ { ID: 'ChainOfSignature',
                  Description: 'Chain Of Signature',
                  ValueType: 'Enumeration',
                  AvailableForPieces: false,
                  PossibleValues:
                   { OptionValue:
                      [ { Value: 'true', Description: 'Yes' },
                        { Value: 'false', Description: 'No' } ] },
                  ChildServiceOptions: null },
                { ID: 'DangerousGoods',
                  Description: 'Dangerous Goods Indicator',
                  ValueType: 'Enumeration',
                  AvailableForPieces: false,
                  PossibleValues:
                   { OptionValue:
                      [ { Value: 'true', Description: 'Yes' },
                        { Value: 'false', Description: 'No' } ] },
                  ChildServiceOptions:
                   { Option:
                      [ { ID: 'DangerousGoodsMode',
                          Description: 'Dangerous Goods Mode',
                          ValueType: 'Enumeration',
                          AvailableForPieces: false,
                          PossibleValues: { OptionValue: [ { Value: 'Ground', Description: 'Ground' } ] },
                          ChildServiceOptions:
                           { Option:
                              [ { ID: 'DangerousGoodsClass',
                                  Description: 'Dangerous Goods Class',
                                  ValueType: 'Enumeration',
                                  AvailableForPieces: false,
                                  PossibleValues:
                                   { OptionValue:
                                      [ { Value: 'FullyRegulated', Description: 'Fully Regulated' },
                                        { Value: 'UN3373', Description: 'UN3373' },
                                        { Value: 'UN1845', Description: 'UN1845' },
                                        { Value: 'LessThan500kgExempt', Description: '<500 kg Exempt' },
                                        { Value: 'LimitedQuantities',
                                          Description: 'Limited Quantities' } ] },
                                  ChildServiceOptions: null } ] } } ] } },
                { ID: 'DeclaredValue',
                  Description: 'Declared Value',
                  ValueType: 'Decimal',
                  AvailableForPieces: false,
                  PossibleValues: null,
                  ChildServiceOptions: null },
                { ID: 'ExpressCheque',
                  Description: 'ExpressCheque',
                  ValueType: 'Enumeration',
                  AvailableForPieces: false,
                  PossibleValues:
                   { OptionValue:
                      [ { Value: 'true', Description: 'Yes' },
                        { Value: 'false', Description: 'No' } ] },
                  ChildServiceOptions:
                   { Option:
                      [ { ID: 'ExpressChequeMethodOfPayment',
                          Description: 'Method of Payment',
                          ValueType: 'Enumeration',
                          AvailableForPieces: false,
                          PossibleValues:
                           { OptionValue:
                              [ { Value: 'Cheque', Description: 'Cheque' },
                                { Value: 'PostDatedCheque', Description: 'Post dated Cheque' },
                                { Value: 'CertifiedCheque', Description: 'Certified Cheque' },
                                { Value: 'MoneyOrder', Description: 'Money Order' },
                                { Value: 'BankDraft', Description: 'Bank Draft' } ] },
                          ChildServiceOptions:
                           { Option:
                              [ { ID: 'ExpressChequeAmount',
                                  Description: 'Amount',
                                  ValueType: 'Decimal',
                                  AvailableForPieces: false,
                                  PossibleValues: null,
                                  ChildServiceOptions: null } ] } } ] } },
                { ID: 'HoldForPickup',
                  Description: 'Hold For Pickup',
                  ValueType: 'Enumeration',
                  AvailableForPieces: false,
                  PossibleValues:
                   { OptionValue:
                      [ { Value: 'true', Description: 'Yes' },
                        { Value: 'false', Description: 'No' } ] },
                  ChildServiceOptions: null },
                { ID: 'OriginSignatureNotRequired',
                  Description: 'OSNR-Signature Not Required',
                  ValueType: 'Enumeration',
                  AvailableForPieces: false,
                  PossibleValues:
                   { OptionValue:
                      [ { Value: 'true', Description: 'Yes' },
                        { Value: 'false', Description: 'No' } ] },
                  ChildServiceOptions: null },
                { ID: 'AdultSignatureRequired',
                  Description: 'Adult Signature Required',
                  ValueType: 'Enumeration',
                  AvailableForPieces: false,
                  PossibleValues:
                   { OptionValue:
                      [ { Value: 'true', Description: 'Yes' },
                        { Value: 'false', Description: 'No' } ] },
                  ChildServiceOptions: null },
                { ID: 'SpecialHandling',
                  Description: 'Special Handling',
                  ValueType: 'Enumeration',
                  AvailableForPieces: true,
                  PossibleValues:
                   { OptionValue:
                      [ { Value: 'true', Description: 'Yes' },
                        { Value: 'false', Description: 'No' } ] },
                  ChildServiceOptions:
                   { Option:
                      [ { ID: 'SpecialHandlingType',
                          Description: 'Type of Special Handling',
                          ValueType: 'Enumeration',
                          AvailableForPieces: true,
                          PossibleValues:
                           { OptionValue:
                              [ { Value: 'FlatPackage', Description: 'Flat Package' },
                                { Value: 'AdditionalHandling',
                                  Description: 'Additional Handling' },
                                { Value: 'LargePackage', Description: 'Large package' },
                                { Value: 'Oversized', Description: 'Oversized' } ] },
                          ChildServiceOptions: null } ] } } ] } },
        { ID: 'PurolatorExpressEnvelope9AM',
          Description: 'Purolator Express Envelope 9AM',
          PackageType: 'ExpressEnvelope',
          PackageTypeDescription: 'Express Envelope',
          Options:
           { Option:
              [ { ID: 'ChainOfSignature',
                  Description: 'Chain Of Signature',
                  ValueType: 'Enumeration',
                  AvailableForPieces: false,
                  PossibleValues:
                   { OptionValue:
                      [ { Value: 'true', Description: 'Yes' },
                        { Value: 'false', Description: 'No' } ] },
                  ChildServiceOptions: null },
                { ID: 'DeclaredValue',
                  Description: 'Declared Value',
                  ValueType: 'Decimal',
                  AvailableForPieces: false,
                  PossibleValues: null,
                  ChildServiceOptions: null },
                { ID: 'HoldForPickup',
                  Description: 'Hold For Pickup',
                  ValueType: 'Enumeration',
                  AvailableForPieces: false,
                  PossibleValues:
                   { OptionValue:
                      [ { Value: 'true', Description: 'Yes' },
                        { Value: 'false', Description: 'No' } ] },
                  ChildServiceOptions: null },
                { ID: 'OriginSignatureNotRequired',
                  Description: 'OSNR-Signature Not Required',
                  ValueType: 'Enumeration',
                  AvailableForPieces: false,
                  PossibleValues:
                   { OptionValue:
                      [ { Value: 'true', Description: 'Yes' },
                        { Value: 'false', Description: 'No' } ] },
                  ChildServiceOptions: null },
                { ID: 'AdultSignatureRequired',
                  Description: 'Adult Signature Required',
                  ValueType: 'Enumeration',
                  AvailableForPieces: false,
                  PossibleValues:
                   { OptionValue:
                      [ { Value: 'true', Description: 'Yes' },
                        { Value: 'false', Description: 'No' } ] },
                  ChildServiceOptions: null } ] } },
        { ID: 'PurolatorExpressEnvelope10:30AM',
          Description: 'Purolator Express Envelope 10:30AM',
          PackageType: 'ExpressEnvelope',
          PackageTypeDescription: 'Express Envelope',
          Options:
           { Option:
              [ { ID: 'ChainOfSignature',
                  Description: 'Chain Of Signature',
                  ValueType: 'Enumeration',
                  AvailableForPieces: false,
                  PossibleValues:
                   { OptionValue:
                      [ { Value: 'true', Description: 'Yes' },
                        { Value: 'false', Description: 'No' } ] },
                  ChildServiceOptions: null },
                { ID: 'DeclaredValue',
                  Description: 'Declared Value',
                  ValueType: 'Decimal',
                  AvailableForPieces: false,
                  PossibleValues: null,
                  ChildServiceOptions: null },
                { ID: 'HoldForPickup',
                  Description: 'Hold For Pickup',
                  ValueType: 'Enumeration',
                  AvailableForPieces: false,
                  PossibleValues:
                   { OptionValue:
                      [ { Value: 'true', Description: 'Yes' },
                        { Value: 'false', Description: 'No' } ] },
                  ChildServiceOptions: null },
                { ID: 'OriginSignatureNotRequired',
                  Description: 'OSNR-Signature Not Required',
                  ValueType: 'Enumeration',
                  AvailableForPieces: false,
                  PossibleValues:
                   { OptionValue:
                      [ { Value: 'true', Description: 'Yes' },
                        { Value: 'false', Description: 'No' } ] },
                  ChildServiceOptions: null },
                { ID: 'AdultSignatureRequired',
                  Description: 'Adult Signature Required',
                  ValueType: 'Enumeration',
                  AvailableForPieces: false,
                  PossibleValues:
                   { OptionValue:
                      [ { Value: 'true', Description: 'Yes' },
                        { Value: 'false', Description: 'No' } ] },
                  ChildServiceOptions: null } ] } },
        { ID: 'PurolatorExpressEnvelope12PM',
          Description: 'Purolator Express Envelope 12PM',
          PackageType: 'ExpressEnvelope',
          PackageTypeDescription: 'Express Envelope',
          Options:
           { Option:
              [ { ID: 'ChainOfSignature',
                  Description: 'Chain Of Signature',
                  ValueType: 'Enumeration',
                  AvailableForPieces: false,
                  PossibleValues:
                   { OptionValue:
                      [ { Value: 'true', Description: 'Yes' },
                        { Value: 'false', Description: 'No' } ] },
                  ChildServiceOptions: null },
                { ID: 'DeclaredValue',
                  Description: 'Declared Value',
                  ValueType: 'Decimal',
                  AvailableForPieces: false,
                  PossibleValues: null,
                  ChildServiceOptions: null },
                { ID: 'HoldForPickup',
                  Description: 'Hold For Pickup',
                  ValueType: 'Enumeration',
                  AvailableForPieces: false,
                  PossibleValues:
                   { OptionValue:
                      [ { Value: 'true', Description: 'Yes' },
                        { Value: 'false', Description: 'No' } ] },
                  ChildServiceOptions: null },
                { ID: 'OriginSignatureNotRequired',
                  Description: 'OSNR-Signature Not Required',
                  ValueType: 'Enumeration',
                  AvailableForPieces: false,
                  PossibleValues:
                   { OptionValue:
                      [ { Value: 'true', Description: 'Yes' },
                        { Value: 'false', Description: 'No' } ] },
                  ChildServiceOptions: null },
                { ID: 'AdultSignatureRequired',
                  Description: 'Adult Signature Required',
                  ValueType: 'Enumeration',
                  AvailableForPieces: false,
                  PossibleValues:
                   { OptionValue:
                      [ { Value: 'true', Description: 'Yes' },
                        { Value: 'false', Description: 'No' } ] },
                  ChildServiceOptions: null } ] } },
        { ID: 'PurolatorExpressEnvelope',
          Description: 'Purolator Express Envelope',
          PackageType: 'ExpressEnvelope',
          PackageTypeDescription: 'Express Envelope',
          Options:
           { Option:
              [ { ID: 'ChainOfSignature',
                  Description: 'Chain Of Signature',
                  ValueType: 'Enumeration',
                  AvailableForPieces: false,
                  PossibleValues:
                   { OptionValue:
                      [ { Value: 'true', Description: 'Yes' },
                        { Value: 'false', Description: 'No' } ] },
                  ChildServiceOptions: null },
                { ID: 'DeclaredValue',
                  Description: 'Declared Value',
                  ValueType: 'Decimal',
                  AvailableForPieces: false,
                  PossibleValues: null,
                  ChildServiceOptions: null },
                { ID: 'ExpressCheque',
                  Description: 'ExpressCheque',
                  ValueType: 'Enumeration',
                  AvailableForPieces: false,
                  PossibleValues:
                   { OptionValue:
                      [ { Value: 'true', Description: 'Yes' },
                        { Value: 'false', Description: 'No' } ] },
                  ChildServiceOptions:
                   { Option:
                      [ { ID: 'ExpressChequeMethodOfPayment',
                          Description: 'Method of Payment',
                          ValueType: 'Enumeration',
                          AvailableForPieces: false,
                          PossibleValues:
                           { OptionValue:
                              [ { Value: 'Cheque', Description: 'Cheque' },
                                { Value: 'PostDatedCheque', Description: 'Post dated Cheque' },
                                { Value: 'CertifiedCheque', Description: 'Certified Cheque' },
                                { Value: 'MoneyOrder', Description: 'Money Order' },
                                { Value: 'BankDraft', Description: 'Bank Draft' } ] },
                          ChildServiceOptions:
                           { Option:
                              [ { ID: 'ExpressChequeAmount',
                                  Description: 'Amount',
                                  ValueType: 'Decimal',
                                  AvailableForPieces: false,
                                  PossibleValues: null,
                                  ChildServiceOptions: null } ] } } ] } },
                { ID: 'HoldForPickup',
                  Description: 'Hold For Pickup',
                  ValueType: 'Enumeration',
                  AvailableForPieces: false,
                  PossibleValues:
                   { OptionValue:
                      [ { Value: 'true', Description: 'Yes' },
                        { Value: 'false', Description: 'No' } ] },
                  ChildServiceOptions: null },
                { ID: 'OriginSignatureNotRequired',
                  Description: 'OSNR-Signature Not Required',
                  ValueType: 'Enumeration',
                  AvailableForPieces: false,
                  PossibleValues:
                   { OptionValue:
                      [ { Value: 'true', Description: 'Yes' },
                        { Value: 'false', Description: 'No' } ] },
                  ChildServiceOptions: null },
                { ID: 'AdultSignatureRequired',
                  Description: 'Adult Signature Required',
                  ValueType: 'Enumeration',
                  AvailableForPieces: false,
                  PossibleValues:
                   { OptionValue:
                      [ { Value: 'true', Description: 'Yes' },
                        { Value: 'false', Description: 'No' } ] },
                  ChildServiceOptions: null } ] } },
        { ID: 'PurolatorExpressPack9AM',
          Description: 'Purolator Express Pack 9AM',
          PackageType: 'ExpressPack',
          PackageTypeDescription: 'Express Pack',
          Options:
           { Option:
              [ { ID: 'ChainOfSignature',
                  Description: 'Chain Of Signature',
                  ValueType: 'Enumeration',
                  AvailableForPieces: false,
                  PossibleValues:
                   { OptionValue:
                      [ { Value: 'true', Description: 'Yes' },
                        { Value: 'false', Description: 'No' } ] },
                  ChildServiceOptions: null },
                { ID: 'DeclaredValue',
                  Description: 'Declared Value',
                  ValueType: 'Decimal',
                  AvailableForPieces: false,
                  PossibleValues: null,
                  ChildServiceOptions: null },
                { ID: 'HoldForPickup',
                  Description: 'Hold For Pickup',
                  ValueType: 'Enumeration',
                  AvailableForPieces: false,
                  PossibleValues:
                   { OptionValue:
                      [ { Value: 'true', Description: 'Yes' },
                        { Value: 'false', Description: 'No' } ] },
                  ChildServiceOptions: null },
                { ID: 'OriginSignatureNotRequired',
                  Description: 'OSNR-Signature Not Required',
                  ValueType: 'Enumeration',
                  AvailableForPieces: false,
                  PossibleValues:
                   { OptionValue:
                      [ { Value: 'true', Description: 'Yes' },
                        { Value: 'false', Description: 'No' } ] },
                  ChildServiceOptions: null },
                { ID: 'AdultSignatureRequired',
                  Description: 'Adult Signature Required',
                  ValueType: 'Enumeration',
                  AvailableForPieces: false,
                  PossibleValues:
                   { OptionValue:
                      [ { Value: 'true', Description: 'Yes' },
                        { Value: 'false', Description: 'No' } ] },
                  ChildServiceOptions: null } ] } },
        { ID: 'PurolatorExpressPack10:30AM',
          Description: 'Purolator Express Pack 10:30AM',
          PackageType: 'ExpressPack',
          PackageTypeDescription: 'Express Pack',
          Options:
           { Option:
              [ { ID: 'ChainOfSignature',
                  Description: 'Chain Of Signature',
                  ValueType: 'Enumeration',
                  AvailableForPieces: false,
                  PossibleValues:
                   { OptionValue:
                      [ { Value: 'true', Description: 'Yes' },
                        { Value: 'false', Description: 'No' } ] },
                  ChildServiceOptions: null },
                { ID: 'DeclaredValue',
                  Description: 'Declared Value',
                  ValueType: 'Decimal',
                  AvailableForPieces: false,
                  PossibleValues: null,
                  ChildServiceOptions: null },
                { ID: 'HoldForPickup',
                  Description: 'Hold For Pickup',
                  ValueType: 'Enumeration',
                  AvailableForPieces: false,
                  PossibleValues:
                   { OptionValue:
                      [ { Value: 'true', Description: 'Yes' },
                        { Value: 'false', Description: 'No' } ] },
                  ChildServiceOptions: null },
                { ID: 'OriginSignatureNotRequired',
                  Description: 'OSNR-Signature Not Required',
                  ValueType: 'Enumeration',
                  AvailableForPieces: false,
                  PossibleValues:
                   { OptionValue:
                      [ { Value: 'true', Description: 'Yes' },
                        { Value: 'false', Description: 'No' } ] },
                  ChildServiceOptions: null },
                { ID: 'AdultSignatureRequired',
                  Description: 'Adult Signature Required',
                  ValueType: 'Enumeration',
                  AvailableForPieces: false,
                  PossibleValues:
                   { OptionValue:
                      [ { Value: 'true', Description: 'Yes' },
                        { Value: 'false', Description: 'No' } ] },
                  ChildServiceOptions: null } ] } },
        { ID: 'PurolatorExpressPack12PM',
          Description: 'Purolator Express Pack 12PM',
          PackageType: 'ExpressPack',
          PackageTypeDescription: 'Express Pack',
          Options:
           { Option:
              [ { ID: 'ChainOfSignature',
                  Description: 'Chain Of Signature',
                  ValueType: 'Enumeration',
                  AvailableForPieces: false,
                  PossibleValues:
                   { OptionValue:
                      [ { Value: 'true', Description: 'Yes' },
                        { Value: 'false', Description: 'No' } ] },
                  ChildServiceOptions: null },
                { ID: 'DeclaredValue',
                  Description: 'Declared Value',
                  ValueType: 'Decimal',
                  AvailableForPieces: false,
                  PossibleValues: null,
                  ChildServiceOptions: null },
                { ID: 'HoldForPickup',
                  Description: 'Hold For Pickup',
                  ValueType: 'Enumeration',
                  AvailableForPieces: false,
                  PossibleValues:
                   { OptionValue:
                      [ { Value: 'true', Description: 'Yes' },
                        { Value: 'false', Description: 'No' } ] },
                  ChildServiceOptions: null },
                { ID: 'OriginSignatureNotRequired',
                  Description: 'OSNR-Signature Not Required',
                  ValueType: 'Enumeration',
                  AvailableForPieces: false,
                  PossibleValues:
                   { OptionValue:
                      [ { Value: 'true', Description: 'Yes' },
                        { Value: 'false', Description: 'No' } ] },
                  ChildServiceOptions: null },
                { ID: 'AdultSignatureRequired',
                  Description: 'Adult Signature Required',
                  ValueType: 'Enumeration',
                  AvailableForPieces: false,
                  PossibleValues:
                   { OptionValue:
                      [ { Value: 'true', Description: 'Yes' },
                        { Value: 'false', Description: 'No' } ] },
                  ChildServiceOptions: null } ] } },
        { ID: 'PurolatorExpressPack',
          Description: 'Purolator Express Pack',
          PackageType: 'ExpressPack',
          PackageTypeDescription: 'Express Pack',
          Options:
           { Option:
              [ { ID: 'ChainOfSignature',
                  Description: 'Chain Of Signature',
                  ValueType: 'Enumeration',
                  AvailableForPieces: false,
                  PossibleValues:
                   { OptionValue:
                      [ { Value: 'true', Description: 'Yes' },
                        { Value: 'false', Description: 'No' } ] },
                  ChildServiceOptions: null },
                { ID: 'DeclaredValue',
                  Description: 'Declared Value',
                  ValueType: 'Decimal',
                  AvailableForPieces: false,
                  PossibleValues: null,
                  ChildServiceOptions: null },
                { ID: 'ExpressCheque',
                  Description: 'ExpressCheque',
                  ValueType: 'Enumeration',
                  AvailableForPieces: false,
                  PossibleValues:
                   { OptionValue:
                      [ { Value: 'true', Description: 'Yes' },
                        { Value: 'false', Description: 'No' } ] },
                  ChildServiceOptions:
                   { Option:
                      [ { ID: 'ExpressChequeMethodOfPayment',
                          Description: 'Method of Payment',
                          ValueType: 'Enumeration',
                          AvailableForPieces: false,
                          PossibleValues:
                           { OptionValue:
                              [ { Value: 'Cheque', Description: 'Cheque' },
                                { Value: 'PostDatedCheque', Description: 'Post dated Cheque' },
                                { Value: 'CertifiedCheque', Description: 'Certified Cheque' },
                                { Value: 'MoneyOrder', Description: 'Money Order' },
                                { Value: 'BankDraft', Description: 'Bank Draft' } ] },
                          ChildServiceOptions:
                           { Option:
                              [ { ID: 'ExpressChequeAmount',
                                  Description: 'Amount',
                                  ValueType: 'Decimal',
                                  AvailableForPieces: false,
                                  PossibleValues: null,
                                  ChildServiceOptions: null } ] } } ] } },
                { ID: 'HoldForPickup',
                  Description: 'Hold For Pickup',
                  ValueType: 'Enumeration',
                  AvailableForPieces: false,
                  PossibleValues:
                   { OptionValue:
                      [ { Value: 'true', Description: 'Yes' },
                        { Value: 'false', Description: 'No' } ] },
                  ChildServiceOptions: null },
                { ID: 'OriginSignatureNotRequired',
                  Description: 'OSNR-Signature Not Required',
                  ValueType: 'Enumeration',
                  AvailableForPieces: false,
                  PossibleValues:
                   { OptionValue:
                      [ { Value: 'true', Description: 'Yes' },
                        { Value: 'false', Description: 'No' } ] },
                  ChildServiceOptions: null },
                { ID: 'AdultSignatureRequired',
                  Description: 'Adult Signature Required',
                  ValueType: 'Enumeration',
                  AvailableForPieces: false,
                  PossibleValues:
                   { OptionValue:
                      [ { Value: 'true', Description: 'Yes' },
                        { Value: 'false', Description: 'No' } ] },
                  ChildServiceOptions: null } ] } },
        { ID: 'PurolatorExpressBox9AM',
          Description: 'Purolator Express Box 9AM',
          PackageType: 'ExpressBox',
          PackageTypeDescription: 'Express Box',
          Options:
           { Option:
              [ { ID: 'ChainOfSignature',
                  Description: 'Chain Of Signature',
                  ValueType: 'Enumeration',
                  AvailableForPieces: false,
                  PossibleValues:
                   { OptionValue:
                      [ { Value: 'true', Description: 'Yes' },
                        { Value: 'false', Description: 'No' } ] },
                  ChildServiceOptions: null },
                { ID: 'DangerousGoods',
                  Description: 'Dangerous Goods Indicator',
                  ValueType: 'Enumeration',
                  AvailableForPieces: false,
                  PossibleValues:
                   { OptionValue:
                      [ { Value: 'true', Description: 'Yes' },
                        { Value: 'false', Description: 'No' } ] },
                  ChildServiceOptions:
                   { Option:
                      [ { ID: 'DangerousGoodsMode',
                          Description: 'Dangerous Goods Mode',
                          ValueType: 'Enumeration',
                          AvailableForPieces: false,
                          PossibleValues: { OptionValue: [ { Value: 'Air', Description: 'Air' } ] },
                          ChildServiceOptions:
                           { Option:
                              [ { ID: 'DangerousGoodsClass',
                                  Description: 'Dangerous Goods Class',
                                  ValueType: 'Enumeration',
                                  AvailableForPieces: false,
                                  PossibleValues:
                                   { OptionValue:
                                      [ { Value: 'FullyRegulated', Description: 'Fully Regulated' },
                                        { Value: 'UN3373', Description: 'UN3373' },
                                        { Value: 'UN1845', Description: 'UN1845' } ] },
                                  ChildServiceOptions: null } ] } } ] } },
                { ID: 'DeclaredValue',
                  Description: 'Declared Value',
                  ValueType: 'Decimal',
                  AvailableForPieces: false,
                  PossibleValues: null,
                  ChildServiceOptions: null },
                { ID: 'HoldForPickup',
                  Description: 'Hold For Pickup',
                  ValueType: 'Enumeration',
                  AvailableForPieces: false,
                  PossibleValues:
                   { OptionValue:
                      [ { Value: 'true', Description: 'Yes' },
                        { Value: 'false', Description: 'No' } ] },
                  ChildServiceOptions: null },
                { ID: 'OriginSignatureNotRequired',
                  Description: 'OSNR-Signature Not Required',
                  ValueType: 'Enumeration',
                  AvailableForPieces: false,
                  PossibleValues:
                   { OptionValue:
                      [ { Value: 'true', Description: 'Yes' },
                        { Value: 'false', Description: 'No' } ] },
                  ChildServiceOptions: null },
                { ID: 'AdultSignatureRequired',
                  Description: 'Adult Signature Required',
                  ValueType: 'Enumeration',
                  AvailableForPieces: false,
                  PossibleValues:
                   { OptionValue:
                      [ { Value: 'true', Description: 'Yes' },
                        { Value: 'false', Description: 'No' } ] },
                  ChildServiceOptions: null },
                { ID: 'SpecialHandling',
                  Description: 'Special Handling',
                  ValueType: 'Enumeration',
                  AvailableForPieces: true,
                  PossibleValues:
                   { OptionValue:
                      [ { Value: 'true', Description: 'Yes' },
                        { Value: 'false', Description: 'No' } ] },
                  ChildServiceOptions:
                   { Option:
                      [ { ID: 'SpecialHandlingType',
                          Description: 'Type of Special Handling',
                          ValueType: 'Enumeration',
                          AvailableForPieces: true,
                          PossibleValues:
                           { OptionValue:
                              [ { Value: 'FlatPackage', Description: 'Flat Package' },
                                { Value: 'AdditionalHandling',
                                  Description: 'Additional Handling' },
                                { Value: 'LargePackage', Description: 'Large package' },
                                { Value: 'Oversized', Description: 'Oversized' } ] },
                          ChildServiceOptions: null } ] } } ] } },
        { ID: 'PurolatorExpressBox10:30AM',
          Description: 'Purolator Express Box 10:30AM',
          PackageType: 'ExpressBox',
          PackageTypeDescription: 'Express Box',
          Options:
           { Option:
              [ { ID: 'ChainOfSignature',
                  Description: 'Chain Of Signature',
                  ValueType: 'Enumeration',
                  AvailableForPieces: false,
                  PossibleValues:
                   { OptionValue:
                      [ { Value: 'true', Description: 'Yes' },
                        { Value: 'false', Description: 'No' } ] },
                  ChildServiceOptions: null },
                { ID: 'DangerousGoods',
                  Description: 'Dangerous Goods Indicator',
                  ValueType: 'Enumeration',
                  AvailableForPieces: false,
                  PossibleValues:
                   { OptionValue:
                      [ { Value: 'true', Description: 'Yes' },
                        { Value: 'false', Description: 'No' } ] },
                  ChildServiceOptions:
                   { Option:
                      [ { ID: 'DangerousGoodsMode',
                          Description: 'Dangerous Goods Mode',
                          ValueType: 'Enumeration',
                          AvailableForPieces: false,
                          PossibleValues: { OptionValue: [ { Value: 'Air', Description: 'Air' } ] },
                          ChildServiceOptions:
                           { Option:
                              [ { ID: 'DangerousGoodsClass',
                                  Description: 'Dangerous Goods Class',
                                  ValueType: 'Enumeration',
                                  AvailableForPieces: false,
                                  PossibleValues:
                                   { OptionValue:
                                      [ { Value: 'FullyRegulated', Description: 'Fully Regulated' },
                                        { Value: 'UN3373', Description: 'UN3373' },
                                        { Value: 'UN1845', Description: 'UN1845' } ] },
                                  ChildServiceOptions: null } ] } } ] } },
                { ID: 'DeclaredValue',
                  Description: 'Declared Value',
                  ValueType: 'Decimal',
                  AvailableForPieces: false,
                  PossibleValues: null,
                  ChildServiceOptions: null },
                { ID: 'HoldForPickup',
                  Description: 'Hold For Pickup',
                  ValueType: 'Enumeration',
                  AvailableForPieces: false,
                  PossibleValues:
                   { OptionValue:
                      [ { Value: 'true', Description: 'Yes' },
                        { Value: 'false', Description: 'No' } ] },
                  ChildServiceOptions: null },
                { ID: 'OriginSignatureNotRequired',
                  Description: 'OSNR-Signature Not Required',
                  ValueType: 'Enumeration',
                  AvailableForPieces: false,
                  PossibleValues:
                   { OptionValue:
                      [ { Value: 'true', Description: 'Yes' },
                        { Value: 'false', Description: 'No' } ] },
                  ChildServiceOptions: null },
                { ID: 'AdultSignatureRequired',
                  Description: 'Adult Signature Required',
                  ValueType: 'Enumeration',
                  AvailableForPieces: false,
                  PossibleValues:
                   { OptionValue:
                      [ { Value: 'true', Description: 'Yes' },
                        { Value: 'false', Description: 'No' } ] },
                  ChildServiceOptions: null },
                { ID: 'SpecialHandling',
                  Description: 'Special Handling',
                  ValueType: 'Enumeration',
                  AvailableForPieces: true,
                  PossibleValues:
                   { OptionValue:
                      [ { Value: 'true', Description: 'Yes' },
                        { Value: 'false', Description: 'No' } ] },
                  ChildServiceOptions:
                   { Option:
                      [ { ID: 'SpecialHandlingType',
                          Description: 'Type of Special Handling',
                          ValueType: 'Enumeration',
                          AvailableForPieces: true,
                          PossibleValues:
                           { OptionValue:
                              [ { Value: 'FlatPackage', Description: 'Flat Package' },
                                { Value: 'AdditionalHandling',
                                  Description: 'Additional Handling' },
                                { Value: 'LargePackage', Description: 'Large package' },
                                { Value: 'Oversized', Description: 'Oversized' } ] },
                          ChildServiceOptions: null } ] } } ] } },
        { ID: 'PurolatorExpressBox12PM',
          Description: 'Purolator Express Box 12PM',
          PackageType: 'ExpressBox',
          PackageTypeDescription: 'Express Box',
          Options:
           { Option:
              [ { ID: 'ChainOfSignature',
                  Description: 'Chain Of Signature',
                  ValueType: 'Enumeration',
                  AvailableForPieces: false,
                  PossibleValues:
                   { OptionValue:
                      [ { Value: 'true', Description: 'Yes' },
                        { Value: 'false', Description: 'No' } ] },
                  ChildServiceOptions: null },
                { ID: 'DangerousGoods',
                  Description: 'Dangerous Goods Indicator',
                  ValueType: 'Enumeration',
                  AvailableForPieces: false,
                  PossibleValues:
                   { OptionValue:
                      [ { Value: 'true', Description: 'Yes' },
                        { Value: 'false', Description: 'No' } ] },
                  ChildServiceOptions:
                   { Option:
                      [ { ID: 'DangerousGoodsMode',
                          Description: 'Dangerous Goods Mode',
                          ValueType: 'Enumeration',
                          AvailableForPieces: false,
                          PossibleValues: { OptionValue: [ { Value: 'Air', Description: 'Air' } ] },
                          ChildServiceOptions:
                           { Option:
                              [ { ID: 'DangerousGoodsClass',
                                  Description: 'Dangerous Goods Class',
                                  ValueType: 'Enumeration',
                                  AvailableForPieces: false,
                                  PossibleValues:
                                   { OptionValue:
                                      [ { Value: 'FullyRegulated', Description: 'Fully Regulated' },
                                        { Value: 'UN3373', Description: 'UN3373' },
                                        { Value: 'UN1845', Description: 'UN1845' } ] },
                                  ChildServiceOptions: null } ] } } ] } },
                { ID: 'DeclaredValue',
                  Description: 'Declared Value',
                  ValueType: 'Decimal',
                  AvailableForPieces: false,
                  PossibleValues: null,
                  ChildServiceOptions: null },
                { ID: 'HoldForPickup',
                  Description: 'Hold For Pickup',
                  ValueType: 'Enumeration',
                  AvailableForPieces: false,
                  PossibleValues:
                   { OptionValue:
                      [ { Value: 'true', Description: 'Yes' },
                        { Value: 'false', Description: 'No' } ] },
                  ChildServiceOptions: null },
                { ID: 'OriginSignatureNotRequired',
                  Description: 'OSNR-Signature Not Required',
                  ValueType: 'Enumeration',
                  AvailableForPieces: false,
                  PossibleValues:
                   { OptionValue:
                      [ { Value: 'true', Description: 'Yes' },
                        { Value: 'false', Description: 'No' } ] },
                  ChildServiceOptions: null },
                { ID: 'AdultSignatureRequired',
                  Description: 'Adult Signature Required',
                  ValueType: 'Enumeration',
                  AvailableForPieces: false,
                  PossibleValues:
                   { OptionValue:
                      [ { Value: 'true', Description: 'Yes' },
                        { Value: 'false', Description: 'No' } ] },
                  ChildServiceOptions: null },
                { ID: 'SpecialHandling',
                  Description: 'Special Handling',
                  ValueType: 'Enumeration',
                  AvailableForPieces: true,
                  PossibleValues:
                   { OptionValue:
                      [ { Value: 'true', Description: 'Yes' },
                        { Value: 'false', Description: 'No' } ] },
                  ChildServiceOptions:
                   { Option:
                      [ { ID: 'SpecialHandlingType',
                          Description: 'Type of Special Handling',
                          ValueType: 'Enumeration',
                          AvailableForPieces: true,
                          PossibleValues:
                           { OptionValue:
                              [ { Value: 'FlatPackage', Description: 'Flat Package' },
                                { Value: 'AdditionalHandling',
                                  Description: 'Additional Handling' },
                                { Value: 'LargePackage', Description: 'Large package' },
                                { Value: 'Oversized', Description: 'Oversized' } ] },
                          ChildServiceOptions: null } ] } } ] } },
        { ID: 'PurolatorExpressBox',
          Description: 'Purolator Express Box',
          PackageType: 'ExpressBox',
          PackageTypeDescription: 'Express Box',
          Options:
           { Option:
              [ { ID: 'ChainOfSignature',
                  Description: 'Chain Of Signature',
                  ValueType: 'Enumeration',
                  AvailableForPieces: false,
                  PossibleValues:
                   { OptionValue:
                      [ { Value: 'true', Description: 'Yes' },
                        { Value: 'false', Description: 'No' } ] },
                  ChildServiceOptions: null },
                { ID: 'DangerousGoods',
                  Description: 'Dangerous Goods Indicator',
                  ValueType: 'Enumeration',
                  AvailableForPieces: false,
                  PossibleValues:
                   { OptionValue:
                      [ { Value: 'true', Description: 'Yes' },
                        { Value: 'false', Description: 'No' } ] },
                  ChildServiceOptions:
                   { Option:
                      [ { ID: 'DangerousGoodsMode',
                          Description: 'Dangerous Goods Mode',
                          ValueType: 'Enumeration',
                          AvailableForPieces: false,
                          PossibleValues: { OptionValue: [ { Value: 'Air', Description: 'Air' } ] },
                          ChildServiceOptions:
                           { Option:
                              [ { ID: 'DangerousGoodsClass',
                                  Description: 'Dangerous Goods Class',
                                  ValueType: 'Enumeration',
                                  AvailableForPieces: false,
                                  PossibleValues:
                                   { OptionValue:
                                      [ { Value: 'FullyRegulated', Description: 'Fully Regulated' },
                                        { Value: 'UN3373', Description: 'UN3373' },
                                        { Value: 'UN1845', Description: 'UN1845' } ] },
                                  ChildServiceOptions: null } ] } } ] } },
                { ID: 'DeclaredValue',
                  Description: 'Declared Value',
                  ValueType: 'Decimal',
                  AvailableForPieces: false,
                  PossibleValues: null,
                  ChildServiceOptions: null },
                { ID: 'ExpressCheque',
                  Description: 'ExpressCheque',
                  ValueType: 'Enumeration',
                  AvailableForPieces: false,
                  PossibleValues:
                   { OptionValue:
                      [ { Value: 'true', Description: 'Yes' },
                        { Value: 'false', Description: 'No' } ] },
                  ChildServiceOptions:
                   { Option:
                      [ { ID: 'ExpressChequeMethodOfPayment',
                          Description: 'Method of Payment',
                          ValueType: 'Enumeration',
                          AvailableForPieces: false,
                          PossibleValues:
                           { OptionValue:
                              [ { Value: 'Cheque', Description: 'Cheque' },
                                { Value: 'PostDatedCheque', Description: 'Post dated Cheque' },
                                { Value: 'CertifiedCheque', Description: 'Certified Cheque' },
                                { Value: 'MoneyOrder', Description: 'Money Order' },
                                { Value: 'BankDraft', Description: 'Bank Draft' } ] },
                          ChildServiceOptions:
                           { Option:
                              [ { ID: 'ExpressChequeAmount',
                                  Description: 'Amount',
                                  ValueType: 'Decimal',
                                  AvailableForPieces: false,
                                  PossibleValues: null,
                                  ChildServiceOptions: null } ] } } ] } },
                { ID: 'HoldForPickup',
                  Description: 'Hold For Pickup',
                  ValueType: 'Enumeration',
                  AvailableForPieces: false,
                  PossibleValues:
                   { OptionValue:
                      [ { Value: 'true', Description: 'Yes' },
                        { Value: 'false', Description: 'No' } ] },
                  ChildServiceOptions: null },
                { ID: 'OriginSignatureNotRequired',
                  Description: 'OSNR-Signature Not Required',
                  ValueType: 'Enumeration',
                  AvailableForPieces: false,
                  PossibleValues:
                   { OptionValue:
                      [ { Value: 'true', Description: 'Yes' },
                        { Value: 'false', Description: 'No' } ] },
                  ChildServiceOptions: null },
                { ID: 'AdultSignatureRequired',
                  Description: 'Adult Signature Required',
                  ValueType: 'Enumeration',
                  AvailableForPieces: false,
                  PossibleValues:
                   { OptionValue:
                      [ { Value: 'true', Description: 'Yes' },
                        { Value: 'false', Description: 'No' } ] },
                  ChildServiceOptions: null },
                { ID: 'SpecialHandling',
                  Description: 'Special Handling',
                  ValueType: 'Enumeration',
                  AvailableForPieces: true,
                  PossibleValues:
                   { OptionValue:
                      [ { Value: 'true', Description: 'Yes' },
                        { Value: 'false', Description: 'No' } ] },
                  ChildServiceOptions:
                   { Option:
                      [ { ID: 'SpecialHandlingType',
                          Description: 'Type of Special Handling',
                          ValueType: 'Enumeration',
                          AvailableForPieces: true,
                          PossibleValues:
                           { OptionValue:
                              [ { Value: 'FlatPackage', Description: 'Flat Package' },
                                { Value: 'AdditionalHandling',
                                  Description: 'Additional Handling' },
                                { Value: 'LargePackage', Description: 'Large package' },
                                { Value: 'Oversized', Description: 'Oversized' } ] },
                          ChildServiceOptions: null } ] } } ] } } ] } }
 */
