const util = require('util');

// Explode an object to the console
function log(o) {
	console.log(util.inspect(o, false, null))
}

// Recursively add a namespace prefix to all keys
function addNamespace(object, namespace = 'ns1') {
	const newObject = {};

	Object.keys(object).forEach(function(key) {

		const value = object[key];
		const isPlainObject = typeof value === 'object' && value.constructor === Object;

		const newKey = `${namespace}:${key}`;
		newObject[newKey] = isPlainObject ? addNamespace(value, namespace) : value;
	});

	return newObject;
}

module.exports = { log, addNamespace };
