/**
 *  Purolator Public Web Services Example Code
 *
 *  Requires    : Installation of soap module dependency
 *              : Local copy of the Service Availability WSDL
 *              : Valid Development/Production Key and Password
 *
 *  This example covers the creation of a proper SOAP Client (including envelope
 *  headers) to communicate with the Locator Web Service.
 */
const soap = require('soap');
const utils = require('../utils');
const path = require('path');
const account = require('../credentials.json');

const API_KEY = account.key;
const API_PASSWORD = account.password;
const ACCOUNT_NUMBER = account.account;
const BILLING_ACCOUNT = account.billingAccount;
const USER_TOKEN = account.userToken;

function createPWSSOAPClient(headers) {

	const wsdlFile = 'LocatorService.wsdl';
	// NOTE: The API version for this service is 1.0, therefore we need to use v1 datatypes
	const namespace = 'http://purolator.com/pws/datatypes/v1';

	const environment = account.production ? 'production' : 'development';
	const pathToWSDL = path.join(__dirname, '../../wsdl', environment, wsdlFile);

	const options = {
		envelopeKey: 'SOAP-ENV',
	};

	return soap
		.createClientAsync(pathToWSDL, options)
		.then(client => {

			client.setSecurity(new soap.BasicAuthSecurity(API_KEY, API_PASSWORD));
			client.addSoapHeader({ RequestContext: headers }, '', 'ns1', '');

			client.wsdl.definitions.xmlns.ns1 = namespace;
			client.wsdl.xmlnsInEnvelope = client.wsdl._xmlnsMap();

			return client;
		});
}

/**
 * GetLocationsByCoordinates Example
 */

const headers = {
	Version: '1.0', // NOTE: The version number on the namespace must match this #
	Language: 'en',
	GroupID: 'xxx',
	RequestReference: 'GetLocationsByCoordinates example',
	// Uncomment this for Commercial applications
	// UserToken: USER_TOKEN
};

createPWSSOAPClient(headers)
	.then(client => {

		const request = {
			Coordinates: {
				Latitude: '79.8195676000',
				Longitude: '43.3310093000',
			},
			SearchOptions: {
				RadialDistanceInKM: 100,
				HoldForPickup: false,
				DangerousGoods: true,
				Kiosk: false,
				StreetAccess: true,
				WheelChairAccess: true,
				MaxNumberOfLocations: 10,
			},
			LocationTypes: {
				LocationType: 'ShippingCentre'
			},
			HoursOfOperation: {
				OpenTime: '09:00:00',
				CloseTime: '17:00:00',
				CurrentlyOpen: true,
				GMTOffset: '17:00:00',
			},
			DaysOfOperation: {
				DaysOfWeek: [
					'Monday'
				]
			}
		};

		return client.GetLocationsByCoordinatesAsync(request);
	})
	.then(([result, rawResponse, soapheader, rawRequest]) => {
		utils.log(rawRequest);
		utils.log(rawResponse);
		utils.log(result);
	})
	.catch(error => {
		console.log(error);
	});

/**
 * SOAP Request Envelope (Request Made from the SOAP Client)
 * <?xml version="1.0" encoding="UTF-8"?>
 * <?xml version="1.0" encoding="utf-8"?><SOAP-ENV:Envelope xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"  xmlns:msc="http://schemas.microsoft.com/ws/2005/12/wsdl/contract" xmlns:tns="http://purolator.com/pws/service/v1" xmlns:wsu="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd" xmlns:q1="http://schemas.datacontract.org/2004/07/Microsoft.Practices.EnterpriseLibrary.Validation.Integration.WCF" xmlns:q2="http://purolator.com/pws/datatypes/v1" xmlns:q3="http://purolator.com/pws/datatypes/v1" xmlns:q4="http://purolator.com/pws/datatypes/v1" xmlns:q5="http://purolator.com/pws/datatypes/v1" xmlns:q6="http://www.microsoft.com/practices/EnterpriseLibrary/2007/01/wcf/validation" xmlns:q7="http://purolator.com/pws/datatypes/v1" xmlns:q8="http://purolator.com/pws/datatypes/v1" xmlns:q9="http://www.microsoft.com/practices/EnterpriseLibrary/2007/01/wcf/validation" xmlns:q10="http://purolator.com/pws/datatypes/v1" xmlns:q11="http://purolator.com/pws/datatypes/v1" xmlns:q12="http://www.microsoft.com/practices/EnterpriseLibrary/2007/01/wcf/validation" xmlns:q13="http://purolator.com/pws/datatypes/v1" xmlns:q14="http://purolator.com/pws/datatypes/v1" xmlns:q15="http://www.microsoft.com/practices/EnterpriseLibrary/2007/01/wcf/validation" xmlns:q16="http://purolator.com/pws/datatypes/v1" xmlns:q17="http://purolator.com/pws/datatypes/v1" xmlns:q18="http://www.microsoft.com/practices/EnterpriseLibrary/2007/01/wcf/validation" xmlns:ns1="http://purolator.com/pws/datatypes/v1"><SOAP-ENV:Header><ns1:RequestContext><ns1:Version>1.0</ns1:Version><ns1:Language>en</ns1:Language><ns1:GroupID>xxx</ns1:GroupID><ns1:RequestReference>GetLocationsByCoordinates example</ns1:RequestReference></ns1:RequestContext></SOAP-ENV:Header><SOAP-ENV:Body><q7:GetLocationsByCoordinatesRequest xmlns:q7="http://purolator.com/pws/datatypes/v1" xmlns="http://purolator.com/pws/datatypes/v1"><q7:Coordinates><q7:Latitude>79.8195676000</q7:Latitude><q7:Longitude>43.3310093000</q7:Longitude></q7:Coordinates><q7:SearchOptions><q7:RadialDistanceInKM>100</q7:RadialDistanceInKM><q7:HoldForPickup>false</q7:HoldForPickup><q7:DangerousGoods>true</q7:DangerousGoods><q7:Kiosk>false</q7:Kiosk><q7:StreetAccess>true</q7:StreetAccess><q7:WheelChairAccess>true</q7:WheelChairAccess><q7:MaxNumberOfLocations>10</q7:MaxNumberOfLocations></q7:SearchOptions><q7:LocationTypes><q7:LocationType>ShippingCentre</q7:LocationType></q7:LocationTypes><q7:HoursOfOperation><q7:OpenTime>09:00:00</q7:OpenTime><q7:CloseTime>17:00:00</q7:CloseTime><q7:CurrentlyOpen>true</q7:CurrentlyOpen><q7:GMTOffset>17:00:00</q7:GMTOffset></q7:HoursOfOperation><q7:DaysOfOperation><q7:DaysOfWeek>Monday</q7:DaysOfWeek></q7:DaysOfOperation></q7:GetLocationsByCoordinatesRequest></SOAP-ENV:Body></SOAP-ENV:Envelope>
 **/

/**
 * SOAP Response Envelope (Request Returned from the Web Service)
 * <s:Envelope xmlns:s="http://schemas.xmlsoap.org/soap/envelope/"><s:Header><h:ResponseContext xmlns:h="http://purolator.com/pws/datatypes/v1" xmlns:i="http://www.w3.org/2001/XMLSchema-instance"><h:ResponseReference>GetLocationsByCoordinates example</h:ResponseReference></h:ResponseContext></s:Header><s:Body><GetLocationsResponse xmlns="http://purolator.com/pws/datatypes/v1" xmlns:i="http://www.w3.org/2001/XMLSchema-instance"><ResponseInformation><Errors/><InformationalMessages i:nil="true"/></ResponseInformation><Locations><DepotLocation><LocationId>324542</LocationId><LocationTypes><LocationType>Staples</LocationType></LocationTypes><LocationName>Mississauga, ON Canada</LocationName><locationAddress><AddressLine1>201 City centre DR</AddressLine1><AddressLine2>Mississauga Road</AddressLine2><AddressLine3>Mississauga, ON L5L 1C6</AddressLine3><AddressType i:nil="true"/><FloorNumber>1</FloorNumber><StreetNumber>201</StreetNumber><UnitSuiteApt>21312</UnitSuiteApt><CityCode>32423</CityCode><CityName>Mississuaga</CityName><ProvinceStateCode>233423</ProvinceStateCode><CountryCode>324324</CountryCode><Direction i:nil="true"/><PostalCode>3359</PostalCode><StreetName i:nil="true"/><StreetType i:nil="true"/><Suffix i:nil="true"/></locationAddress><ContactName>Jim</ContactName><PhoneNumber>18887447123</PhoneNumber><SpecialInstructionsEn>En</SpecialInstructionsEn><SpecialInstructionsFr>Fr</SpecialInstructionsFr><Depot>Yes</Depot><Unicode>34321234</Unicode><HoldForPickup>false</HoldForPickup><DangerousGoods>false</DangerousGoods><Kiosk>false</Kiosk><StreetAccess>true</StreetAccess><WheelChairAccess>false</WheelChairAccess><OpenMon>00:00:00</OpenMon><CloseMon>11:59:59</CloseMon><OpenExceptionMon>00:00:00</OpenExceptionMon><CloseExceptionMon>11:59:59</CloseExceptionMon><OpenTue>00:00:00</OpenTue><CloseTue>11:59:59</CloseTue><OpenExceptionTue>00:00:00</OpenExceptionTue><CloseExceptionTue>11:59:59</CloseExceptionTue><OpenWed>00:00:00</OpenWed><CloseWed>11:59:59</CloseWed><OpenExceptionWed>00:00:00</OpenExceptionWed><CloseExceptionWed>11:59:59</CloseExceptionWed><OpenThu>00:00:00</OpenThu><CloseThu>11:59:59</CloseThu><OpenExceptionThu>00:00:00</OpenExceptionThu><CloseExceptionThu>11:59:59</CloseExceptionThu><OpenFri>00:00:00</OpenFri><CloseFri>11:59:59</CloseFri><OpenExceptionFri>00:00:00</OpenExceptionFri><CloseExceptionFri>11:59:59</CloseExceptionFri><OpenSat>00:00:00</OpenSat><CloseSat>11:59:59</CloseSat><OpenExceptionSat>00:00:00</OpenExceptionSat><CloseExceptionSat>11:59:59</CloseExceptionSat><OpenSun>00:00:00</OpenSun><CloseSun>11:59:59</CloseSun><OpenExceptionSun>00:00:00</OpenExceptionSun><CloseExceptionSun>11:59:59</CloseExceptionSun><Latitude>51.02</Latitude><Longitude>101.03</Longitude><RadialDistanceInKM>100</RadialDistanceInKM><GMTOffset>25446</GMTOffset><ActivatedDate>2015-12-22T00:00:00</ActivatedDate><DropOffWeekDayAirDom>05:00</DropOffWeekDayAirDom><DropOffWeekDayAirUS>05:00</DropOffWeekDayAirUS><DropOffWeekDayAirIntl>05:00</DropOffWeekDayAirIntl><DropOffWeekDayGndDom>05:00</DropOffWeekDayGndDom><DropOffWeekDayGndUS>05:00</DropOffWeekDayGndUS><DropOffWeekDayGndIntl>05:00</DropOffWeekDayGndIntl><DropOffSatAirDom>05:00</DropOffSatAirDom><DropOffSatAirUS>05:00</DropOffSatAirUS><DropOffSatAirIntl>05:00</DropOffSatAirIntl><DropOffSatGndDom>05:00</DropOffSatGndDom><DropOffSatGndUS>05:00</DropOffSatGndUS><DropOffSatGndIntl>05:00</DropOffSatGndIntl></DepotLocation></Locations></GetLocationsResponse></s:Body></s:Envelope>
 **/

/*
 * EXPECTED RESULTS from PWS
{ ResponseInformation: { Errors: null },
  Locations:
   { DepotLocation:
      [ { LocationId: '324542',
          LocationTypes: { LocationType: [ 'Staples' ] },
          LocationName: 'Mississauga, ON Canada',
          locationAddress:
           { AddressLine1: '201 City centre DR',
             AddressLine2: 'Mississauga Road',
             AddressLine3: 'Mississauga, ON L5L 1C6',
             FloorNumber: '1',
             StreetNumber: '201',
             UnitSuiteApt: '21312',
             CityCode: '32423',
             CityName: 'Mississuaga',
             ProvinceStateCode: '233423',
             CountryCode: '324324',
             PostalCode: '3359' },
          ContactName: 'Jim',
          PhoneNumber: '18887447123',
          SpecialInstructionsEn: 'En',
          SpecialInstructionsFr: 'Fr',
          Depot: 'Yes',
          Unicode: '34321234',
          HoldForPickup: false,
          DangerousGoods: false,
          Kiosk: false,
          StreetAccess: true,
          WheelChairAccess: false,
          OpenMon: '00:00:00',
          CloseMon: '11:59:59',
          OpenExceptionMon: '00:00:00',
          CloseExceptionMon: '11:59:59',
          OpenTue: '00:00:00',
          CloseTue: '11:59:59',
          OpenExceptionTue: '00:00:00',
          CloseExceptionTue: '11:59:59',
          OpenWed: '00:00:00',
          CloseWed: '11:59:59',
          OpenExceptionWed: '00:00:00',
          CloseExceptionWed: '11:59:59',
          OpenThu: '00:00:00',
          CloseThu: '11:59:59',
          OpenExceptionThu: '00:00:00',
          CloseExceptionThu: '11:59:59',
          OpenFri: '00:00:00',
          CloseFri: '11:59:59',
          OpenExceptionFri: '00:00:00',
          CloseExceptionFri: '11:59:59',
          OpenSat: '00:00:00',
          CloseSat: '11:59:59',
          OpenExceptionSat: '00:00:00',
          CloseExceptionSat: '11:59:59',
          OpenSun: '00:00:00',
          CloseSun: '11:59:59',
          OpenExceptionSun: '00:00:00',
          CloseExceptionSun: '11:59:59',
          Latitude: '51.02',
          Longitude: '101.03',
          RadialDistanceInKM: '100',
          GMTOffset: '25446',
          ActivatedDate: 2015-12-22T05:00:00.000Z,
          DropOffWeekDayAirDom: '05:00',
          DropOffWeekDayAirUS: '05:00',
          DropOffWeekDayAirIntl: '05:00',
          DropOffWeekDayGndDom: '05:00',
          DropOffWeekDayGndUS: '05:00',
          DropOffWeekDayGndIntl: '05:00',
          DropOffSatAirDom: '05:00',
          DropOffSatAirUS: '05:00',
          DropOffSatAirIntl: '05:00',
          DropOffSatGndDom: '05:00',
          DropOffSatGndUS: '05:00',
          DropOffSatGndIntl: '05:00' } ] } }
 */
