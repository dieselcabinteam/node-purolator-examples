/**
 *  Purolator Public Web Services Example Code
 *
 *  Requires    : Installation of soap module dependency
 *              : Local copy of the Service Availability WSDL
 *              : Valid Development/Production Key and Password
 *
 *  This example covers the creation of a proper SOAP Client (including envelope
 *  headers) to communicate with the Returns Management Web Service.
 */
const soap = require('soap');
const utils = require('../utils');
const path = require('path');
const account = require('../credentials.json');

const API_KEY = account.key;
const API_PASSWORD = account.password;
const ACCOUNT_NUMBER = account.account;
const BILLING_ACCOUNT = account.billingAccount;
const USER_TOKEN = account.userToken;

function createPWSSOAPClient(headers) {

	const wsdlFile = 'ReturnsManagementService.wsdl';
	const namespace = 'http://purolator.com/pws/datatypes/v2';

	const environment = account.production ? 'production' : 'development';
	const pathToWSDL = path.join(__dirname, '../../wsdl', environment, wsdlFile);

	const options = {
		envelopeKey: 'SOAP-ENV',
	};

	return soap
		.createClientAsync(pathToWSDL, options)
		.then(client => {

			client.setSecurity(new soap.BasicAuthSecurity(API_KEY, API_PASSWORD));
			client.addSoapHeader({ RequestContext: headers }, '', 'ns1', '');

			client.wsdl.definitions.xmlns.ns1 = namespace;
			client.wsdl.xmlnsInEnvelope = client.wsdl._xmlnsMap();

			return client;
		});
}

/**
 * ValidateReturnsManagementShipment Example
 *
 * Create a 1 piece returns management shipment
 */

const headers = {
	Version: '2.0',
	Language: 'en',
	GroupID: 'xxx',
	RequestReference: 'ValidateReturnsShipment example',
	// Uncomment this for Commercial applications
	// UserToken: USER_TOKEN
};

createPWSSOAPClient(headers)
	.then(client => {

		const request = {
			RMA: 'RMA123',
			ReturnsManagementShipment: {
				SenderInformation: {
					Address: {
						Name: 'Aaron Summer',
						StreetNumber: '1234',
						StreetName: 'Main Street',
						City: 'Mississauga',
						Province: 'ON',
						Country: 'CA',
						PostalCode: 'L4W5M8',
						PhoneNumber: {
							CountryCode: '1',
							AreaCode: '905',
							Phone: '5555555',
						}
					}
				},
				ReceiverInformation: {
					Address: {
						Name: 'Aaron Summer',
						StreetNumber: '2245',
						StreetName: 'Douglas Road',
						City: 'Burnaby',
						Province: 'BC',
						Country: 'CA',
						PostalCode: 'V5C5A9',
						PhoneNumber: {
							CountryCode: '1',
							AreaCode: '604',
							Phone: '2982181',
						}
					}
				},
				PackageInformation: {
					ServiceID: 'PurolatorExpress',
					TotalWeight: {
						Value: 40,
						WeightUnit: 'lb',
					},
					TotalPieces: 1,
					PiecesInformation: {
						Piece: [{
							Weight: {
								Value: 40,
								WeightUnit: 'lb',
							},
							Length: {
								Value: 40,
								DimensionUnit: 'in',
							},
							Width: {
								Value: 10,
								DimensionUnit: 'in',
							},
							Height: {
								Value: 2,
								DimensionUnit: 'in',
							},
							Options: {
								OptionIDValuePair: [
									{ ID: 'SpecialHandling', Value: true },
									{ ID: 'SpecialHandlingType', Value: 'LargePackage' }
								]
							}
						}]
					},
					OptionsInformation: {
						Options: {
							OptionIDValuePair: [
								{ ID: 'ResidentialSignatureDomestic', Value: true },
							]
						}
					}
				},
				PaymentInformation: {
					PaymentType: 'Sender',
					RegisteredAccountNumber: ACCOUNT_NUMBER,
					BillingAccountNumber: BILLING_ACCOUNT,
				},
				PickupInformation: {
					PickupType: 'DropOff',
				},
				TrackingReferenceInformation: {
					Reference1: 'Reference id'
				},
				ProactiveNotification: {
					RequestorName: 'MyName',
					RequestorEmail: 'test@test.com',
					Subscriptions: {
						Subscription: [{
							Name: 'MyName',
							Email: 'test@test.com',
							NotifyWhenExceptionOccurs: true,
							NotifyWhenDeliveryOccurs: true,
						}]
					}
				}
			}
		};

		return client.ValidateReturnsManagementShipmentAsync(request);
	})
	.then(([result, rawResponse, soapheader, rawRequest]) => {
		utils.log(rawRequest);
		utils.log(rawResponse);
		utils.log(result);
	})
	.catch(error => {
		console.log(error);
	});

/**
 * SOAP Request Envelope (Request Made from the SOAP Client)
 * <?xml version="1.0" encoding="UTF-8"?>
 * <?xml version="1.0" encoding="utf-8"?><SOAP-ENV:Envelope xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"  xmlns:wsu="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd" xmlns:tns="http://purolator.com/pws/service/v2" xmlns:msc="http://schemas.microsoft.com/ws/2005/12/wsdl/contract" xmlns:q1="http://schemas.datacontract.org/2004/07/Microsoft.Practices.EnterpriseLibrary.Validation.Integration.WCF" xmlns:q2="http://purolator.com/pws/datatypes/v2" xmlns:q3="http://purolator.com/pws/datatypes/v2" xmlns:q4="http://purolator.com/pws/datatypes/v2" xmlns:q5="http://purolator.com/pws/datatypes/v2" xmlns:q6="http://www.microsoft.com/practices/EnterpriseLibrary/2007/01/wcf/validation" xmlns:q7="http://purolator.com/pws/datatypes/v2" xmlns:q8="http://purolator.com/pws/datatypes/v2" xmlns:q9="http://purolator.com/pws/datatypes/v2" xmlns:q10="http://purolator.com/pws/datatypes/v2" xmlns:q11="http://www.microsoft.com/practices/EnterpriseLibrary/2007/01/wcf/validation" xmlns:ns1="http://purolator.com/pws/datatypes/v2"><SOAP-ENV:Header><ns1:RequestContext><ns1:Version>2.0</ns1:Version><ns1:Language>en</ns1:Language><ns1:GroupID>xxx</ns1:GroupID><ns1:RequestReference>ValidateReturnsShipment example</ns1:RequestReference></ns1:RequestContext></SOAP-ENV:Header><SOAP-ENV:Body><q7:ValidateReturnsManagementShipmentRequest xmlns:q7="http://purolator.com/pws/datatypes/v2" xmlns="http://purolator.com/pws/datatypes/v2"><q7:RMA>RMA123</q7:RMA><q7:ReturnsManagementShipment><q7:SenderInformation><q7:Address><q7:Name>Aaron Summer</q7:Name><q7:StreetNumber>1234</q7:StreetNumber><q7:StreetName>Main Street</q7:StreetName><q7:City>Mississauga</q7:City><q7:Province>ON</q7:Province><q7:Country>CA</q7:Country><q7:PostalCode>L4W5M8</q7:PostalCode><q7:PhoneNumber><q7:CountryCode>1</q7:CountryCode><q7:AreaCode>905</q7:AreaCode><q7:Phone>5555555</q7:Phone></q7:PhoneNumber></q7:Address></q7:SenderInformation><q7:ReceiverInformation><q7:Address><q7:Name>Aaron Summer</q7:Name><q7:StreetNumber>2245</q7:StreetNumber><q7:StreetName>Douglas Road</q7:StreetName><q7:City>Burnaby</q7:City><q7:Province>BC</q7:Province><q7:Country>CA</q7:Country><q7:PostalCode>V5C5A9</q7:PostalCode><q7:PhoneNumber><q7:CountryCode>1</q7:CountryCode><q7:AreaCode>604</q7:AreaCode><q7:Phone>2982181</q7:Phone></q7:PhoneNumber></q7:Address></q7:ReceiverInformation><q7:PackageInformation><q7:ServiceID>PurolatorExpress</q7:ServiceID><q7:TotalWeight><q7:Value>40</q7:Value><q7:WeightUnit>lb</q7:WeightUnit></q7:TotalWeight><q7:TotalPieces>1</q7:TotalPieces><q7:PiecesInformation><q7:Piece><q7:Weight><q7:Value>40</q7:Value><q7:WeightUnit>lb</q7:WeightUnit></q7:Weight><q7:Length><q7:Value>40</q7:Value><q7:DimensionUnit>in</q7:DimensionUnit></q7:Length><q7:Width><q7:Value>10</q7:Value><q7:DimensionUnit>in</q7:DimensionUnit></q7:Width><q7:Height><q7:Value>2</q7:Value><q7:DimensionUnit>in</q7:DimensionUnit></q7:Height><q7:Options><q7:OptionIDValuePair><q7:ID>SpecialHandling</q7:ID><q7:Value>true</q7:Value></q7:OptionIDValuePair><q7:OptionIDValuePair><q7:ID>SpecialHandlingType</q7:ID><q7:Value>LargePackage</q7:Value></q7:OptionIDValuePair></q7:Options></q7:Piece></q7:PiecesInformation><q7:OptionsInformation><q7:Options><q7:OptionIDValuePair><q7:ID>ResidentialSignatureDomestic</q7:ID><q7:Value>true</q7:Value></q7:OptionIDValuePair></q7:Options></q7:OptionsInformation></q7:PackageInformation><q7:PaymentInformation><q7:PaymentType>Sender</q7:PaymentType><q7:RegisteredAccountNumber>9999999999</q7:RegisteredAccountNumber><q7:BillingAccountNumber>9999999999</q7:BillingAccountNumber></q7:PaymentInformation><q7:PickupInformation><q7:PickupType>DropOff</q7:PickupType></q7:PickupInformation><q7:TrackingReferenceInformation><q7:Reference1>Reference id</q7:Reference1></q7:TrackingReferenceInformation><q7:ProactiveNotification><q7:RequestorName>MyName</q7:RequestorName><q7:RequestorEmail>test@test.com</q7:RequestorEmail><q7:Subscriptions><q7:Subscription><q7:Name>MyName</q7:Name><q7:Email>test@test.com</q7:Email><q7:NotifyWhenExceptionOccurs>true</q7:NotifyWhenExceptionOccurs><q7:NotifyWhenDeliveryOccurs>true</q7:NotifyWhenDeliveryOccurs></q7:Subscription></q7:Subscriptions></q7:ProactiveNotification></q7:ReturnsManagementShipment></q7:ValidateReturnsManagementShipmentRequest></SOAP-ENV:Body></SOAP-ENV:Envelope>
 **/

/**
 * SOAP Response Envelope (Request Returned from the Web Service)
 * <s:Envelope xmlns:s="http://schemas.xmlsoap.org/soap/envelope/"><s:Header><h:ResponseContext xmlns:h="http://purolator.com/pws/datatypes/v2" xmlns:i="http://www.w3.org/2001/XMLSchema-instance"><h:ResponseReference>ValidateReturnsShipment example</h:ResponseReference></h:ResponseContext></s:Header><s:Body><ValidateReturnsManagementShipmentResponse xmlns="http://purolator.com/pws/datatypes/v2" xmlns:i="http://www.w3.org/2001/XMLSchema-instance"><ResponseInformation><Errors/><InformationalMessages i:nil="true"/></ResponseInformation><ValidShipment>true</ValidShipment></ValidateReturnsManagementShipmentResponse></s:Body></s:Envelope>
 **/

/*
 * EXPECTED RESULTS from PWS
{ ResponseInformation: { Errors: null }, ValidShipment: true }
 */
