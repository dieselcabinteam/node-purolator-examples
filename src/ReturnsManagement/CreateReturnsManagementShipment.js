/**
 *  Purolator Public Web Services Example Code
 *
 *  Requires    : Installation of soap module dependency
 *              : Local copy of the Service Availability WSDL
 *              : Valid Development/Production Key and Password
 *
 *  This example covers the creation of a proper SOAP Client (including envelope
 *  headers) to communicate with the Returns Management Web Service.
 */
const soap = require('soap');
const utils = require('../utils');
const path = require('path');
const account = require('../credentials.json');

const API_KEY = account.key;
const API_PASSWORD = account.password;
const ACCOUNT_NUMBER = account.account;
const BILLING_ACCOUNT = account.billingAccount;
const USER_TOKEN = account.userToken;

function createPWSSOAPClient(headers) {

	const wsdlFile = 'ReturnsManagementService.wsdl';
	const namespace = 'http://purolator.com/pws/datatypes/v2';

	const environment = account.production ? 'production' : 'development';
	const pathToWSDL = path.join(__dirname, '../../wsdl', environment, wsdlFile);

	const options = {
		envelopeKey: 'SOAP-ENV',
	};

	return soap
		.createClientAsync(pathToWSDL, options)
		.then(client => {

			client.setSecurity(new soap.BasicAuthSecurity(API_KEY, API_PASSWORD));
			client.addSoapHeader({ RequestContext: headers }, '', 'ns1', '');

			client.wsdl.definitions.xmlns.ns1 = namespace;
			client.wsdl.xmlnsInEnvelope = client.wsdl._xmlnsMap();

			return client;
		});
}

/**
 * CreateReturnsManagementShipment Example
 *
 * Create a 1 piece returns management shipment
 */

const headers = {
	Version: '2.0',
	Language: 'en',
	GroupID: 'xxx',
	RequestReference: 'CreateReturnsShipment example',
	// Uncomment this for Commercial applications
	// UserToken: USER_TOKEN
};

createPWSSOAPClient(headers)
	.then(client => {

		const request = {
			RMA: 'RMA123',
			ReturnsManagementShipment: {
				SenderInformation: {
					Address: {
						Name: 'Aaron Summer',
						StreetNumber: '1234',
						StreetName: 'Main Street',
						City: 'Mississauga',
						Province: 'ON',
						Country: 'CA',
						PostalCode: 'L4W5M8',
						PhoneNumber: {
							CountryCode: '1',
							AreaCode: '905',
							Phone: '5555555',
						}
					}
				},
				ReceiverInformation: {
					Address: {
						Name: 'Aaron Summer',
						StreetNumber: '2245',
						StreetName: 'Douglas Road',
						City: 'Burnaby',
						Province: 'BC',
						Country: 'CA',
						PostalCode: 'V5C5A9',
						PhoneNumber: {
							CountryCode: '1',
							AreaCode: '604',
							Phone: '2982181',
						}
					}
				},
				PackageInformation: {
					ServiceID: 'PurolatorExpress',
					TotalWeight: {
						Value: 40,
						WeightUnit: 'lb',
					},
					TotalPieces: 1,
					PiecesInformation: {
						Piece: [{
							Weight: {
								Value: 40,
								WeightUnit: 'lb',
							},
							Length: {
								Value: 40,
								DimensionUnit: 'in',
							},
							Width: {
								Value: 10,
								DimensionUnit: 'in',
							},
							Height: {
								Value: 2,
								DimensionUnit: 'in',
							},
							Options: {
								OptionIDValuePair: [
									{ ID: 'SpecialHandling', Value: true },
									{ ID: 'SpecialHandlingType', Value: 'LargePackage' }
								]
							}
						}]
					},
					OptionsInformation: {
						Options: {
							OptionIDValuePair: [
								{ ID: 'ResidentialSignatureDomestic', Value: true },
							]
						}
					}
				},
				PaymentInformation: {
					PaymentType: 'Sender',
					RegisteredAccountNumber: ACCOUNT_NUMBER,
					BillingAccountNumber: BILLING_ACCOUNT,
				},
				PickupInformation: {
					PickupType: 'DropOff',
				},
				TrackingReferenceInformation: {
					Reference1: 'Reference id'
				},
				ProactiveNotification: {
					RequestorName: 'MyName',
					RequestorEmail: 'test@test.com',
					Subscriptions: {
						Subscription: [{
							Name: 'MyName',
							Email: 'test@test.com',
							NotifyWhenExceptionOccurs: true,
							NotifyWhenDeliveryOccurs: true,
						}]
					}
				}
			},
			PrinterType: 'Thermal'
		};

		return client.CreateReturnsManagementShipmentAsync(request);
	})
	.then(([result, rawResponse, soapheader, rawRequest]) => {
		utils.log(rawRequest);
		utils.log(rawResponse);
		utils.log(result);
	})
	.catch(error => {
		console.log(error);
	});

/**
 * SOAP Request Envelope (Request Made from the SOAP Client)
 * <?xml version="1.0" encoding="UTF-8"?>
 * <?xml version="1.0" encoding="utf-8"?><SOAP-ENV:Envelope xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"  xmlns:wsu="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd" xmlns:tns="http://purolator.com/pws/service/v2" xmlns:msc="http://schemas.microsoft.com/ws/2005/12/wsdl/contract" xmlns:q1="http://schemas.datacontract.org/2004/07/Microsoft.Practices.EnterpriseLibrary.Validation.Integration.WCF" xmlns:q2="http://purolator.com/pws/datatypes/v2" xmlns:q3="http://purolator.com/pws/datatypes/v2" xmlns:q4="http://purolator.com/pws/datatypes/v2" xmlns:q5="http://purolator.com/pws/datatypes/v2" xmlns:q6="http://www.microsoft.com/practices/EnterpriseLibrary/2007/01/wcf/validation" xmlns:q7="http://purolator.com/pws/datatypes/v2" xmlns:q8="http://purolator.com/pws/datatypes/v2" xmlns:q9="http://purolator.com/pws/datatypes/v2" xmlns:q10="http://purolator.com/pws/datatypes/v2" xmlns:q11="http://www.microsoft.com/practices/EnterpriseLibrary/2007/01/wcf/validation" xmlns:ns1="http://purolator.com/pws/datatypes/v2"><SOAP-ENV:Header><ns1:RequestContext><ns1:Version>2.0</ns1:Version><ns1:Language>en</ns1:Language><ns1:GroupID>xxx</ns1:GroupID><ns1:RequestReference>CreateReturnsShipment example</ns1:RequestReference></ns1:RequestContext></SOAP-ENV:Header><SOAP-ENV:Body><q2:CreateReturnsManagementShipmentRequest xmlns:q2="http://purolator.com/pws/datatypes/v2" xmlns="http://purolator.com/pws/datatypes/v2"><q2:RMA>RMA123</q2:RMA><q2:ReturnsManagementShipment><q2:SenderInformation><q2:Address><q2:Name>Aaron Summer</q2:Name><q2:StreetNumber>1234</q2:StreetNumber><q2:StreetName>Main Street</q2:StreetName><q2:City>Mississauga</q2:City><q2:Province>ON</q2:Province><q2:Country>CA</q2:Country><q2:PostalCode>L4W5M8</q2:PostalCode><q2:PhoneNumber><q2:CountryCode>1</q2:CountryCode><q2:AreaCode>905</q2:AreaCode><q2:Phone>5555555</q2:Phone></q2:PhoneNumber></q2:Address></q2:SenderInformation><q2:ReceiverInformation><q2:Address><q2:Name>Aaron Summer</q2:Name><q2:StreetNumber>2245</q2:StreetNumber><q2:StreetName>Douglas Road</q2:StreetName><q2:City>Burnaby</q2:City><q2:Province>BC</q2:Province><q2:Country>CA</q2:Country><q2:PostalCode>V5C5A9</q2:PostalCode><q2:PhoneNumber><q2:CountryCode>1</q2:CountryCode><q2:AreaCode>604</q2:AreaCode><q2:Phone>2982181</q2:Phone></q2:PhoneNumber></q2:Address></q2:ReceiverInformation><q2:PackageInformation><q2:ServiceID>PurolatorExpress</q2:ServiceID><q2:TotalWeight><q2:Value>40</q2:Value><q2:WeightUnit>lb</q2:WeightUnit></q2:TotalWeight><q2:TotalPieces>1</q2:TotalPieces><q2:PiecesInformation><q2:Piece><q2:Weight><q2:Value>40</q2:Value><q2:WeightUnit>lb</q2:WeightUnit></q2:Weight><q2:Length><q2:Value>40</q2:Value><q2:DimensionUnit>in</q2:DimensionUnit></q2:Length><q2:Width><q2:Value>10</q2:Value><q2:DimensionUnit>in</q2:DimensionUnit></q2:Width><q2:Height><q2:Value>2</q2:Value><q2:DimensionUnit>in</q2:DimensionUnit></q2:Height><q2:Options><q2:OptionIDValuePair><q2:ID>SpecialHandling</q2:ID><q2:Value>true</q2:Value></q2:OptionIDValuePair><q2:OptionIDValuePair><q2:ID>SpecialHandlingType</q2:ID><q2:Value>LargePackage</q2:Value></q2:OptionIDValuePair></q2:Options></q2:Piece></q2:PiecesInformation><q2:OptionsInformation><q2:Options><q2:OptionIDValuePair><q2:ID>ResidentialSignatureDomestic</q2:ID><q2:Value>true</q2:Value></q2:OptionIDValuePair></q2:Options></q2:OptionsInformation></q2:PackageInformation><q2:PaymentInformation><q2:PaymentType>Sender</q2:PaymentType><q2:RegisteredAccountNumber>9999999999</q2:RegisteredAccountNumber><q2:BillingAccountNumber>9999999999</q2:BillingAccountNumber></q2:PaymentInformation><q2:PickupInformation><q2:PickupType>DropOff</q2:PickupType></q2:PickupInformation><q2:TrackingReferenceInformation><q2:Reference1>Reference id</q2:Reference1></q2:TrackingReferenceInformation><q2:ProactiveNotification><q2:RequestorName>MyName</q2:RequestorName><q2:RequestorEmail>test@test.com</q2:RequestorEmail><q2:Subscriptions><q2:Subscription><q2:Name>MyName</q2:Name><q2:Email>test@test.com</q2:Email><q2:NotifyWhenExceptionOccurs>true</q2:NotifyWhenExceptionOccurs><q2:NotifyWhenDeliveryOccurs>true</q2:NotifyWhenDeliveryOccurs></q2:Subscription></q2:Subscriptions></q2:ProactiveNotification></q2:ReturnsManagementShipment><q2:PrinterType>Thermal</q2:PrinterType></q2:CreateReturnsManagementShipmentRequest></SOAP-ENV:Body></SOAP-ENV:Envelope>
 **/

/**
 * SOAP Response Envelope (Request Returned from the Web Service)
 * <s:Envelope xmlns:s="http://schemas.xmlsoap.org/soap/envelope/"><s:Header><h:ResponseContext xmlns:h="http://purolator.com/pws/datatypes/v2" xmlns:i="http://www.w3.org/2001/XMLSchema-instance"><h:ResponseReference>CreateReturnsShipment example</h:ResponseReference></h:ResponseContext></s:Header><s:Body><CreateReturnsManagementShipmentResponse xmlns="http://purolator.com/pws/datatypes/v2" xmlns:i="http://www.w3.org/2001/XMLSchema-instance"><ResponseInformation><Errors/><InformationalMessages i:nil="true"/></ResponseInformation><ShipmentPIN><Value>329022093408</Value></ShipmentPIN><PiecePINs><PIN><Value>329022093408</Value></PIN></PiecePINs><RMA>RMA123</RMA></CreateReturnsManagementShipmentResponse></s:Body></s:Envelope>
 **/

/*
 * EXPECTED RESULTS from PWS
{ ResponseInformation: { Errors: null },
  ShipmentPIN: { Value: '329022093408' },
  PiecePINs: { PIN: [ { Value: '329022093408' } ] },
  RMA: 'RMA123' }
 */
