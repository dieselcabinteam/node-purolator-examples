/**
 *  Purolator Public Web Services Example Code
 *
 *  Requires    : Installation of soap module dependency
 *              : Local copy of the Service Availability WSDL
 *              : Valid Development/Production Key and Password
 *
 *  This example covers the creation of a proper SOAP Client (including envelope
 *  headers) to communicate with the Tracking Web Service.
 */
const soap = require('soap');
const utils = require('../utils');
const path = require('path');
const account = require('../credentials.json');

const API_KEY = account.key;
const API_PASSWORD = account.password;
const ACCOUNT_NUMBER = account.account;
const BILLING_ACCOUNT = account.billingAccount;
const USER_TOKEN = account.userToken;

function createPWSSOAPClient(headers) {

	const wsdlFile = 'TrackingService.wsdl';
	// NOTE: The API version for this service is 1.2, therefore we need to use v1 datatypes
	const namespace = 'http://purolator.com/pws/datatypes/v1';

	const environment = account.production ? 'production' : 'development';
	const pathToWSDL = path.join(__dirname, '../../wsdl', environment, wsdlFile);

	const options = {
		envelopeKey: 'SOAP-ENV',
	};

	return soap
		.createClientAsync(pathToWSDL, options)
		.then(client => {

			client.setSecurity(new soap.BasicAuthSecurity(API_KEY, API_PASSWORD));
			client.addSoapHeader({ RequestContext: headers }, '', 'ns1', '');

			client.wsdl.definitions.xmlns.ns1 = namespace;
			client.wsdl.xmlnsInEnvelope = client.wsdl._xmlnsMap();

			return client;
		});
}

/**
 * TrackPackagesByPin Example
 *
 * Display the tracking results for a 1 piece shipment
 */

const headers = {
	Version: '1.2', // NOTE: The version number on the namespace must match this #
	Language: 'en',
	GroupID: 'xxx',
	RequestReference: 'TrackPackagesByPin example',
	// Uncomment this for Commercial applications
	// UserToken: USER_TOKEN
};

createPWSSOAPClient(headers)
	.then(client => {

		const request = {
			PINs: {
				PIN: [{
					Value: '329022093200'
				}]
			}
		};

		return client.TrackPackagesByPinAsync(request);
	})
	.then(([result, rawResponse, soapheader, rawRequest]) => {
		utils.log(rawRequest);
		utils.log(rawResponse);
		utils.log(result);
	})
	.catch(error => {
		console.log(error);
	});

/**
 * SOAP Request Envelope (Request Made from the SOAP Client)
 * <?xml version="1.0" encoding="UTF-8"?>
 * <?xml version="1.0" encoding="utf-8"?><SOAP-ENV:Envelope xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"  xmlns:wsu="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd" xmlns:tns="http://purolator.com/pws/service/v1" xmlns:msc="http://schemas.microsoft.com/ws/2005/12/wsdl/contract" xmlns:q1="http://schemas.datacontract.org/2004/07/Microsoft.Practices.EnterpriseLibrary.Validation.Integration.WCF" xmlns:q2="http://purolator.com/pws/datatypes/v1" xmlns:q3="http://purolator.com/pws/datatypes/v1" xmlns:q4="http://purolator.com/pws/datatypes/v1" xmlns:q5="http://purolator.com/pws/datatypes/v1" xmlns:q6="http://www.microsoft.com/practices/EnterpriseLibrary/2007/01/wcf/validation" xmlns:q7="http://purolator.com/pws/datatypes/v1" xmlns:q8="http://purolator.com/pws/datatypes/v1" xmlns:q9="http://purolator.com/pws/datatypes/v1" xmlns:q10="http://purolator.com/pws/datatypes/v1" xmlns:q11="http://www.microsoft.com/practices/EnterpriseLibrary/2007/01/wcf/validation" xmlns:q12="http://purolator.com/pws/datatypes/v1" xmlns:q13="http://purolator.com/pws/datatypes/v1" xmlns:q14="http://purolator.com/pws/datatypes/v1" xmlns:q15="http://purolator.com/pws/datatypes/v1" xmlns:q16="http://www.microsoft.com/practices/EnterpriseLibrary/2007/01/wcf/validation" xmlns:ns1="http://purolator.com/pws/datatypes/v1"><SOAP-ENV:Header><ns1:RequestContext><ns1:Version>1.2</ns1:Version><ns1:Language>en</ns1:Language><ns1:GroupID>xxx</ns1:GroupID><ns1:RequestReference>TrackPackagesByPin example</ns1:RequestReference></ns1:RequestContext></SOAP-ENV:Header><SOAP-ENV:Body><q2:TrackPackagesByPinRequest xmlns:q2="http://purolator.com/pws/datatypes/v1" xmlns="http://purolator.com/pws/datatypes/v1"><q2:PINs><q2:PIN><q2:Value>329022093200</q2:Value></q2:PIN></q2:PINs></q2:TrackPackagesByPinRequest></SOAP-ENV:Body></SOAP-ENV:Envelope>
 **/

/**
 * SOAP Response Envelope (Request Returned from the Web Service)
 * <s:Envelope xmlns:s="http://schemas.xmlsoap.org/soap/envelope/"><s:Header><h:ResponseContext xmlns:h="http://purolator.com/pws/datatypes/v1" xmlns:i="http://www.w3.org/2001/XMLSchema-instance"><h:ResponseReference>TrackPackagesByPin example</h:ResponseReference></h:ResponseContext></s:Header><s:Body><TrackPackagesByPinResponse xmlns="http://purolator.com/pws/datatypes/v1" xmlns:i="http://www.w3.org/2001/XMLSchema-instance"><ResponseInformation><Errors/><InformationalMessages i:nil="true"/></ResponseInformation><TrackingInformationList><TrackingInformation><PIN><Value>329022093200</Value></PIN><Scans><Scan i:type="DeliveryScan"><ScanType>Delivery</ScanType><PIN><Value>329022093200</Value></PIN><Depot><Name>BURNABY, BC</Name></Depot><ScanDate>2015-10-01</ScanDate><ScanTime>164300</ScanTime><Description>Shipment delivered to</Description><Comment/><SummaryScanIndicator>false</SummaryScanIndicator><ScanDetails><DeliverySignature>K BONCI</DeliverySignature><SignatureImage/><SignatureImageSize>0</SignatureImageSize><SignatureImageFormat>GIF</SignatureImageFormat><DeliveryAddress/><DeliveryCompanyName/><PremiumServiceText>Not known or specified</PremiumServiceText><ProductTypeText/><SpecialHandlingText>Not known or specified</SpecialHandlingText><PaymentTypeText/></ScanDetails></Scan><Scan><ScanType>Undeliverable</ScanType><PIN><Value>329022093200</Value></PIN><Depot><Name>BURNABY, BC</Name></Depot><ScanDate>2015-10-01</ScanDate><ScanTime>164300</ScanTime><Description>Available for pickup for 5 business days from arrival date at the counter.</Description><Comment/><SummaryScanIndicator>false</SummaryScanIndicator></Scan><Scan><ScanType>Undeliverable</ScanType><PIN><Value>329022093200</Value></PIN><Depot><Name>BURNABY, BC</Name></Depot><ScanDate>2015-10-01</ScanDate><ScanTime>135900</ScanTime><Description>Attempted Delivery - Customer Closed</Description><Comment/><SummaryScanIndicator>false</SummaryScanIndicator></Scan><Scan i:type="OnDeliveryScan"><ScanType>OnDelivery</ScanType><PIN><Value>329022093200</Value></PIN><Depot><Name>BURNABY, BC</Name></Depot><ScanDate>2015-10-01</ScanDate><ScanTime>135500</ScanTime><Description>On vehicle for delivery</Description><Comment/><SummaryScanIndicator>false</SummaryScanIndicator><ScanDetails><DeliveryAddress/></ScanDetails></Scan><Scan><ScanType>Other</ScanType><PIN><Value>329022093200</Value></PIN><Depot><Name>BURNABY, BC</Name></Depot><ScanDate>2015-09-29</ScanDate><ScanTime>065100</ScanTime><Description>Arrived at sort facility</Description><Comment/><SummaryScanIndicator>false</SummaryScanIndicator></Scan><Scan><ScanType>Other</ScanType><PIN><Value>329022093200</Value></PIN><Depot><Name>VANCOUVER, BC</Name></Depot><ScanDate>2015-09-29</ScanDate><ScanTime>055200</ScanTime><Description>Departed sort facility</Description><Comment/><SummaryScanIndicator>false</SummaryScanIndicator></Scan><Scan><ScanType>Other</ScanType><PIN><Value>329022093200</Value></PIN><Depot><Name>VANCOUVER, BC</Name></Depot><ScanDate>2015-09-29</ScanDate><ScanTime>051400</ScanTime><Description>Arrived at sort facility</Description><Comment/><SummaryScanIndicator>false</SummaryScanIndicator></Scan><Scan><ScanType>Other</ScanType><PIN><Value>329022093200</Value></PIN><Depot><Name>WINNIPEG AIRPORT/AEROPORT, MB</Name></Depot><ScanDate>2015-09-28</ScanDate><ScanTime>234900</ScanTime><Description>Departed sort facility</Description><Comment/><SummaryScanIndicator>false</SummaryScanIndicator></Scan><Scan><ScanType>Other</ScanType><PIN><Value>329022093200</Value></PIN><Depot><Name>REGINA, SK</Name></Depot><ScanDate>2015-09-28</ScanDate><ScanTime>153100</ScanTime><Description>Arrived at sort facility</Description><Comment/><SummaryScanIndicator>false</SummaryScanIndicator></Scan><Scan><ScanType>Other</ScanType><PIN><Value>329022093200</Value></PIN><Depot><Name>REGINA, SK</Name></Depot><ScanDate>2015-09-28</ScanDate><ScanTime>081900</ScanTime><Description>Picked up by Purolator at</Description><Comment/><SummaryScanIndicator>false</SummaryScanIndicator></Scan><Scan><ScanType>Undeliverable</ScanType><PIN><Value>329022093200</Value></PIN><Depot><Name>Purolator</Name></Depot><ScanDate>2015-10-01</ScanDate><ScanTime>164200</ScanTime><Description>Shipment created</Description><Comment/><SummaryScanIndicator>false</SummaryScanIndicator></Scan></Scans><ResponseInformation i:nil="true"/></TrackingInformation></TrackingInformationList></TrackPackagesByPinResponse></s:Body></s:Envelope>
 **/

/*
 * EXPECTED RESULTS from PWS
{ ResponseInformation: { Errors: null },
  TrackingInformationList:
   { TrackingInformation:
      [ { PIN: { Value: '329022093200' },
          Scans:
           { Scan:
              [ { attributes: { 'i:type': 'DeliveryScan' },
                  ScanType: 'Delivery',
                  PIN: { Value: '329022093200' },
                  Depot: { Name: 'BURNABY, BC' },
                  ScanDate: '2015-10-01',
                  ScanTime: '164300',
                  Description: 'Shipment delivered to',
                  Comment: '',
                  SummaryScanIndicator: false,
                  ScanDetails:
                   { DeliverySignature: 'K BONCI',
                     SignatureImage: null,
                     SignatureImageSize: '0',
                     SignatureImageFormat: 'GIF',
                     DeliveryAddress: null,
                     DeliveryCompanyName: null,
                     PremiumServiceText: 'Not known or specified',
                     ProductTypeText: null,
                     SpecialHandlingText: 'Not known or specified',
                     PaymentTypeText: null } },
                { ScanType: 'Undeliverable',
                  PIN: { Value: '329022093200' },
                  Depot: { Name: 'BURNABY, BC' },
                  ScanDate: '2015-10-01',
                  ScanTime: '164300',
                  Description: 'Available for pickup for 5 business days from arrival date at the counter.',
                  Comment: '',
                  SummaryScanIndicator: false },
                { ScanType: 'Undeliverable',
                  PIN: { Value: '329022093200' },
                  Depot: { Name: 'BURNABY, BC' },
                  ScanDate: '2015-10-01',
                  ScanTime: '135900',
                  Description: 'Attempted Delivery - Customer Closed',
                  Comment: '',
                  SummaryScanIndicator: false },
                { attributes: { 'i:type': 'OnDeliveryScan' },
                  ScanType: 'OnDelivery',
                  PIN: { Value: '329022093200' },
                  Depot: { Name: 'BURNABY, BC' },
                  ScanDate: '2015-10-01',
                  ScanTime: '135500',
                  Description: 'On vehicle for delivery',
                  Comment: '',
                  SummaryScanIndicator: false,
                  ScanDetails: { DeliveryAddress: null } },
                { ScanType: 'Other',
                  PIN: { Value: '329022093200' },
                  Depot: { Name: 'BURNABY, BC' },
                  ScanDate: '2015-09-29',
                  ScanTime: '065100',
                  Description: 'Arrived at sort facility',
                  Comment: '',
                  SummaryScanIndicator: false },
                { ScanType: 'Other',
                  PIN: { Value: '329022093200' },
                  Depot: { Name: 'VANCOUVER, BC' },
                  ScanDate: '2015-09-29',
                  ScanTime: '055200',
                  Description: 'Departed sort facility',
                  Comment: '',
                  SummaryScanIndicator: false },
                { ScanType: 'Other',
                  PIN: { Value: '329022093200' },
                  Depot: { Name: 'VANCOUVER, BC' },
                  ScanDate: '2015-09-29',
                  ScanTime: '051400',
                  Description: 'Arrived at sort facility',
                  Comment: '',
                  SummaryScanIndicator: false },
                { ScanType: 'Other',
                  PIN: { Value: '329022093200' },
                  Depot: { Name: 'WINNIPEG AIRPORT/AEROPORT, MB' },
                  ScanDate: '2015-09-28',
                  ScanTime: '234900',
                  Description: 'Departed sort facility',
                  Comment: '',
                  SummaryScanIndicator: false },
                { ScanType: 'Other',
                  PIN: { Value: '329022093200' },
                  Depot: { Name: 'REGINA, SK' },
                  ScanDate: '2015-09-28',
                  ScanTime: '153100',
                  Description: 'Arrived at sort facility',
                  Comment: '',
                  SummaryScanIndicator: false },
                { ScanType: 'Other',
                  PIN: { Value: '329022093200' },
                  Depot: { Name: 'REGINA, SK' },
                  ScanDate: '2015-09-28',
                  ScanTime: '081900',
                  Description: 'Picked up by Purolator at',
                  Comment: '',
                  SummaryScanIndicator: false },
                { ScanType: 'Undeliverable',
                  PIN: { Value: '329022093200' },
                  Depot: { Name: 'Purolator' },
                  ScanDate: '2015-10-01',
                  ScanTime: '164200',
                  Description: 'Shipment created',
                  Comment: '',
                  SummaryScanIndicator: false } ] } } ] } }

 */
